// Job-specific Stat Bonuses Database
//
// Structure of Database:
// JobID,JobLv1,JobLv2,JobLv3,...
//
// Legend for 'JobLvN' fields:
//  0 = No stat bonus at this job level
//  1 = STR increased by 1 at this job level
//  2 = AGI increased by 1 at this job level
//  3 = VIT increased by 1 at this job level
//  4 = INT increased by 1 at this job level
//  5 = DEX increased by 1 at this job level
//  6 = LUK increased by 1 at this job level
//
// TMW2 default is: ,0,6,5,0,2,3,0,1,4,0
// 
// Human (max level = 100)
0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Ukar (max level = 20)
1,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Redy (max level = 20)
2,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Elven (max level = 20)
3,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Orc (max level = 20)
4,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Raijin (max level = 20)
5,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
// Tritan (max level = 20)
6,0,6,5,0,2,3,0,1,4,0,0,6,5,0,2,3,0,1,4,0
