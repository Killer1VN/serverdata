/*
Player groups configuration file
---------------------------------

This file defines "player groups" and their privileges.

Each group has its id and name, lists of available commands and other 
permissions, and a list of other groups it inherits from.


Group settings
--------------
<id>
Unique group number. The only required field.

<name>
Any string. If empty, defaults to "Group <id>". It is used in several @who 
commands.

<level>
Equivalent of GM level, which was used in revisions before r15572. You can 
set it to any number, but usually it's between 0 (default) and 99. Members of 
groups with lower level can not perform some actions/commands (like @kick) on 
members of groups with higher level. It is what script command getgmlevel() 
returns. Group level can also be used to override trade restrictions 
(db/item_trade.txt).

<commands>
A group of settings
	<command name> : <bool>
or
	<commandname> : [ <bool>, <bool> ]
First boolean value is for atcommand, second one for charcommand. If set to 
true, group can use command. If only atcommand value is provided, false is 
assumed for charcommand. If a command name is not included, false is assumed for 
both atcommand and charcommand.
For a full list of available commands, see: doc/atcommands.txt.
Command names must not be aliases.

<log_commands>
Boolean value. If true then all commands used by the group will be logged to 
atcommandlog. If setting is omitted in a group definition, false is assumed.
Requires 'log_commands' to be enabled in 'conf/logs.conf'.

<permissions>
A group of settings
	<permission> : <bool>
If a permission is not included, false is assumed.
For a full list of available permissions, see: doc/permissions.txt

<inherit>
A list of group names that given group will inherit commands and permissions 
from. Group names are case-sensitive.

Inheritance results
-------------------
Both multiple inheritance (Group 2 -> Group 1 and Group 3 -> Group 1) and
recursive inheritance (Group 3 -> Group 2 -> Group 1) are allowed.

Inheritance rules should not create cycles (eg Group 1 inherits from Group 2, 
and Group inherits from Group 1 at the same time). Configuration with cycles is 
considered faulty and can't be processed fully by server.

Command or permission is inherited ONLY if it's not already defined for the 
group.
If group inherits from multiple groups, and the same command or permission is 
defined for more than one of these groups, it's undefined which one will be 
inherited.

Syntax
------
This config file uses libconfig syntax: 
http://www.hyperrealm.com/libconfig/libconfig_manual.html#Configuration-Files
*/

groups: (
{
	id: 0 /* group 0 is the default group for every new account */
	name: "Player"
	level: 0
	inherit: ( /*empty list*/ )
	commands: {
		help: true
		commands: true
		request: true
		duel: true
		accept: true
		reject: true
		leave: true
		whogm: true
		email: true
		rates: true
	}
	permissions: {
		/* without this basic permissions regular players could not 
		trade or party */
		hack_info: true
		can_trade: true
		can_party: true
	}
},
{
	id: 1
	name: "Super Player"
	inherit: ( "Player" ) /* sponsors can do everything Players can and more */
	level: 1
	commands: {
		hominfo: true
		homtalk: true
		pettalk: true
		refresh: true
	}
	log_commands: false
	permissions: {
	}
},
/* “Support” is a baseclass with the basic commands every DEV/GM/ADM may have. */
{
	id: 2
	name: "Support"
	inherit: ( "Super Player" )
	level: 3
	commands: {
		log: true
		tee: true
		hugo: true
		linus: true
		charcommands: true
		uptime: true
		showdelay: true
		exp: true
		mobinfo: true
		iteminfo: true
		whodrops: true
		time: true
		jailtime: true
		hominfo: [true, true]
		homstats: true
		showexp: true
		showzeny: true
		whereis: true
		noask: true
		noks: true
		autoloot: true
		alootid: true
		autoloottype: true
		autotrade: true
		go: true
		breakguild: true
		channel: true
		version: true
		where: [true, true]
		jumpto: true
		who: true
		who2: true
		who3: true
		whomap: true
		whomap2: true
		whomap3: true
		users: true
		mapinfo: true
		gat: true
		mobsearch: true
		idsearch: true
		showmobs: true
		skillid: true
		skilltree: true
		monsterignore: true
		broadcast: true
		localbroadcast: true
	}
	log_commands: true
	permissions: {
		show_client_version: true
		receive_requests: true
		view_equipment: true
	}
},
/* Developer class is only used on TEST SERVER */
{
	id: 5
	name: "Developer"
	inherit: ( "Support" )
	level: 5
	commands: {
		tonpc: true
		hidenpc: true
		shownpc: true
		loadnpc: true
		unloadnpc: true
		npcmove: true
		addwarp: true
		save: [true, true]
		hide: true
		follow: true
		warp: true
		jump: true
		memo: true
		load: true
		recall: true
		slide: [true, true]
		sound: true
	}
	log_commands: true
	permissions: {
	}
},
{
	id: 60
	name: "Officer"
	level: 60
	inherit: ( "Support" )
	commands: {
		trade: true
		storagelist: [true, true]
		cartlist: [true, true]
		itemlist: [true, true]
		stats: [true, true]
		recall: true
		raisemap: [true, true]
		kick: true
		jailfor: true
        unjail: true
		unban: true
		monster: true
		monstersmall: true
		monsterbig: true
		killmonster2: true
		cleanarea: true
		cleanmap: true
		heal: [true, true]
		alive: [true, true]
		sound: true
		storage: true
		clone: [true, true]
		slaveclone: [true, true]
		evilclone: [true, true]
		jump: [true, true]
		npctalk: true
		mute: true
		unmute: true
		speed: true
		accinfo: true
	}
	log_commands: true
	permissions: {
		who_display_aid: true
		view_hpmeter: true
		all_equipment: true
		join_chat: true
		kick_chat: true
		hide_session: true
		hack_info: true
		send_gm: true
		can_trade_bound: false
	}
},
{
	id: 80
	name: "Game Master"
	level: 80
	inherit: ( "Player", "Developer" )
	commands: {
		kick: true
		jailfor: true
		hide: true
		follow: true
		warp: true
		jump: true
		memo: true
		load: true
		recall: true
		summon: true
		monster: true
		killmonster2: true
		cleanarea: true
		cleanmap: true
		delitem: [true, true]
		produce: [true, true]
		refine: [true, true]
		disguise: [true, true]
		undisguise: [true, true]
		size: [true, true]
		raise: true
		raisemap: true
		day: true
		night: true
		skillon: true
		skilloff: true
		pvpon: true
		pvpoff: true
		gvgon: true
		gvgoff: true
		allowks: true
		refresh: [true, true]
		refreshall: true
		fakename: true
		kill: true
		nuke: [true, true]
		doommap: true
		heal: [true, true]
		alive: [true, true]
		sound: true
		storage: true
		clone: [true, true]
		slaveclone: [true, true]
		evilclone: [true, true]
		repairall: [true, true]
		storeall: true
		itemreset: true
		clearstorage: true
		cleargstorage: true
		jump: [true, true]
		killer: true
		killable: true
		monsterignore: [true, true]
		npctalk: true
		mute: true
		mutearea: true
		unmute: true
		speed: [true, true]
		heal: [true, true]
		alive: [true, true]
		ban: true
		block: true
		jail: true
		mute: true
		unmute: true
		storagelist: [true, true]
		cartlist: [true, true]
		itemlist: [true, true]
		stats: [true, true]
		fakename: true
		recallall: true
		raisemap: [true, true]
		raise: [true, true]
		dropall: true
		raisemap: [true, true]
		raise: [true, true]
        unjail: true
		unban: true
		accinfo: true
	}
	log_commands: true
	permissions: {
		who_display_aid: true
		view_hpmeter: true
		join_chat: true
		kick_chat: true
		hide_session: true
		hack_info: true
		send_gm: true
		can_trade_bound: false
		can_party: true
		all_skill: false
		all_equipment: true
		skill_unconditional: false
		use_check: true
		use_changemaptype: true
	}
},
{
	id: 99
	name: "Administrator"
	level: 99
	inherit: ( "Developer" )
	commands: {
		/* not necessary due to all_commands: true */
		accinfo: false
	}
	log_commands: true
	permissions: {
		who_display_aid: true
		view_hpmeter: true
		any_warp: false
		join_chat: true
		kick_chat: true
		hide_session: true
		hack_info: true
		send_gm: true
		can_trade_bound: false
		can_party: true
		all_skill: false
		all_equipment: true
		skill_unconditional: false
		use_check: true
		use_changemaptype: true
		all_commands: true
		hchsys_admin: true
	}
}
)
