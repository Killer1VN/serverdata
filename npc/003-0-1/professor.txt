// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Professor - allows you to gain EXP for idling (Speech skill)

003-0-1,35,29,0	script	Professor	NPC_PLAYER,{
    mesn;
    mesq l("I've mastered the art of speech and communication.");
    next;
    mesn;
    mesq l("Sit on the rug in front of me and learn wisdom! Maybe you'll learn something this way.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, GraduationCap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GraduationRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, DeepBlackBoots);
    setunitdata(.@npcId, UDT_WEAPON, CottonGloves);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    initnpctimer;
    end;

OnTimer3000:
    areatimer2("003-0-1", 29, 34, 41, 38, 10, .name$+"::OnSpeeching");
    initnpctimer;
    end;

OnSpeeching:
    // If you have been IDLE for at least 2 seconds (max AFK 15 minutes)
    // You'll get the EXP. Note you don't need to sit, only be idle.
    if (is_between(2, 900, checkidle())) {
        .@sk=getskilllv(TMW2_SPEECH);
        getexp 1+.@sk,1+(.@sk/2);
        // dispbottom l("It is a boring speech...");

        // If you are learning TMW2_READANCIENTLANGUAGES    
        if (ANCIENTLANGUAGEBOUNCER) {
            ANCIENTLANGUAGEBOUNCER-=1;
            if (ANCIENTLANGUAGEBOUNCER == 1) {
                ANCIENTLANGUAGEBOUNCER=0;
                skill TMW2_ANCIENTLANGUAGES, 1, 0;
                dispbottom l("It was a boring speech, but you have learned ancient languages.");
            }
        }

    }
    end;
}

