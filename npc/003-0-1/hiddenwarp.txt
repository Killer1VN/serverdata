// TMW2 Script
// Author:
//    Jesusalva
// Easter Egg for Hello World player
// TODO: In future, check player direction

003-0-1,21,24,0	script	#MSchoolRoof	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    warp "003-1", 49, 24;
    .@q=getq(General_EasterEggs);

    if (!(.@q & EE_MAGICSCHOOL)) {
        setq General_EasterEggs, .@q|EE_MAGICSCHOOL;
        dispbottom l("For finding an Easter Egg, you got Strange Coins!");
        getitem StrangeCoin, 3;
    }

    end;
}
