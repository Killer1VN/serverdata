// TMW-2 Script
// Author:
//  Jesusalva
// Description:
//  Controls the Yetifly Challenge to earn the right to access Lilit

// .@q = LilitQuest_Access
// 0 - Access not granted
//      q2 - Number of attempts
// 1 - Access granted
// 2 - Tree Minigame complete.

// It is always cast with an instance in mind
018-5-boss,0,0,0	script	#YetiFlyChallengeCtrl	NPC_HIDDEN,{
    end;

OnCleanUp:
    YETIFLY_INSTANCE=0;
    end;

OnWarn1:
    instance_announce(YETIFLY_INSTANCE, "On your marks!", 0);
    addtimer(2000, "#YetiFlyChallengeCtrl::OnWarn2");
    end;

OnWarn2:
    instance_announce(YETIFLY_INSTANCE, "READY?!", 0);
    addtimer(2000, "#YetiFlyChallengeCtrl::OnBegin");
    end;

OnBegin:
    instance_announce(YETIFLY_INSTANCE, "START!", 0);
    @YETIFLY_CYCLE=0;
    // No event is bound: Slaying the Yetifly is not planned and thus, without effect.
    // It'll vanish along the instance
    monster "lilt@"+getcharid(0), 33, 24, l("Yetifly the Mighty"), Yetifly, 1;
    addtimer(10000, "#YetiFlyChallengeCtrl::OnCycle");
    end;

OnCycle:
    if (!(getmap() ~= "lilt*" ) || ispcdead())
        end;
    @YETIFLY_CYCLE+=1;

    // Is it over? Were you victorious?
    if (@YETIFLY_CYCLE >= 6) {
        setq LilitQuest_Access, 1;
        getexp BaseLevel*425, JobLevel*190; // Reference: (35,25). Scalable reward.
        warp "018-5", 97, 70;
        mesn l("Yetifly the Mighty");
        mesq l("Not bad... You can now enter Lilit. If you think you can defeat me, climb the tree and meet me at the top!");
        close;
    }

    // Resume execution
    instance_announce(YETIFLY_INSTANCE, l("survive @@ seconds more!", (6-@YETIFLY_CYCLE)*10), 0);
    addtimer(10000, "#YetiFlyChallengeCtrl::OnCycle");
    end;

}
