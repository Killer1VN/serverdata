// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Town Administrator file, see npc/functions/politics.txt
// User variables:
//    #POL_APPLYWEEK = Week of last application
//    #POL_VOTEDAY = Day of last vote

003-2,29,35,0	script	Tulimshar Office	NPC_POLITICS,{
do
{
    mesc ".:: "+l("Tulimshar Townhall")+" ::.", 2;
    mesc l("Current Town Administrator: ")+$TULIM_MAYOR$, 3;
    mesc l("Application fee: @@ GP", .applytax);
    next;
    select
        l("Information"),
        rif(#POL_APPLYWEEK != gettimeparam(GETTIME_WEEKDAY), l("Apply for the office!")),
        l("View Candidate List and cast a vote"),
        l("[Quit]");

    switch (@menu) {
        case 1:
            POL_Information();
            break;
        case 2:
            // array_push might be too sensible for getd/setd
            if (Zeny < .applytax)
                break;
            Zeny-=.applytax;
            $TULIM_MONEY+=.applytax;
            #POL_APPLYWEEK=gettimeparam(GETTIME_WEEKDAY);
            array_push($TULIM_CANDIDATE$, strcharinfo(0));
            array_push($TULIM_VOTES, 0);
            mesc l("Application successful!"), 3;
            next;
            break;
        case 3:
            POL_Candidate("TULIM");
            break;
        default:
            close;
    }
} while (true);
end;

OnInit:
    .applytax=60;
    .distance=4;
    end;
}

