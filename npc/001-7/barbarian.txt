// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Wolvern Teeth Necklace Quest. Inhabits Frostia Hills.
//    The necklace can also be dropped by the Wolvern normally. Any of those will do
//    to advance the Barbarian Necklaces plot. (Until you get the final version of it)
//    This is a BONUS QUEST and can only be done via Yeti King Quest. It is NOT
//    meant to be doable outside it. It's overrewarding, yes. It's meant to be this way.
//    Do not tell everybody about this one :> Bonus to those who explore!
// NOTE: Temporaly replaced with ClawPendant
// Variables:
//    FrostiaQuest_WolfNecklace  - quest var


001-7,72,39,0	script	Barbarian#Frostia	NPC_HALBERDBARBARIAN,{
    .@q = getq(FrostiaQuest_WolfNecklace);
    mesn;
    mesq l("Hello my friend.");
    if (BaseLevel < 35) goto L_Weakling;
    if (.@q == 0 && countitem(ToothNecklace)) goto L_Quest;
    if (.@q == 1) goto L_CheckItems;
    close;

L_Weakling:
    mesq l("These cliffs are no place for weak people like you. You better teleport yourself away as soon as possible!");
    close;

L_Quest:
    mesq l("Oooh, I see you have a @@ with you.", getitemlink(ToothNecklace));
    next;
    mesn;
    mesq l("It's done with @@ teethes. These drain your life and raise your strenght to SMASH your foes!", getmonsterlink(Wolvern));
    next;
    mesn;
    mesq l("If you bring me 2 @@, that Necklace, and 4000 GP, I can improve it. It'll be worth it, I warrant you!", getitemlink(WolvernTooth)); // 5% drop rate of tooth.
    setq FrostiaQuest_WolfNecklace, 1;
    close;

L_CheckItems:
    if (!countitem(ToothNecklace) || countitem(WolvernTooth) < 2 || Zeny < 4000) {
        mesn;
        mesq l("Sorry, but you don't have what I need.");
        mesq l("I need 2 @@, the @@, and 4000 GP.", getitemlink(WolvernTooth), getitemlink(ToothNecklace));
        close;
    }
    mesn;
    mesq l("Are you with what I asked for?");
    if (askyesno() == ASK_NO)
        close;
    mes "";

    // No checks because I did before and cheaters must die.
    if (Zeny < 4000) {
        Zeny=0;
        percentheal -75, -100;
        mesc l("All cheaters must die."), 1;
        close;
    }
    inventoryplace ClawPendant, 1;
    delitem WolvernTooth, 2;
    delitem ToothNecklace, 1;
    getitem ClawPendant, 1;
    Zeny=Zeny-4000;
    getexp 15000, 0;
    mesn;
    mesq l("Here you go, thanks for doing my SECRET quest! Eh, not much to do here. But I like life this way.");
    setq FrostiaQuest_WolfNecklace, 2;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
