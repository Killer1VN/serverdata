// TMW-2 Script.
// Authors:
//    Saulc
// Description:
//    A small note

005-2-1,41,33,0	script	Note#saxsocave	NPC_PAPER_NOTE,{
    mesc l("I leave this basement that start to be too dangerous!");

    .@q=getq(General_EasterEggs);

    if (!(.@q & EE_SAXSO)) {
        setq General_EasterEggs, .@q|EE_SAXSO;
        dispbottom l("For finding an Easter Egg, you got Strange Coins!");
        getitem StrangeCoin, 5;
    }
    close;

OnInit:
    .distance = 2;
    end;
}
