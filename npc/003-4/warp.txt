// TMW2 Scripts.

003-4,38,31,0	script	Mahoud Basement	NPC_HIDDEN,0,0,{

OnTouch:
    if (isinstance(getq2(TulimsharQuest_Hasan)) && getq2(TulimsharQuest_Hasan) != 0)
        warp "hasn@"+str(getcharid(0)), 34, 27;
    else if (getq(TulimsharQuest_Hasan) > 3)
        warp "003-4-1", 34, 27;
    else
        dispbottom l("This door is locked.");
    end;
}
