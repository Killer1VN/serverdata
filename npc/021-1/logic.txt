// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Cave logic. Controls also switchs and false warps. See soren/main for more info.

// Logic
function	script	CindySwitch_Check_211	{
    .@st1=getvariableofnpc(.lifetime, "#CindySwitch_01")-gettimetick(2);
    .@st2=getvariableofnpc(.lifetime, "#CindySwitch_02")-gettimetick(2);
    .@st3=getvariableofnpc(.lifetime, "#CindySwitch_03")-gettimetick(2);
    .@st4=getvariableofnpc(.lifetime, "#CindySwitch_04")-gettimetick(2);
    .@st5=getvariableofnpc(.lifetime, "#CindySwitch_05")-gettimetick(2);
    .@i=0;
    if (.@st1 > 0) .@i++;
    if (.@st2 > 0) .@i++;
    if (.@st3 > 0) .@i++;
    if (.@st4 > 0) .@i++;
    if (.@st5 > 0) .@i++;
    return .@i;
}

// Passages
021-1,155,196,0	script	#0211Logic	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    // Sanitize some stuff
    if ($@CINDY_STATE < gettimetick(2) && $@CINDY_STATE > 1500000000)
        $@CINDY_STATE=0;
    // Main logic
    .@q=getq(NivalisQuest_Cindy);
    if (.@q < 3) end;
    if ($@CINDY_STATE > gettimetick(2)) goto L_Safe;
    if ($@CINDY_STATE % 2 == 1) goto L_Blocked;
    if (CindySwitch_Check_211() == 5) {
        .@pos=rand(0,8);
        if (.@q == 3) {
            setq NivalisQuest_Cindy, 4;
            warp "021-1", .xp[.@pos], .yp[.@pos];
        } else {
            warp "021-2", 80, 102;
        }
    } else {
        switch (CindySwitch_Check_211()) {
            case 0:
            case 1:
                end;
            case 2:
            case 3:
                dispbottom l("A few switches aren't triggered yet."); break;
            case 4:
            case 5:
                dispbottom l("A single switch is not online - Cannot pass without all of them on."); break;
        }
    }
    end;

L_Blocked:
    dispbottom l("This place is reeking blood. We better come again later.");
    end;

L_Safe:
    dispbottom l("There's no reason to enter these caves now.");
    end;

OnInit:
    setarray .xp,  25,  43,  97, 142, 274,  52, 36, 52, 120;
    setarray .yp, 300, 300, 300, 300, 299, 176, 79, 77,  73;
    end;
}

021-1,136,29,0	script	#FrostiaGateway	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    if (!$NIVALIS_LIBDATE) {
        npctalkonce l("Brr! It's extremely cold! I cannot go there!");
        end;
    } else if (BaseLevel < 40) {
        mesc l("You're about to enter a dangerous area. Are you sure?");
        if (askyesno() == ASK_NO)
            end;
    }
    warp "022-1", 69, 111;
    end;

}

// Switches
021-1,282,34,0	script	#CindySwitch_01	NPC_SWITCH_OFFLINE,{
    .@q=getq(NivalisQuest_Cindy);
    if (.@q < 3) {
        mesn strcharinfo(0);
        mesq l("This is a strange switch...");
        close;
    }
    if (.lifetime-gettimetick(2) <= 0) {
        mesc l("Pull the switch?");
        select
            l("Pull it"),
            l("Leave it");
        if (@menu == 1) {
            getexp rand(55, 110), rand(5, 11);
            .lifetime=gettimetick(2)+rand(110, 150)+180; // Something between 1m50s and 2m30s + 3 minutes because the maze is huge
            specialeffect(FX_SUCCESS);
            setnpcdisplay .name$, NPC_SWITCH_ONLINE;
            initnpctimer;
            .@r=rand(0,100);
            getmapxy(.@m$, .@x, .@y,0);
            if (.@r < 80)
                monster .@m$, .@x, .@y, "Yeti", Yeti, 1;
            else if (.@r > 99)
                makeitem(Candy, 1, .@m$, .@x, .@y);
            npctalk l("Automatic disarm in: @@", FuzzyTime(.lifetime));
            closedialog;
        }
        close;
    }
    npctalk l("Automatic disarm in: @@", FuzzyTime(.lifetime));
    end;

OnTimer1000:
    if (.lifetime-gettimetick(2) <= 0) {
        setnpcdisplay .name$, NPC_SWITCH_OFFLINE;
        stopnpctimer;
    } else {
        initnpctimer;
    }
    end;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    .lifetime=0;  // When will this switch turn off automatically
    end;

}

021-1,231,36,0	duplicate(#CindySwitch_01)	#CindySwitch_02	NPC_SWITCH_OFFLINE
021-1,34,29,0	duplicate(#CindySwitch_01)	#CindySwitch_03	NPC_SWITCH_OFFLINE
021-1,25,297,0	duplicate(#CindySwitch_01)	#CindySwitch_04	NPC_SWITCH_OFFLINE
021-1,30,168,0	duplicate(#CindySwitch_01)	#CindySwitch_05	NPC_SWITCH_OFFLINE

