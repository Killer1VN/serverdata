// TMW2 Scripts.
// Author:
//  Jesusalva
// Description:
//  Magic School recepcionist (TODO: Give S. Badge)

003-0,47,44,4	script	Recepcionist#003-0	NPC_FEMALE,{
    if (MGQUEST && getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER >= 1) goto L_T3_S0;

    mesn;
    mesq l("Hello, and welcome to the Magic School of Tulimshar.");
    next;
    mesn;
    mesq l("Thankfully, we'll be moving soon. This building is old and the roof is falling.");
    next;
    mesn;
    mesq l("I hope our new campus get built soon. It'll be much better than here.");
    if (!MGQUEST)
        goto L_Prologue;
    if (!MAGIC_LVL)
        close;
    next;
    switch (getskilllv(TMW2_SKILLPERMIT)) {
        case 0:
            if (MAGIC_LVL >= 1)
                goto L_Tier1;
            break;
        case 1:
            if (MAGIC_LVL >= 2)
                goto L_Tier2;
            break;
        case 2:
            if (MAGIC_LVL >= 3)
                goto L_Tier3;
            break;
    }
    closeclientdialog;
    goodbye;
    close;

//////////////
/* Prologue */
//////////////
L_Prologue:
    next;
    mesn l("Magic Academy Tutorial");
    mesc l("The Magic Academy System is responsible for learning most skills ingame. For that, you need two things: Magic Skill Points and a reagent.");
    next;
    mesn l("Magic Academy Tutorial");
    mesc l("The reagent is always the same for the same class. If you do not have enough reagents but have an @@, it'll be used to continue.", getitemlink(ScholarshipBadge));
    next;
    mesn l("Magic Academy Tutorial");
    mesc l("Magic Skill Points can be obtained in three ways: By touching a Mana Stone, by signing up in a Special Class (if you have enough magic power) and by having high amounts of Job Level.");
    next;
    mesn l("Magic Academy Tutorial");
    mesc l("Upgrading a skill level can be done the same away and will always cost a single Magic Skill Point. Job Level points are obtained only after Lv @@, and is a single point each @@ levels.", 15+12, 12);
    next;
    inventoryplace ScholarshipBadge, 1;
    mesn;
    mesq l("That being said, I'll give you an @@, which allows you to learn a skill even if you can't pay for it.", getitemlink(ScholarshipBadge));
    next;
    mesn;
    mesq l("Please note unless you have Magic Powers, obtained from the Mana Seed, all you will be able to learn are small tricks, so please make a wise choice.");
    MGQUEST=true;
    getitem ScholarshipBadge, 1;
    close;

////////////////
/* First Tier */
////////////////
L_Tier1:
    mesn;
    mesq l("Also, I see you're a newly registered mage. Am I right?");
    next;
    if (askyesno() == ASK_NO) {
        mesn;
        mesq l("Alright. I wish you good luck in your studies.");
        close;
    }
    mesn;
    mesq l("Good. Did you knew you could register to the Special Class, in order to get an extra skill point?");
    next;
    mesn;
    mesq l("We only require a small fee of 1 @@, or 40 @@, or 500 @@ if you are poor adventurer.", getitemlink(DivineApple), getitemlink(SnakeEgg), getitemlink(MaggotSlime));
    next;
    switch(select(
        rif(countitem(DivineApple) >= 1, l("I got the apple.")),
        rif(countitem(SnakeEgg) >= 40, l("I got the eggs.")),
        rif(countitem(MaggotSlime) >= 500, l("I got the maggots slimes.")),
        l("I will apply later."))) {

        case 1:
            delitem DivineApple, 1;
            getexp $MANA_BLVL*100, $MANA_JLVL*10;
            break;
        case 2:
            delitem SnakeEgg, 40;
            getexp $MANA_BLVL*100, $MANA_JLVL*10;
            break;
        case 3:
            delitem MaggotSlime, 500;
            break;
        default:
            close;
            break;
    }
    skill TMW2_SKILLPERMIT, 1, 0;
    mes "";
    mesn;
    mesq l("Many thanks, your help has been invaluable. You now have an extra point, use it wisely.");
    close;

/////////////////
/* Second Tier */
/////////////////
L_Tier2:
    mesn;
    mesq l("Maybe you're interested in the Special Class again? An extra magic skill point for a lot of items, what do ya say?");
    next;
    if (askyesno() == ASK_NO) {
        mesn;
        mesq l("Alright. I wish you good luck in your studies.");
        close;
    }
    mesn;
    mesq l("Great news! Then please bring me 1 @@, or 200 @@ @@ 20 @@.", getitemlink(DivineApple), getitemlink(SilkCocoon), b(l("and")), getitemlink(ChocolateMouboo));
    next;
    switch(select(
        rif(countitem(DivineApple) >= 1, l("I got the apple.")),
        rif(countitem(SilkCocoon) >= 200 && countitem(ChocolateMouboo) >= 20, l("I got the silk and chocolate.")),
        l("I will apply later."))) {

        case 1:
            delitem DivineApple, 1;
            break;
        case 2:
            delitem SilkCocoon, 200;
            delitem ChocolateMouboo, 20;
            break;
        default:
            close;
            break;
    }
    skill TMW2_SKILLPERMIT, 2, 0;
    getexp 10000, 0;
    mes "";
    mesn;
    mes l("Many thanks, and once again, your help has been invaluable.");
    mes l("You now have an extra point, use it wisely.");
    close;

/////////////////
/* Third Tier */
/////////////////
L_Tier3:
    ST_TIER=1;
    mesn;
    mesq l("Maybe you're interested in the Special Class again? An extra magic skill point, but this time in a dangerous journey, what do ya say?");
    next;
    if (nard_reputation() < 8) {
        mesn;
        mesc l("I advise you to do more quests on Tulimshar and Candor, otherwise, you will fail right at the end."), 1;
        next;
    }
    if (askyesno() == ASK_NO) {
        mesn;
        mesq l("Alright. I wish you good luck in your studies.");
        close;
    }
    mesn;
    mesq l("I will prepare you a potion. But beware, that potion will only last 20 minutes. You should assign some intelligence points to succeed.");
    next;
    mesn;
    mes l("If it expires, you'll need to do another. To bake it I need 1 @@, 10 @@ and an @@.", getitemlink(EverburnPowder), getitemlink(MaggotSlime), getitemlink(EmptyBottle));
    mesc l("Have Maggot Slimes, Bug Legs, Mauve Herbs and Money, lots of them."), 1;
    next;
    select
        rif(countitem(MaggotSlime) >= 10 && countitem(EverburnPowder) && countitem(EmptyBottle), l("I have everything.")),
        l("I'm not ready.");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Yes, as you see, the costs are high. Prepare yourself.");
        close;
    }

    delitem EmptyBottle, 1;
    delitem EverburnPowder, 1;
    delitem MaggotSlime, 10;
    ST_TIER=2;
    QUEST_ELEVARTEMPO=gettimetick(2) + (60 * 20);
    getexp 400, 0;
    mesn;
    mesc l("She mix the powder with the slime inside the bottle, and makes some weird mixture.");
    next;
    mesn;
    mesc l("She pours something on it, you're not sure what. And then utters some magic words.");
    next;
    // Reset timer, this is the place where it should really happen.
    QUEST_ELEVARTEMPO=gettimetick(2) + (60 * 20);
    mesn;
    mesq l("The potion is baked, and the time is now running! Read as fast as you can, don't miss details!");
    next;
    mesn;
    mesq l("First thing is to get a @@. One from black market won't do, go to Halinarzo!", getitemlink(SunnyCrystal));
    next;
    mesn;
    mesq l("Speak with ##BBarzil##b. Tell him it is for the Magic Academy. HURRY UP!");
    close;

// Logic handler
L_T3_S0:
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 1) goto L_Tier3;
    if (gettimetick(2) > QUEST_ELEVARTEMPO) goto L_T3_Fail;

    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 2) goto L_T3_S2;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 3) goto L_T3_S3;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 4) goto L_T3_S4;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 5) goto L_T3_S5;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 6) goto L_T3_S6;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER <= 9) goto L_T3_S7;
    if (getskilllv(TMW2_SKILLPERMIT) == 2 && MAGIC_LVL >= 3 && ST_TIER == 10) goto L_T3_Final;
    mesc l("Error, error, L_T3_S0 General Error, REPORT ME");
    close;

L_T3_S2:
    mesn;
    mesq l("Hurry up! Bring a @@ from Barzil in Halinarzo!! You only have @@ left!", getitemlink(SunnyCrystal), FuzzyTime(QUEST_ELEVARTEMPO,2,2));
    close;

L_T3_S3:
    if (countitem(SunnyCrystal) == 0) {
        mesn;
        mesq l("Where's the Sunny Crystal? Hurry up, you only have @@ left!", FuzzyTime(QUEST_ELEVARTEMPO,2,2));
    }
    delitem SunnyCrystal, 1;
    ST_TIER=4;
    getexp 250, 0;
    mesn;
    mesq l("Good, you did it!");
    next;
    mesn;
    mesc l("*chants more words, while the crystal hovers the potion*");
    next;

L_T3_S4:
    mesn;
    mesq l("I will need many Mauve Herbs! Do you have them with you? If you don't have enough, we'll lose everything! You need at most @@!", BaseLevel+40);
    select
        rif(countitem(MauveHerb), l("Yes, I have herbs. I assume the risks.")),
        l("No I don't have herbs. I'll be back.");

    if (@menu == 2)
        close;

    if (gettimetick(2) > QUEST_ELEVARTEMPO) goto L_T3_Fail;
    .@req=rand2(BaseLevel-20, BaseLevel+40);
    // Minimum is 40, max is unknown, defaults to 100

    mesn;
    mesq l("I need @@ Herbs!", .@req);
    //next; // If you comment this next, you'll allow players to logout and prevent penalty.
    mes "";

    if (countitem(MauveHerb) < .@req) goto L_T3_Fail;
    delitem MauveHerb, .@r;
    ST_TIER=5;
    getexp .@r*3, 0;
    //getitem MagicPotion, 1;

    mesc l("You quickly give her the herbs, and she skillfully mix them on a potion.");
    next;
    // You'll get a random amount of time, based on spent herbs
    // Usually, 10~73 sec, being 73 sec = 1m13s
    QUEST_ELEVARTEMPO=QUEST_ELEVARTEMPO+rand2(10,.@r-27);

L_T3_S5:
    mesn;
    mesq l("Good! Last step! West of Hurnscald, there is a magic fountain.");
    next;
    mesn;
    mesq l("Talk to the Fountain. Pour the potion on it. I advise you to put all your points on int if possible.");
    next;
    mesn;
    mesq l("Hurry up, you'll run out of time in @@!", FuzzyTime(QUEST_ELEVARTEMPO,2,2));
    close;

L_T3_S6:
    ST_TIER=7;
    mesn;
    mesq l("You did it! You're now on the last stage of this BORING and LONG quest!");
    next;

L_T3_S7:
    mesn;
    mesq l("Jesusaves wrote a grimorie, with ancient secrets of our world.");
    next;
    mesn;
    mesq l("Captain Nard have it. Fetch it with him! Quick, you only have @@ left!", FuzzyTime(QUEST_ELEVARTEMPO,2,2));
    close;

L_T3_Final:
    skill TMW2_SKILLPERMIT, 3, 0;
    getexp 40000, 0; // Yes, 40k experience points. Waw.
    mesc l(".:: Congratulations! ::."), 2;
    mesc l("You have completed the Jesusaves Grimorium Quest!"), 2;
    next;
    mesn;
    mesq l("Keep the Grimorie with you. It's a rare book which holds data from all others. The book shall guide your advances!");
    next;
    mesn;
    mesq lg("Yes, courageous and worthy adventurer. You did well!");
    next;
    mesn;
    mesc l("*sigh*");
    mesq l("Now I can turn in my report to Professor Volrtaw... I should not have stayed behind the classes.");
    close;

/// Fail handlers
L_T3_Fail:
    if (ST_TIER == 3) {
        if (countitem(SunnyCrystal) > 0) {
            delitem SunnyCrystal, 1;
        } else {
            mesn;
            mesc l("WARNING. YOU ARE CHEATING THE SUNNY CRYSTAL QUEST."), 1;
            next;
            mesn;
            mesc l("YOU WILL BE PENALIZED WITH 60% OF HEALTH."), 1;
            mesc l("IF YOU DIE, YOU'LL SUFFER THE EXP PENALTY."), 1;
            percentheal -60, -100;
            close;
        }
    }

    if (ST_TIER == 10) {
        if (countitem(JesusalvaGrimorium) > 0) {
            delitem JesusalvaGrimorium, 1;
        } else {
            mesn;
            mesc l("WARNING. YOU ARE CHEATING THE GRIMORIE QUEST."), 1;
            next;
            mesn;
            mesc l("YOU WILL BE PENALIZED WITH 70% OF HEALTH."), 1;
            mesc l("IF YOU DIE, YOU'LL SUFFER THE EXP PENALTY."), 1;
            percentheal -70, -100;
            close;
        }
    }

    mesc l(".:: Mission Failed ::."), 1;
    mesc l("You ran out of time."), 1;
    mes "";
    mes l("You should have gotten here @@.", FuzzyTime(QUEST_ELEVARTEMPO,0,2));
    ST_TIER=1;
    close;

/// Core code
OnTimer1000:
    domovestep;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SorcererRobe);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    //setunitdata(.@npcId, UDT_WEAPON, JeansShorts);
    setunitdata(.@npcId, UDT_HAIRSTYLE, any(8,11,20));
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);

    // Small movement
    initpath "move", 47, 44,
             "dir", UP, 0,
             "wait", 7, 0,
             "move", 48, 46,
             "dir", DOWN, 0,
             "wait", 7, 0,
             "move", 49, 44,
             "dir", UP, 0,
             "wait", 7, 0,
             "move", 47, 44;
    initialmove;
    initnpctimer;

    .sex=G_FEMALE;
    .distance=5;
    end;
}

