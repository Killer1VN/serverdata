// TMW2 script.
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Luca, of Physical Science Class.

003-0,34,41,0	script	Luca	NPC_PLAYER,{
    if (!MAGIC_LVL) goto L_NoMagic;
    mes l(".:: Physical Sciences Class ::.");
    mesc l("Specialized in skills with weapon-based damage and Assassination.");
    next;
    mesn;
    do {
    mesc l("You have @@ magic skill points available.", sk_points());
    next;
    mesc l("Falkon Punch - Bash your weapon against your enemies with raised damage and accuracy.");
    mesc l("Supreme Attack - Cause a very strong attack with lowered accuracy.");
    mesc l("Arrow Shower - Shoot FIVE arrows or bullets to the air and cause Area Of Effect Damage.");

    mesc l("Counter Attack - Next attack will be retaliated, with twice critical ratio.");

    mesc l("Ground Strike - Hit the ground, exploding the surroundings and disabling enemies.");
    mesc l("Sharpshooter - Shoot an arrow or bullet which damages everything on its way.");
    menuint
        l("Falkon Punch"), SM_BASH,
        l("Supreme Attack"), MC_MAMMONITE,
        l("Arrow Shower"), AC_SHOWER,
        l("Counter Attack"), KN_AUTOCOUNTER,
        l("Ground Strike"), ASC_METEORASSAULT,
        l("Sharpshooter"), SN_SHARPSHOOTING,
        l("Cancel"), 0;
    mes "";

    switch (@menuret) {
        case SM_BASH:
            if (!mlearn(SM_BASH, 10, 1, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        case MC_MAMMONITE:
            if (!mlearn(MC_MAMMONITE, 10, 1, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        case AC_SHOWER:
            if (!mlearn(AC_SHOWER, 10, 1, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        case KN_AUTOCOUNTER:
            if (!mlearn(KN_AUTOCOUNTER, 5, 2, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        case ASC_METEORASSAULT:
            if (!mlearn(ASC_METEORASSAULT, 3, 2, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        case SN_SHARPSHOOTING:
            if (!mlearn(SN_SHARPSHOOTING, 1, 3, FluoPowder, 3))
                mesc l("You do not meet all requisites for this skill."), 1;
            break;
        default:
            if (@menuret)
                Exception("ERROR skill not implemented", RB_DEFAULT|RB_SPEECH);
            else
                closeclientdialog;
            break;
    }
    } while (@menuret);
    close;

L_NoMagic:
    next;
    mesn;
    mesq l("Your lack of magical power is critical. I dare say, you might never in your life get access to a Mana Stone.");
    next;
    mesn;
    mesq l("Besides the Magic Council, Andrei Sakar have his own Mana Stone, but I doubt he would train the likes of you, or share his Mana Stone.");
    next;
    mesn;
    mesq l("Perhaps, in the city, someone knows rumors about Mana Stones and can teach you. Other than that, you're on your own.");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, BromenalChest);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 21);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 6);

    .sex = G_MALE;
    .distance = 5;
    end;
}
