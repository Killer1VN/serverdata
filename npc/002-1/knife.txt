// Evol scripts.
// Authors:
//    Ablu
//    Saulc
//    Qwerty Dragon
// Description:
//    Knife on the table.
// Variable:
//    ShipQuests_Knife
// Values:
//    0   Default, not taken.
//    1   Knife taken.

002-1,31,28,0	script	#knife	NPC_KNIVES,{
    .@q = getq(ShipQuests_Knife);
    if (.@q) end;

    mesn l("Narrator");
    mesc l("There are some old rusty knives on the table. Would you like to take one?");
    next;

    menu
        l("Yeah!"), L_Give,
        l("Nah."), -;

    closedialog;
    close;

L_Give:
    mes "";
    inventoryplace RustyKnife, 1;

    setq ShipQuests_Knife, 1;
    getitem RustyKnife, 1;

    mesn l("Narrator");
    mesc l("To open your inventory, use the F3 key or use your mouse to select it in the above menu in your client.");
    next;
    mesc l("When your inventory is open, you can equip an item by selecting it and clicking 'Equip'. You can do the same to unequip an item by clicking on 'Unequip'.");
    next;
    mesc l("Items have different effects. Some will heal you, some can be used as weapons or armor, and some can be sold for gold.");

    close;

OnInit:
    .distance = 2;
    end;
}
