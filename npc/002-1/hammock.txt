// TMW2 Script
// Notes:
//    Hammocks 1~5 were deleted during 002-1 ship hold remapping
// Evol scripts.
// Author:
//    Reid
// Description:
//    Animated hammock at the mid level of the ship.

002-1,37,34,0	script	#hammock6	NPC_RIGHT_HAMMOCK,1,0,{

OnTouch:
    hamTouchLeft;

OnUnTouch:
    hamUnTouch;

OnTimer5440:
    hamTimerLeft;
}

002-1,37,36,0	script	#hammock7	NPC_RIGHT_HAMMOCK,1,0,{

OnTouch:
    hamTouchLeft;

OnUnTouch:
    hamUnTouch;

OnTimer5440:
    hamTimerLeft;
}

002-1,37,38,0	script	#hammock8	NPC_RIGHT_HAMMOCK,1,0,{

OnTouch:
    hamTouchLeft;

OnUnTouch:
    hamUnTouch;

OnTimer5440:
    hamTimerLeft;
}

002-1,32,40,0	script	#hammock9	NPC_LEFT_HAMMOCK,1,0,{

OnTouch:
    hamTouchLeft;

OnUnTouch:
    hamUnTouch;

OnTimer5440:
    hamTimerLeft;
}

002-1,37,40,0	script	#hammock10	NPC_RIGHT_HAMMOCK,1,0,{

OnTouch:
    hamTouchLeft;

OnUnTouch:
    hamUnTouch;

OnTimer5440:
    hamTimerLeft;
}

002-1,58,25,0	duplicate(#hammock9)	#hammock1	NPC_LEFT_HAMMOCK,1,0
002-1,58,27,0	duplicate(#hammock9)	#hammock2	NPC_LEFT_HAMMOCK,1,0
002-1,58,29,0	duplicate(#hammock9)	#hammock3	NPC_LEFT_HAMMOCK,1,0

