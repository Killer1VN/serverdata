// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Temporary, placeholder.

016-1,19,29,0	script	Captain	NPC_NARD,{
    .@price=410;
    if (BaseLevel < 20)
        goto L_TooWeak;

    mesn;
    mesq l("Hi @@.", strcharinfo(0));
    next;
    mesq l("You are currently at @@.", LOCATION$);
    mesc l("Note: Onboard, Destructive, Fire, and AoE Magic are NOT allowed.");
    mes "";

        menu
            rif(LOCATION$ != "Tulim", l("To Tulimshar.")), L_TTulim,
            rif(LOCATION$ != "Hurns", l("To Hurnscald.")), L_THurns,
            rif(LOCATION$ != "Nival", l("To Nivalis.")), L_TNival,
            l("No, I'll save my money."), -;

    close;

///// ---------- Tulimshar ----------
L_TTulim:
    .@x=(reputation("Tulim")+reputation(LOCATION$))/2;
    if (.@x >= 50) .@x+=10;
    .@price-=min(400, (.@x/10)*40);

    mes "";
    mesn;
    mesq l("It'll cost you @@ GP.", .@price);
    mes "";

    if (Zeny < .@price) {
        mes l("You still need @@ GP to afford it.", (.@price-Zeny));
        close;
    }

    if (askyesno() != ASK_YES)
        close;

    Zeny=Zeny-.@price;
    PC_DEST$="Tulim";
    @timer_navio_running = 1;

    mes "";
    mesn;
    mesq l("Tulimshar, right? The oldest human city-state!");
    next;
    mesq l("I was planning to go there soon, anyway. All aboard!");
    close2;
    goto L_DoWarp;







///// ---------- Hurnscald ----------
L_THurns:
    .@x=(reputation("Hurns")+reputation(LOCATION$))/2;
    if (.@x >= 50) .@x+=10;
    .@price-=min(400, (.@x/10)*40);

    mes "";
    mesn;
    mesq l("It'll cost you @@ GP.", .@price);
    mes "";

    if (Zeny < .@price) {
        mes l("You still need @@ GP to afford it.", (.@price-Zeny));
        close;
    }

    if (askyesno() != ASK_YES)
        close;

    Zeny=Zeny-.@price;
    PC_DEST$="Hurns";

    mes "";
    mesn;
    mesq l("Hurnscald? Small farming towns are always nice to visit.");
    next;
    mesq l("I was planning to go there soon, anyway. All aboard!");
    close2;
    goto L_DoWarp;







///// ---------- Nivalis ----------
L_TNival:
    .@x=(reputation("Nival")+reputation(LOCATION$))/2;
    if (.@x >= 50) .@x+=10;
    .@price-=min(400, (.@x/10)*40);

    // Nivalis Liberation Day. Zero could cause weird bugs.
    if (!$NIVALIS_LIBDATE)
        .@price=1;

    // Maybe this destination is NOT AVAILABLE
    if (!$NLIB_DAY && !$NIVALIS_LIBDATE) {
        mesn;
        mesq l("I would love to, but the Monster King laid siege there.");
        mesc l("A Game Master is required to begin the Liberation Day."), 1;
        close;
    }

    mes "";
    mesn;
    mesq l("It'll cost you @@ GP.", .@price);
    mes "";

    if (Zeny < .@price) {
        mes l("You still need @@ GP to afford it.", (.@price-Zeny));
        close;
    }

    if (askyesno() != ASK_YES)
        close;

    Zeny=Zeny-.@price;
    PC_DEST$="Nival";

    mes "";
    mesn;
    mesq l("Nivalis? It's frozen during the whole year! I hope you have good ice gear and a high level...");
    next;
    mesq l("I was planning to go there soon, anyway. All aboard!");
    close2;
    goto L_DoWarp;







///// ---------------- Core Utils
L_TooWeak:
    mesn;
    mesq l("The sea route I take is very dangerous, and full of pirates. You're too weak to travel with me.");
    close;

L_DoWarp:
    addtimer nard_time(PC_DEST$), "#MarineShip::OnEvent";
    @timer_navio_running = 1;
    warp "016-6", 40, 32;

    // 10% base chance of Pirate Attack!
    // Each level INCREASES this in 0.1%.
    // So for a level 40 player, chances are 14%.
    if (rand(1, 10000) < 1000+(BaseLevel*10))
        addtimer rand(3000,6000), "#MarineShipAttack::OnEvent";
    end;


OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;

}
