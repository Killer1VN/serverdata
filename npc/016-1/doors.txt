// TMW2 Scripts.
// Author:
//    Jesusalva
// Description:
//    La Marine ship Doors NPCs.

016-1,21,25,0	script	#MarineToOutside	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "Hurns") {
        warp "012-1", 156, 65;
        close;
    }
    if (LOCATION$ == "Tulim") {
        warp "003-1", 119, 25;
        close;
    }
    if (LOCATION$ == "Nival") {
        warp "019-2", 119, 113;
        close;
    }
    mesc l("Oh noes! The door is locked!! Quick! Call a GM!!!"), 1;
    close;
}


