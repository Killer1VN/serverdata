// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script Core Functions
//
// Used for our pseudo-magic.
// These are only helpers, you can add more restrictions and effects freely.
// Important Variables:
//  MAGIC_EXP
//      Current mana magic experience
//  LAST_SKILL
//      Last Mana Skill used
//  MAGIC_LVL
//      Maximum tier of usable magic, capped by Mana Stone
//  MAGIC_SUBCLASS
//      Bitmasked magic subclass.

// SkillID, EXP Points
function	script	GetManaExp	{
    .@sk=getarg(0);
    .@pt=getarg(1);
    .@bonus=rand(0,getskilllv(TMW2_SAGE)*3/2);
    if (LAST_SKILL == .@sk) {
        .@pt=limit(0, (.@pt+.@bonus)/3, 1);
        .@bonus=0;
    }
    LAST_SKILL=.@sk;
    MAGIC_EXP=MAGIC_EXP+.@pt+.@bonus;
    return;
}

// SkillID, Mana{, MP per level}
function	script	MagicCheck	{
    // PRE EXECUTION
    // Load Variables
    .@sk=getarg(0);
    .@mp=getarg(1);
    .@amp=getarg(2,0);

    // Check Skill
    if (getskilllv(.@sk) < 1)
        return 0;

    // Load mana cost
    .@mp=.@mp+getskilllv(.@sk)*.@amp-.@amp;

    // Check mana
    if (readparam(Sp) < .@mp) {
        dispbottom l("Insufficient mana: @@/@@.", readparam(Sp), .@mp);
        return 0;
    }

    // Apply mana cost
    heal 0, 0-.@mp;

    return 1;
}

// SkillID, MobID{, SkillLevelPerMob=2{, Level Override}}
function	script	SummonMagic	{
    .@sk=getarg(0);
    .@id=getarg(1);
    .@adj=getarg(2,2);
    .@lv=getarg(3,getskilllv(.@sk));

    if (.@adj < 1) {
        debugmes "\033[31mInvalid MobPerSkillLevel for SummonMagic (.@adj): "+.@adj+"\033[0m";
        dispbottom l("Invalid parameter specified, blame saulc.");
        end;
    }

    // Cause effect
    // Summoned monsters live from 45 to 60 seconds, and each skill levels grants 10s extra life
    // The 35~50 is not a defect, remember skill starts at level 1...
    for (.@i = 0; .@i < (.@lv+(.@adj-1))/.@adj; .@i++) {
    	.@mids=summon("Summoned Monster", .@id, rand(35,50)+.@lv*10);
        .@bhp=getunitdata(.@mids, UDT_MAXHP);
        .@lvx=max(0, (.@lv-1)*.@bhp/50);
        setunitdata(.@mids, UDT_MAXHP,    .@bhp+.@lvx);
        setunitdata(.@mids, UDT_HP,       .@bhp+.@lvx);
    }
    dispbottom l("All monsters summoned!");
    return;
}

// mescordialog(text, color, {dialog=1})
function	script	mescordialog	{
    if (getarg(2, true))
        mesc getarg(0), getarg(1);
    else
        dispbottom col(getarg(0), getarg(1));
    return;
}

// ShowAbizit({dialog=1})
function	script	ShowAbizit	{
    .@dial=getarg(0, true);
    if (.@dial)
        mesn l("Current Magic Control");

    .@val=MAGIC_EXP+rand(-MAGIC_LVL*5, MAGIC_LVL*5);
    .@base=(MAGIC_LVL**3);
    if (.@val > .@base*5)
        mescordialog l("You are perfectly in control of your magic."), 3, .@dial;
    else if (.@val > .@base*4)
        mescordialog l("You are mostly in control of your magic."), 2, .@dial;
    else if (.@val > .@base*3)
        mescordialog l("You are somewhat in control of your magic."), 4, .@dial;
    else if (.@val > .@base*2)
        mescordialog l("Your magic is more powerful than you, but you can control."), 7, .@dial;
    else if (.@val > .@base)
        mescordialog l("You still are overwhelmed by your magic."), 6, .@dial;
    else
        mescordialog l("You are completly overwhelmed by your magic."), 1, .@dial;
    return;
}

