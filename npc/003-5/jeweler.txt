// TMW2 Script
// Author:
//    Jesusalva
// Description:
//  Sells and crafts jewels
//
// Notes:
//  Gold/Silver/Copper+Tin Ore (dropped by bifs) → Pieces or Ingot, depends on what you want to forge
//  Jeweler only works with Gold/Silver/Bronze PIECES. The Ingots are for forges.
//  Jeweler can also polish raw gemstones for a comission ($$$)
//  Polished gemstones can be inserted on rings.
//  The less precious the ring, heavier and less defense value.
//
//  All three rings can be swapped by a Light Ring (you will lose any gemstone on them!).
//  Still pending discussion.

003-5,33,37,0	script	Jeweler	NPC_FEMALE,{
    mesn l("Inya, the Jeweler");
    mesq l("Hello!");
    goto L_Menu;

L_Menu:
    mes "";
    mesn strcharinfo(0);
    menu
        l("I want to shop."), -,
        l("I want to craft rings."), L_Craft,
        l("I want to create polished gemstones."), L_Gemstone,
        l("I want to remove all stuff applied to my ring!"), L_Clear,
        l("Nothing, sorry."), L_Close;

    npcshopattach(.name$);
    openshop;
    closedialog;
    close;

L_Craft:
    mes "";
    mesn l("Inya, the Jeweler");
    mesq l("Of course, I actually have two options:");
    mes l("- 5 @@, with 1200 GP, for a @@.", getitemlink(GoldPieces), getitemlink(GoldenRing));
    mes l("- 5 @@, with 1200 GP, for a @@.", getitemlink(SilverIngot), getitemlink(SilverRing)); // TODO
    next;
    menu
        rif(countitem(GoldPieces) >= 5 && Zeny >= 1200, l("Yes, deal. Gimme the gold ring.")), -,
        rif(countitem(SilverIngot) >= 5 && Zeny >= 1200, l("Yes, deal. Gimme the silver ring.")), -,
        l("...I'll be back later."), L_Close;

    switch (@menu) {
        case 1:
            inventoryplace GoldenRing, 1;
            delitem GoldPieces, 5;
            Zeny=Zeny-1200;
            getitem GoldenRing, 1;
            getexp 100, 25;
            break;
        case 2:
            inventoryplace SilverRing, 1;
            delitem SilverIngot, 5; // TODO
            Zeny=Zeny-1200;
            getitem SilverRing, 1;
            getexp 50, 15;
            break;
    }
    mes "";
    mesn l("Inya, the Jeweler");
    mesq l("Here you go! Anything else?");
    next;
    goto L_Menu;

L_Gemstone:
    mes "";
    mesn l("Inya, the Jeweler");
    mesq l("Polished gemstones can be dragged and insert on rings. They'll give special stats to rings, but these gems cannot be removed easily.");
    mesq l("I charge a service fee of @@ GP, and two gemstones.", 300);
    next;
    if (Zeny < 300) {
        mesn l("Inya, the Jeweler");
        mesq lg("You clearly don't have money, so let's not bother with that right now.");
        next;
        goto L_Menu;
    }
    if (!countitem(GoldenRing) && !countitem(SilverRing)) {
        mesn l("Inya, the Jeweler");
        mesq l("You don't have a ring but I can polish the gemstones nonetheless.");
        next;
    }
    select
        l("Not today, sorry."),
        rif(countitem(Diamond) >= 2 , l("Diamond (+2 vit)")),
        rif(countitem(Ruby) >= 2    , l("Ruby (+2 str)")),
        rif(countitem(Emerald) >= 2 , l("Emerald (+2 luck)")),
        rif(countitem(Sapphire) >= 2, l("Sapphire (+2 int)")),
        rif(countitem(Topaz) >= 2   , l("Topaz (+2 agi)")),
        rif(countitem(Amethyst) >= 2, l("Amethyst (+2 dex)"));

    mes "";
    if (@menu == 1)
        goto L_Menu;

    // As usual, I don't care with cheaters, so if you somehow cheat money or gemstones, YOU WILL LOSE ALL REAGENTS. No refunds.
    inventoryplace PolishedDiamond-2+@menu, 1;
    delitem Diamond-2+@menu, 2;
    Zeny-=300;
    getitem PolishedDiamond-2+@menu, 1;
    getexp 800, 0;
    goto L_Menu;


/* TODO Special rings
L_Pearl:
    inventoryplace GoldenPearlRing;
    delitem GoldenRing, 1;
    Zeny=Zeny-1000;
    delitem Pearl, 3;
    getitem GoldenPearlRing, 1;
    getexp 1000, 0;
    goto L_Menu;

L_BlackPearl:
    inventoryplace GoldenBlackPearlRing;
    delitem GoldenRing, 1;
    Zeny=Zeny-1000;
    delitem BlackPearl, 1;
    getitem GoldenBlackPearlRing, 1;
    getexp 1000, 0;
    goto L_Menu;
*/

L_Clear:
    mesn l("Inya, the Jeweler");
    mesq l("Oh... Of course! I'll even do this for free!");
    next;
    mesn l("Inya, the Jeweler");
    mesq l("Just be really, REALLY sure you want to do it. I mean, the gemstone will break.");
    mesq l("It's not warranted you'll be able to get the powder back, either.");
    next;
    mes "##B" + l("Drag and drop an item from your inventory.") + "##b";
    .@item_index = requestitemindex();
    if (.@item_index < 0)
        goto L_Menu;

    // Check if we received an item
    getinventorylist;
    .@item=@inventorylist_id[.@item_index];

    // Is it the one we're looking for?
    if (.@item != GoldenRing && .@item != SilverRing) {
        mesn;
        mesq l("Well, that's not a ring.");
        next;
        goto L_Menu;
    }

    // Retrieve the gemstone ID. Must be on slot 1 (aka. 0)!
    .@gem=getcardbyindex(.@item_index, 0);
    // Skip bound rings. It would be stupid '-'
    if (!checkbound(.@item)) {
        failedremovecardsindex .@item_index, 1;
        // No inventoryplace check here
        // DiamondPowder = 858. PolishedDiamond = 5031
        if (.@gem > 5000)
            getitem .@gem-4173, 1;
        mesn;
        mesq l("Well... Here you go!");
        next;
    } else {
        mesn;
        mesq l("I don't work with bound items.");
        next;
    }

    goto L_Menu;

L_Close:
    closedialog;
    goodbye;
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 3);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 8);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;

    tradertype(NST_ZENY);
    sellitem WeddingRing, 5000;
    sellitem GoldenBlackPearlRing, 500000; // I'm joking of course. Don't tell me you'll pay half MILLION for... this?
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;

}
