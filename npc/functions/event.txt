// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Controls world events so Saulc and I can take vacations.
//    The event specifics are more often scattered accross seasons.txt, command/event.txt
//    003-1/events.txt, soulmenhir.txt and, of course, the event maps (if any).
//    Schedule, if needed to change, can be found in the OnInit loop.
//    An event, obviously, cannot last more than 7... 15 days at WORST.
//    Therefore, month checks aren't included in day checks.

-	script	sPatrick	NPC_HIDDEN,{
OnInit:
    setarray .maps$,
                    "005-1",
                    "013-1",
                    "014-1",
                    "014-2",
                    "014-3",
                    "014-4",
                    "014-5",
                    "017-1",
                    "018-2",
                    "018-4",
                    "018-5",
                    "soren";
    end;
OnMyMobDeath:
    end;
OnClock0000:
OnClock6000:
OnClock1200:
OnClock1500:
OnClock1800:
OnClock2100:
    .@d=gettime(GETTIME_DAYOFMONTH);
    // Patrick Day should work only in 2 luck days according to Saulc
    // If this is required use $PATRICK_DAYCTRL so the days before $@PATRICK_DAYMAX
    // act with 100% chances (determinism).
    // As it is being spawn 4 times at a day (like TMW-BR events), because it is
    // flatly and outright IMPOSSIBLE to add a permanent spawn, that is not required.
    if ($EVENT$ == "St. Patrick" && .@d <= $@PATRICK_DAYMAX) {
        for (.@i=0; .@i < getarraysize(.maps$); .@i++) {
            .@m$=.maps$[.@i];
            .@x=getmapinfo(MAPINFO_SIZE_X, .@m$)-20;
            .@y=getmapinfo(MAPINFO_SIZE_Y, .@m$)-20;

            // Remove previously spawned clovers
            killmonster(.@m$, "sPatrick::OnMyMobDeath");
            // It is one clover for each 225 tiles (about a 25x25 square)
            .@maparea=(.@x-20)*(.@y-20);
            .@mobs=max(1, .@maparea/225);
            areamonster .@m$, 20, 20, .@x, .@y, strmobinfo(1, StPatricksClover), StPatricksClover, .@mobs, "sPatrick::OnMyMobDeath";
        }
    }
    end;

// Remove previously spawned clovers for event end
// And then disables the NPC
OnCleanUp:
    for (.@i=0; .@i < getarraysize(.maps$); .@i++) {
        .@m$=.maps$[.@i];
        killmonster(.@m$, "sPatrick::OnMyMobDeath");
    }
    disablenpc "sPatrick";
    end;
}

000-0,0,0,0	script	#EventCore	NPC_HIDDEN,{
    end;

// Event Schedule (all dates are inclusive)
OnInit:
    .valentine_stday=12;
    .valentine_stmon=FEBRUARY;
    .valentine_endday=15;
    .valentine_endmon=FEBRUARY;

    .patrick_stday=15;
    .patrick_stmon=MARCH;
    .patrick_endday=19;
    .patrick_endmon=MARCH;

    .easter_stday=17;
    .easter_stmon=APRIL;
    .easter_endday=24;
    .easter_endmon=APRIL;

    .worker_stday=27;
    .worker_stmon=APRIL;
    .worker_endday=3;
    .worker_endmon=MAY;

    .xmas_stday=19;
    .xmas_stmon=DECEMBER;
    .xmas_endday=2;
    .xmas_endmon=JANUARY;

    // Some setup
    disablenpc "sPatrick";

// Check for events every midnight
OnClock0000:
    .@d=gettime(GETTIME_DAYOFMONTH);
    .@m=gettime(GETTIME_MONTH);

    // Handle Valentine Day
    if (.@m >=.valentine_stmon && .@m <= .valentine_endmon) {
        // It's tomorrow: Send out an announcement
        if (.@d == .valentine_stday - 1) {
            channelmes("#world", "Valentine Day will start tomorrow!");
            announce ("Valentine Day will start tomorrow!"), bc_all|bc_npc;
        }
        // Last Day: Send out an announcement
        if (.@d == .valentine_endday) {
            channelmes("#world", "It's the last day for Valentine Event! Hurry up!");
            announce ("It's the last day for Valentine Event! Hurry up!"), bc_all|bc_npc;
        }
        // Begin the event
        if (.@d == .valentine_stday) {
            channelmes("#world", "Valentine Day begun!");
            announce ("Valentine Day begun!"), bc_all|bc_npc;
            $EVENT$="Valentine";
            $@VALENTINE_LOVELETTER = htnew();
            $@VALENTINE_GIFTSTACKS = htnew();
        }
        // End the event the day after
        if (.@d == .valentine_endday+1) {
            channelmes("#world", "Valentine Day ended!");
            announce ("Valentine Day ended!"), bc_all|bc_npc;
            sClear();
            $EVENT$="";
        }
    }


    // Handle St. Patrick Day
    // During this day, a special four leaf clover is spawned daily, at 4 times in the day
    // in all forests: 6:00, 12:00, 18:00 and 00:00.
    // $PATRICK_DAYMAX
    if (.@m >=.patrick_stmon && .@m <= .patrick_endmon) {
        // It's tomorrow: Send out an announcement
        if (.@d == .patrick_stday - 1) {
            channelmes("#world", "St. Patrick Day will start tomorrow!");
            announce ("St. Patrick Day will start tomorrow!"), bc_all|bc_npc;
        }
        // Last Day: Send out an announcement
        if (.@d == .patrick_endday) {
            channelmes("#world", "It's the last day for St. Patrick Event! Hurry up!");
            announce ("It's the last day for St. Patrick Event! Hurry up!"), bc_all|bc_npc;
        }
        // Begin the event
        if (.@d == .patrick_stday) {
            channelmes("#world", "St. Patrick Day begun!");
            announce ("St. Patrick Day begun!"), bc_all|bc_npc;
            $EVENT$="St. Patrick";
            $@PATRICK_DAYMAX=.patrick_endday;
            enablenpc "sPatrick";
            enablenpc "Patrick Saulc";
            enablenpc "St. Patrick Gold Pot";
            donpcevent "St. Patrick Gold Pot::OnForcedMove";
        }
        // End the event the day after
        if (.@d == .patrick_endday+1) {
            channelmes("#world", "St. Patrick Day ended!");
            announce ("St. Patrick Day ended!"), bc_all|bc_npc;
            sClear();
            $EVENT$="";
            donpcevent "sPatrick::OnCleanUp";
            disablenpc "Patrick Saulc";
        }
    }

    // Handle Easter
    if (.@m >=.easter_stmon && .@m <= .easter_endmon) {
        // It's tomorrow: Send out an announcement
        if (.@d == .easter_stday - 1 && .@m == .easter_stmon) {
            channelmes("#world", "Easter will start tomorrow!");
            announce ("Easter will start tomorrow!"), bc_all|bc_npc;
        }
        // Last Day: Send out an announcement
        if (.@d == .easter_endday && .@m == .easter_endmon) {
            channelmes("#world", "It's the last day for Easter Event! Hurry up!");
            announce ("It's the last day for Easter Event! Hurry up!"), bc_all|bc_npc;
        }
        // Begin the event
        if (.@d == .easter_stday && .@m == .easter_stmon) {
            channelmes("#world", "Easter begun!");
            announce ("Easter begun!"), bc_all|bc_npc;
            $EVENT$="Easter";
        }
        // End the event the day after
        if (.@d == .easter_endday+1 && .@m == .easter_endmon) {
            channelmes("#world", "Easter ended!");
            announce ("Easter ended!"), bc_all|bc_npc;
            sClear();
            $EVENT$="";
        }
    }

    // Handle Worker Day
    if (.@m >=.worker_stmon && .@m <= .worker_endmon) {
        // It's tomorrow: Send out an announcement
        if (.@d == .worker_stday - 1 && .@m == .worker_stmon) {
            channelmes("#world", "Worker Day will start tomorrow!");
            announce ("Worker Day will start tomorrow!"), bc_all|bc_npc;
        }
        // Last Day: Send out an announcement
        if (.@d == .worker_endday && .@m == .worker_endmon) {
            channelmes("#world", "It's the last day for Worker Day Event! Hurry up!");
            announce ("It's the last day for Worker Day Event! Hurry up!"), bc_all|bc_npc;
        }
        // Begin the event
        if (.@d == .worker_stday && .@m == .worker_stmon) {
            channelmes("#world", "Worker Day begun!");
            announce ("Worker Day begun!"), bc_all|bc_npc;
            $EVENT$="Worker Day";
        }
        // End the event the day after
        if (.@d == .worker_endday+1 && .@m == .worker_endmon) {
            channelmes("#world", "Worker Day ended!");
            announce ("Worker Day ended!"), bc_all|bc_npc;
            sClear();
            $EVENT$="";
        }
    }


    // Handle Christmas
    if (.@m >=.xmas_stmon && .@m <= .xmas_endmon) {
        // It's tomorrow: Send out an announcement
        if (.@d == .xmas_stday - 1 && .@m == .xmas_stmon) {
            channelmes("#world", "Christmas will start tomorrow!");
            announce ("Christmas will start tomorrow!"), bc_all|bc_npc;
        }
        // Last Day: Send out an announcement
        if (.@d == .xmas_endday && .@m == .xmas_endmon) {
            channelmes("#world", "It's the last day for Christmas Event! Hurry up!");
            announce ("It's the last day for Christmas Event! Hurry up!"), bc_all|bc_npc;
        }
        // Begin the event
        if (.@d == .xmas_stday && .@m == .xmas_stmon) {
            channelmes("#world", "Christmas begun!");
            announce ("Christmas begun!"), bc_all|bc_npc;
            $EVENT$="Christmas";
            query_sql("DELETE FROM `quest` WHERE `quest_id` = '"+SQuest_Christmas+"'");
            sChristmas();
        }
        // End the event the day after
        if (.@d == .xmas_endday+1 && .@m == .xmas_endmon) {
            channelmes("#world", "Christmas ended!");
            announce ("Christmas ended!"), bc_all|bc_npc;
            sClear();
            $EVENT$="";
        }
    }
    end;
}

