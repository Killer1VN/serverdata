// TMW2 script
// Evol functions.
// Author:
//    Reid, Jesusalva
// Description:
//    Function setting the player language

function	script	asklanguage	{

    .@nb_language = 8;

    switch (getarg(0, 0))
    {
        case LANG_ON_SEA:
            setarray .@messages$[0], "I hear you... (English)", // English
                                     "Je vous entends... (Français)", // French
                                     "Да я вас слышу... (Русский)", // Russian
                                     "Te oigo... (Español)", // Spanish
                                     "Eu te ouço... (Português)", // Portuguese
                                     "Ich höre euch... (Deutsch)", // German
                                     "Słyszę cię... (Polski)", // Polish
                                     "Vi sento... (Italiano)", // Italian
                                     "Mi aŭdas vin... (Angle)"; // Esperanto
            break;
        case LANG_IN_SHIP:
            setarray .@messages$[0], "I speak English.", // English
                                     "Je parle français.", // French
                                     "Я говорю на русском.", // Russian
                                     "Hablo Español.", // Spanish
                                     "Eu falo Português.", // Portuguese
                                     "Ich spreche Deutsch.", // German
                                     "Mówię po polsku.", // Polish
                                     "Parlo Italiano.", // Italian
                                     "Mi parolas Esperanton."; // Esperanto
            break;
        default:
            return;
    }

    setarray .@flags$[0],    "flags/en",
                             "flags/fr",
                             "flags/ru",
                             "flags/es",
                             "flags/pt_BR",
                             "flags/de",
                             "flags/pl",
                             "flags/it",
                             "flags/eo";

    .@menustr$ = "";
    .@separator$ = ":";

    for (.@i = 0; .@i <= .@nb_language; .@i++)
    {
        if (.@i == .@nb_language) {
            .@separator$ = "";
        }
        .@menustr$ = .@menustr$ + .@flags$[.@i] + "|" + .@messages$[.@i] + .@separator$;
    }

    select(.@menustr$);

    .@lang = @menu - 1;

    if (.@lang >= 0 || .@lang <= .@nb_language) {
        Lang = .@lang;
    }

    return;
}
