// TMW2 Script
// Author:
//      Jesusalva
//      Pyndragon (inspiration)
//      Scall (inspiration)
// Description:
//      Refine functions. Includes equipment repair. In future use getequippercentrefinery(.@it, REFINE_CHANCE_TYPE_ENRICHED) too

// (No arguments)
function	script	refineMaster	{
    mesn;
    mesq l("Ah, hello there, funny face! Do you want to refine items?");
    mesc l("There's always some chance that THE ITEM WILL BREAK, beware!");
    next;
    mesn;
    mesq l("First - item must be equipped.");
    mesq l("If you succeed, the defense and/or attack will raise. If you fail, refine level goes down.");
    //mesq l("And if the item breaks, that is definitive - no repair is possible. What to refine?");
    mesq l("And if the item breaks, that's your loss. What to refine?");
    next;

    // Dark magic from hercules used, take care
    menuint
        l("Nothing, thanks"), 0,
        rif(getequipisequiped(EQI_HEAD_MID), l("My chestplate, @@", getequipname(EQI_HEAD_MID))), EQI_HEAD_MID,
        rif(getequipisequiped(EQI_HAND_R), l("My weapon, @@", getequipname(EQI_HAND_R))), EQI_HAND_R,
        rif(getequipisequiped(EQI_HAND_L), l("My shield, @@", getequipname(EQI_HAND_L))), EQI_HAND_L,
        rif(getequipisequiped(EQI_HEAD_TOP), l("My helmet, @@", getequipname(EQI_HEAD_TOP))), EQI_HEAD_TOP,
        l("Item break? Too dangerous! I don't want it!!"), 0;


	if (@menuret == 0) {
        mesn;
        //mesq l("Wait a moment - you are naked! YOU WILL DIE IN THE FREEZE! o.o");
        mesq l("Well... we'll see around!");
		close;
	}
    .@it=@menuret;
    .@item=getequipid(.@it);

    /*
    mes "##B" + l("Drag and drop an item from your inventory.") + "##b";
    .@item = requestitem();

    // Check if we received an item
    if (.@item < 1) {
        mesn;
        mesq l("Well, no risk, no gain.");
        close;
        return;
    }
    */

    // Not all items are refinable
	if (!getequipisenableref(.@it)) {
		mesn;
		mesq l("Well, sorry, but only some items can be refined.");
        mesq l("There may be some exceptions. Anyway, this item clearly cannot be refined.");
        next;
		return;
	}

    // Max refining level: 10
	if (getequiprefinerycnt(.@it) >= 10) {
		mesn;
		mesq l("This item cannot be refined further.");
        return;
	}

    .@rlv=getequiprefinerycnt(.@it);
    .@price=(.@rlv+1)*1000;
    .@amount=(.@rlv/2)+1;
    // required item
    switch (.@rlv) {
        case 0:
        case 1:
        case 2:
        case 3:
            .@rg=Wurtzite; break;
        case 4:
        case 5:
        case 6:
        case 7:
            .@amount=((.@rlv-4)/2)+1;
            .@rg=Graphene; break;
        case 8:
        case 9:
        case 10:
        default:
            .@amount=((.@rlv-8)/2)+1;
            .@rg=Arcanum; break;
    }
    mesn;
    mesq l("This @@ is a nice stuff. I can refine it for @@ GP and @@ @@.", getitemlink(.@item), .@price, .@amount, getitemlink(.@rg));
    mesc l("Success ratio is of aprox. @@ %", (100-(.@rlv*7))+rand2(-6,6));
    next;
    if (askyesno() != ASK_YES)
        close;

    // Anti-Crazyfefe™ System
    // Item is gone, 100% warranted cheater
    if (countitem(.@item) < 1) {
        mesn;
        mesc l("All cheaters must die."), 1;
        percentheal -100, -100;
        close;
    }
    // No money
    if (Zeny < .@price) {
        mesn;
        mesq l("Where's the money? Go away.");
        close;
    }
    // No reagents
    if (countitem(.@rg) < .@amount) {
        mesn;
        mesq l("Where's the reagent? You don't expect me to find it, right? Go bring them!");
        close;
    }
    // Item removed
	if (getequipisequiped(.@it) == 0) {
        mesn;
        mesc l("All cheaters must die."), 1;
        percentheal -100, -100;
        close;
    }
    // Item changed
	if (getequiprefinerycnt(.@it) != .@rlv || getequipid(.@it) != .@item) {
        mesn;
        mesc l("All cheaters must die."), 1;
        percentheal -100, -100;
        close;
    }
    ///// This should cover most of Crazyfefe tricks ¬.¬ I hope ¬.¬
    mesn;
    mesq l("HA, HA, HA! The blacksmiths from the Land of Fire are the best from the world! There's no challenge too big for us!");
    mesq l("It's time to show this shiny @@ who is the boss! HAHAHAHAHAHAHAH!", getitemlink(.@item));
    next;
    // If you cheat somewhere along the way, the script will explode
    Zeny=Zeny-.@price;
    delitem .@rg, .@amount;
    // Cheater should be locked for eternity.
	if (getequiprefinerycnt(.@it) != .@rlv || getequipid(.@it) != .@item || getequipisequiped(.@it) == 0) {
        atcommand "@jail "+strcharinfo(0);
        close;
    }

    // Were we successful?
	if (getequippercentrefinery(.@it) > rand2(100)) {
	    successrefitem .@it;
        mesn;
        mesq l("HAHAHAHAH! Shiny as new, and stronger than ever! Have fun!");
        mesc l("Weapons: +8 attack, +8 magic attack");
        mesc l("Armors: +3~5 defense");
        next;
    // Item is weakened. (1% per refine level)
    } else if (rand2(100) < .@rlv) {
		downrefitem .@it, 1;
        mesn;
        mesq l("A masterpiece!... Whaaaat, this stuff got ##Bweaker##b??");
        next;
        mesn;
        mesq l("Well, I did my best, but this had been so refined, that it was difficult to find my way.");
        next;
        mesn;
        mesq l("I'm sure I could refine this again, if you bring me the payment and the reagents again.");
        next;
    // Perhaps the item has broken? (~0.1% chance/refine level)
    } else if (rand2(10000) <= 10*(.@rlv+1)) {
		failedrefitem .@it;
        mesc l("*CRASH*");
        next;
        mesn;
        mesq l("Ops... I hammered this stuff too hard... It's junk now.");
        next;
        mesn;
        mesq l("Well, you were warned. Do you have any other stuff for me?");
        next;
    // Nothing happens
    } else {
        mesn;
        mesq l("Well, I did my best, but this had been so refined, that it was difficult to find my way.");
        next;
        mesn;
        mesq l("I'm sure I could refine this again, if you bring me the payment and the reagents again.");
        next;
    }
    return;
}


// (No arguments)
function	script	repairMaster	{
    mesn;
    mesq l("I am a really good blacksmith. In fact, there is no stuff I can't fix.");
    next;
    mesn;
	if (getbrokenid(1) == 0) {
		mesq l("I don't think you have any broken item, but if a friend of mine break your stuff, come talk to me!");
        close;
	} else {
        // Print the name of the first broken item
		mesq l("You seem to have @@ broken items and... Oh, I see you have a broken @@ here! I wanna repair it!", getbrokencount(), getitemname(getbrokenid(1)));
        .@save=getbrokenid(1);
        next;
    }
    .@price=getiteminfo(getbrokenid(1), ITEMINFO_SELLPRICE)+getiteminfo(getbrokenid(1), ITEMINFO_ELV)-(getbrokencount()*15);
    if (.@price < 100 || getbrokenid(1) == BronzeGladius)
        .@price=100;
    mesn;
    mesq l("I can fix this for you. Just pay me @@ GP and a @@. It'll take no time at all!", .@price, getitemlink(IronIngot));
    next;
    if (Zeny < .@price || countitem(IronIngot) < 1 || askyesno() != ASK_YES) {
        mes "";
        mesn;
        mesq l("Ahh, that's a pity... Well, just talk to me with enough money and I'll gladly fix that for you! %%0");
        close;
    }
    // Cheating? Well, let's try to be innovative this time
    if (.@save != getbrokenid(1)) {
        mesn;
        mesq l("Ah...! POLICE! POLICE! Quick, send this cheating thief to jail!!");
        atcommand "@jailfor 20mn "+strcharinfo(0);
        close;
    }
    Zeny=Zeny-.@price;
    delitem IronIngot, 1;
    repair(1);
    mesn;
    mesq l("Here, sparking new! Nice!!");
    if (getbrokencount() > 0) {
        next;
        return;
    } else {
        close;
    }
}
