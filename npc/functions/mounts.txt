// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Mount Renting (var MOUBOO_RENTTIME - TODO: Discount when mouboo friend )

// MoubooRent( NPC Name )
function	script	MoubooRent	{
    .@n$=getarg(0, strnpcinfo(0));
    if (MOUBOO_RENTTIME > gettimetick(2) || BaseLevel < 20) {
        mesn .@n$;
        mesq l("Moo!");
        return;
    }

    mesn .@n$;
    mesq l("Moo!");
    next;
    mesn .@n$;
    mesq l("Do you want to rent a Mouboo? It allows quick traveling!");
    next;
    menuint
        rif(Zeny > 100, l("Rent 1 day for 100 GP")), 100,
        rif(Zeny > 270, l("Rent 3 days for 270 GP")), 270,
        rif(Zeny > 600, l("Rent 7 days for 600 GP")), 600,
        rif(Zeny > 1200, l("Rent 15 days for 1200 GP")), 1200,
        rif(Zeny > 2000, l("Rent 30 days for 2000 GP")), 2000,
        l("Don't rent a mouboo."), 0;
    mes "";
    switch (@menuret) {
        case 0:
            return;
        case 100:
            .@time=86400; break;
        case 270:
            .@time=259200; break;
        case 600:
            .@time=604800; break;
        case 1200:
            .@time=1296000; break;
        case 2000:
            .@time=2592000; break;
        default:
            Exception("Invalid price: "+@menuret, RB_DEFAULT|RB_SPEECH|RB_ISFATAL);
            break;
    }
    inventoryplace RidingMouboo, 1;
    MOUBOO_RENTTIME=gettimetick(2)+.@time;
    Zeny-=@menuret;
    rentitem RidingMouboo, .@time;
    return;
}

// TortugaRent( NPC Name )
function	script	TortugaRent	{
    .@n$=getarg(0, strnpcinfo(0));
    if (MOUBOO_RENTTIME > gettimetick(2)) {
        mesn .@n$;
        mesq l("Boo!");
        return;
    }

    mesn .@n$;
    mesq l("Boo!");
    next;
    mesn .@n$;
    mesq l("Do you want to rent a Tortuga? It allows quick traveling!");
    next;
    menuint
        rif(Zeny > 50, l("Rent 1 day for 50 GP")), 50,
        rif(Zeny > 135, l("Rent 3 days for 135 GP")), 135,
        rif(Zeny > 300, l("Rent 7 days for 300 GP")), 300,
        rif(Zeny > 600, l("Rent 15 days for 600 GP")), 600,
        rif(Zeny > 1000, l("Rent 30 days for 1000 GP")), 1000,
        l("Don't rent a tortuga."), 0;
    mes "";
    switch (@menuret) {
        case 0:
            return;
        case 50:
            .@time=86400; break;
        case 135:
            .@time=259200; break;
        case 300:
            .@time=604800; break;
        case 600:
            .@time=1296000; break;
        case 1000:
            .@time=2592000; break;
        default:
            Exception("Invalid price: "+@menuret, RB_DEFAULT|RB_SPEECH|RB_ISFATAL);
            break;
    }
    inventoryplace RidingTortuga, 1;
    MOUBOO_RENTTIME=gettimetick(2)+.@time;
    Zeny-=@menuret;
    rentitem RidingTortuga, .@time;
    return;
}


