// TMW2 functions.
// Author:
//    Jesusalva
// Description:
//    Random Treasure Box Utils

function	script	TreasureBox	{
    .@id=getnpcid();
    if (RNGTREASURE_DATE[.@id] > gettimetick(2)) {
        mesc l("The chest is unlocked and empty.");
        close;
    }

    mesc l("Open the chest?");
    mesc l("Cost: 1 @@", getitemlink(TreasureKey)), 1;
    if (!countitem(TreasureKey))
        close;
    next;
    if (askyesno() == ASK_NO)
        close;

    delitem TreasureKey, 1;
    mesc l("You open the chest!");
    RNGTREASURE_DATE[.@id]=gettimetick(2)+60*15; // Minimum 15 minutes

    .@empty=getvariableofnpc(.empty, strnpcinfo(0));
    if (!.@empty) {
        TREASURE_OPEN=TREASURE_OPEN+1;
        .@t=TREASURE_OPEN;
        .@r=rand(0,10000)-(readparam2(bLuk)*2);

        // Select treasure list
        // You're warranted an ultra rare (0.1%) every 149 open chests
        // You're warranted a super rare (1%) every 50 open chests
        // There's also rares (9%), uncommons (30%) and commons (60%)
        .@ur_rate=min(10, (TREASURE_OPEN/10));
        if (.@t % 149 == 0 || .@r < .@ur_rate)
            .@loot=any(AtroposMixture, GoldenApple, DivineApple, MercBoxA, MoveSpeedPotion, Shemagh, EverburnPowder, IridiumOre, PlatinumOre);
        else if (.@t % 50 == 0 || .@r < 100)
            .@loot=any(SmokeGrenade, SnakeEgg, LachesisBrew, ArrowAmmoBox, CoinBag, SilverGift, TerraniteOre, LeadOre, TinOre, SilverOre, GoldOre, TitaniumOre, FluoPowder, AlchemyBlueprintB, AncientBlueprint);
        else if (.@r < 1000 || .@t == 0)
            .@loot=any(MoubooSteak, ClothoLiquor, Coal, RedPlushWine, HastePotion, StrengthPotion, Dagger, BronzeGift, IronOre, CopperOre, AlchemyBlueprintA);
        else if (.@r < 5000)
            .@loot=any(Croconut, Potatoz, MoubooSteak, ClothoLiquor, Coal, SmallMushroom, HastePotion, StrengthPotion, WoodenLog, LeatherPatch, Beer);
        else
            .@loot=any(FatesPotion, ChocolateBar, Plushroom, Chagashroom, RawLog, LeatherPatch, BugLeg, ScorpionStinger, SmallKnife, StrangeCoin);


        inventoryplace .@loot, 1;
        mesc l("You find @@ inside!", getitemlink(.@loot));
        getitem .@loot, 1;
        // Get Monster points for treasure hunting (20% from job level)
        if (MPQUEST)
            Mobpt+=(JobLevel/5);
    } else {
        mesc l("You find @@ inside!", l("nothing"));
    }
    return;
}

