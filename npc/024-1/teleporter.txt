// TMW2 Script
// Authors:
//    Jesusalva
// Description:
//    Link portals to soul menhirs like the teleporters from old
//    The price is temporary. This feature got in because no ship in Nivalis Port
//    PS. Anise => “Aisen” Anagram


024-1,155,80,0	script	#WarpGateFrost	NPC_NO_SPRITE,1,0,{
    end;

OnTouch:
    if (!(TELEPORTERS & TP_FROST)) {
        TELEPORTERS=TELEPORTERS|TP_FROST;
        mesn "Anise Inc.";
        mesc l("Location Registered. You are now capable to use this warp gate.");
        next;
    }
    mesc l("Where should I warp to?");
    mesc l("Cost: 1 @@", getitemlink(EverburnPowder)), 1;
    if (!countitem(EverburnPowder))
        close;
    next;
    select
        rif(TELEPORTERS & TP_FROST && 0, l("Frostia")),
        rif(TELEPORTERS & TP_HALIN, l("Halinarzo")),
        rif(TELEPORTERS & TP_LILIT, l("Lilit")),
        rif(TELEPORTERS & TP_TULIM, l("Tulimshar")),
        rif(TELEPORTERS & TP_HURNS, l("Hurnscald")),
        rif(TELEPORTERS & TP_NIVAL, l("Nivalis")),
        rif(TELEPORTERS & TP_ARTIS, l("Artis")),
        rif(TELEPORTERS & TP_ESPER, l("Esperia")),
        rif(TELEPORTERS & TP_BOSSR, l("The Monster King Fortress")),
        l("None");
    mes "";
    if (@menu != 9)
        delitem EverburnPowder, 1;
    closedialog;
    switch (@menu) {
        case 2:
            warp "009-1", 113, 91; break;
        case 3:
            warp "018-5", any(89, 90), 45; break;
        case 4:
            warp "003-1", any(40, 41), 49; break;
        case 5:
            warp "012-1", any(86, 87), any(69, 70); break;
        default:
            close;
    }

    close;


OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

