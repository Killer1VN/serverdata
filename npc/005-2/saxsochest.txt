// TMW2 Script
// Author:
//    Crazyfefe
// Description:
//    Minor quest for a necklace

005-2,44,41,0	script	Saxso Chest	NPC_NO_SPRITE,{

    function quest_completed {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("you already opened the chest.");
            close;
    }

    function quest_open {
        if (countitem(.key) > 0) {
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("You open the chest and found a @@.",getitemlink(.reward));
            delitem .key,1;
            getitem .reward,1;
            getexp 80, 15;
            setq CandorQuest_Chest, 1;
            close;
        } else {
            speech S_FIRST_BLANK_LINE,
                l("You don't have the key.");
            close;
        }
    }

    function quest_started {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("It looks locked.");
        narrator S_LAST_NEXT,
            l("Perhaps you should use a key to open it.");
        do
        {
            select
                l("Use a key."),
                menuaction(l("Quit"));

            switch (@menu) {
                case 1:
                    quest_open;
                    break;
            }
        } while (@menu != 2);
    }

    do
    {
        .@chest = getq(CandorQuest_Chest);
        if (.@chest == 1)
            goto quest_completed;
        select
            rif(.@chest == 0,l("There is a dusty chest.")),
            menuaction(l("Quit"));

        switch (@menu) {
            case 1:
                quest_started;
                break;
        }
    } while (@menu != 2);

    closedialog;
    goodbye;
    close;

OnInit:
    .key = SaxsoKey;
    .reward = ToothNecklace;

    .sex = G_MALE;
    .distance = 3;
    end;
}

