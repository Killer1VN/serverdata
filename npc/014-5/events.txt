// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    St. Patrick Day
// Variables:
//	  #PATRICK_DAY
//		When you spinned this
//	  #PATRICK_CTRL
//		Controls rarity, lowering boost
//	  $@PATRICK_DAYMAX
//		Last day in St. Patrick event

014-5,47,53,0	script	St. Patrick Gold Pot	NPC_GOLDPOT,{
    function symbol{
        switch (getarg(0)) {
        case 0:
            mesn "%%t";
            break;
        case 1:
            mesn "%%g";
            break;
        case 2:
            mesn "%%H";
            break;
        case 3:
            mesn "%%G";
            break;
        case 4:
            mesn "%%Q";
            break;
        case 5:
            mesn "%%N";
            break;
        case 6:
            mesn "%%K";
            break;
        default:
            mesn "%%@";
            break;
        }
    }

	if ($EVENT$ != "St. Patrick" && !$@GM_OVERRIDE) {
		channelmes("#world", "ERROR, ST. PATRICK GOLD POT: NOEVENT ERROR");
		disablenpc .name$;
		atcommand("@kick "+strcharinfo(0));
		close;
	}
	if (#PATRICK_DAY == gettime(GETTIME_DAYOFMONTH)) {
		mesn;
		mesc l("You already claimed a reward today! Come back tomorrow, and remember to wear GREEN!"), 2;
		close;
	}
	mesn;
	mesc l("Lucky you, you found me! Do you want to SPIN THE WHEEL and gain GREAT rewards?"), 2;
	mesc l("Remember that wearing green BOOSTS the rates at which good stuff will be found!"), 2;
    mesc l("Only green from @@ will be taken in account.", getitemlink(GreenDye));
	next;
	select
		l("Spin it!"),
		l("How does spinning works?"),
		l("Don't spin it!");
	mes "";
	if (@menu == 3)
		close;

	if (@menu == 2) {
		mes ("%%0 - "+l("GP"));
		mes ("%%1 - "+getitemlink(CasinoCoins));
		mes ("%%2 - "+getitemlink(Topaz));
		mes ("%%3 - "+getitemlink(StrangeCoin));
		mes ("%%4 - "+getitemlink(GoldOre));
		mes ("%%5 - "+getitemlink(GoldenGift));
		mes ("%%6 - "+getitemlink(GoldenApple));
		close;
	}

	if (@menu == 1) {
		// SPINNING! Set that you can't spin again.
		#PATRICK_DAY=gettime(GETTIME_DAYOFMONTH);

		// Each green accessory raises boost gauge in 25. Full set raises in further 25.
		// Hat, Shirt, Pants, Boots and Gloves. Max boost gauge from dye is 150 atm.
		.@boost=0;
		if (getequipcardid(EQI_HEAD_TOP, 0) == GreenDye)
			.@boost+=25;
		if (getequipcardid(EQI_HEAD_MID, 0) == GreenDye)
			.@boost+=25;
		if (getequipcardid(EQI_HEAD_LOW, 0) == GreenDye)
			.@boost+=25;
		if (getequipcardid(EQI_SHOES, 0) == GreenDye)
			.@boost+=25;
		if (getequipcardid(EQI_GARMENT, 0) == GreenDye)
			.@boost+=25;
		if (.@boost >= 125)
			.@boost+=25;

		// Blv+Jlv gives a small boost. The "maximum" sum is 250, but we divide by 5
		// So this grants a further boost of 4~50 depending on level.
		.@boost+=(BaseLevel+JobLevel)/5;

		// .@boost, right now, is a number from 0 to 200.
		// Randomness factor can make that 20% higher, up to 240.
		.@r=rand(0,20);
		.@boost+=(.@boost*.@r/100);
		// Then it may add or subtract 10 points from boost. Apply a cap to range 0~250.
		.@boost+=rand(-10, 10);

		// Now, you can't get 5 Golden Apples straight, right?
		// You lose boost as you get rewards. You lose boost the farther from
		// event last day you are, too.
		.@boost-=#PATRICK_CTRL;
		.@boost-=($@PATRICK_DAYMAX-#PATRICK_DAY);

		// sanitize boost so it ranges from 0 to 250.
		.@boost=max(0, .@boost);

		// REFERENCE: At release time, max levels ingame were about 60/40.
		// That would give 10~34 (not green) to 160~214 (fully green) boost value.
		mesc l("Spinning!"), 2;

		// .@v holds the reward you'll get
		if (.@boost > 240)
			.@v=6;
		else if (.@boost > 200)
			.@v=5;
		else if (.@boost > 160)
			.@v=4;
		else if (.@boost > 120)
			.@v=3;
		else if (.@boost > 80)
			.@v=2;
		else if (.@boost > 40)
			.@v=1;
		else
			.@v=0;

		// Tell players what they spinned, and lower rarity on next tries
		symbol(.@v);
		#PATRICK_CTRL+=.@v+(.@v/2);

		// Grant players the reward
		switch (.@v) {
			case 0: // GP
				Zeny=Zeny+rand(10, .@boost+10);
				break;
			case 1: // Casino Coins
				getitem CasinoCoins, max(1, .@boost/10);
				Zeny=Zeny+rand(20, .@boost+10);
				break;
			case 2: // Topaz
				getitem Topaz, 1;
				Zeny=Zeny+rand(30, .@boost+10);
				break;
			case 3: // Strange Coins
				getitem StrangeCoin, max(1, .@boost/10);
				Zeny=Zeny+rand(40, .@boost+10);
				break;
			case 4: // Gold Ore
				getitem GoldOre, 1;
				Zeny=Zeny+rand(50, .@boost+10);
				break;
			case 5: // Golden Gift
				getitem GoldenGift, 1;
				Zeny=Zeny+rand(60, .@boost+10);
				break;
			case 6: // Golden Apple (OP)
				getitem GoldenApple, 1;
				Zeny=Zeny+rand(70, .@boost+10);
				break;
		}
		next;
		mesn;
		mesc l("Did you like it? Find me tomorrow, until day @@, for another gift!", $@PATRICK_DAYMAX), 2;
		close;
	}
	// Should not reach here
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;

    if ($EVENT$ != "St. Patrick") {
        disablenpc(.name$);
        end;
    }
OnClock0000:
OnForcedMove:
    if ($EVENT$ == "St. Patrick") {
        setarray .@vx, 134, 162, 108, 105, 67, 191, 168, 170, 191, 188, 160, 145, 196, 120;
        setarray .@vy,  36,  61,  45,  96, 76,  74,  72, 111, 113, 129, 125, 119, 143, 104;
        .@r=rand(0,getarraysize(.@vx)-1);
        movenpc .name$, .@vx[.@r], .@vy[.@r];
		debugmes "Warping to %d,%d (seed %d)", .@vx[.@r], .@vy[.@r], .@r;
    }
    end;
}

