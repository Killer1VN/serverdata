// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Spring Quest, disabled during Easter
//
//    SQuest_SPring

014-5,79,168,0	script	Blossom	NPC_BLOSSOM,{
    if (season() != SPRING && !$@GM_OVERRIDE)
        goto L_OutOfSeason;
    if ($EVENT$ == "Easter")
        goto L_QuestDisabled;

    .@q=getq(SQuest_Spring);
    mesn;
    mesq l("Thorn is so annoying...");
    if (.@q < 1)
        goto L_SpringQuest;

L_Main:
    next;
    .@q=getq(HurnscaldQuest_Blossom);
    if (.@q == 0) {
        mesn;
        mesq l("Anyway, I got this really nice @@ from Audsbel. He said I could collect herbs more efficiently with it.", getitemlink(EnchantedHerbBag));
        next;
        mesn;
        mesq l("I am willing to give it to you, if help me out. I promise it won't be a bother, and it'll really be useful for you, I swear.");
        next;
        mesn;
        mesq l("I would like @@ @@, @@ @@ and @@ @@!", 30, getitemlink(HalfEggshell), 7, getitemlink(RedApple), 1, getitemlink(Orange));
        mesc l("@@ drolls without realizing.", .name$);
        next;
        mesc l("Give her the materials she asked for?");
        if (askyesno() == ASK_YES) {
            inventoryplace EnchantedHerbBag, 1;
            if (!transcheck(HalfEggshell, 30, RedApple, 7, Orange, 1))
                close;
            getitem EnchantedHerbBag, 1;
            getexp 600, 120; // Reference: Level 20
            mesn;
            mesq l("Thanks for helping me out! Here, take this bag. Now if you excuse me...");
            mesc l("*drolls*");
            setq HurnscaldQuest_Blossom, 1;
            next;
        }
    }
    closeclientdialog;
    goodbye;
    end;

L_OutOfSeason:
    mesn;
    mesq l("Hmm, Woodlands is a wonderful place to live in! Although Thorn gets really a thorn in my boots during spring...");
    goto L_Main;

L_QuestDisabled:
    mesn;
    mesq l("Happy Easter!");
    mesc l("The @@ quest is disabled during Easter event.", getitemlink(Wreath));
    next;
    goto L_Main;

L_SpringQuest:
    next;
    mesn;
    mesq l("Hey, you! Aid me!");
    next;
    mesn;
    mesq l("There's an annoying half-wose-half-elf called Thorn trying to impress me with his wose side!");
    next;
    mesn;
    mesq l("I do not like him, but even so, he keeps bothering me! Maybe I could out-impress him, though!");
    next;
    mesn;
    mes l("What about you bring me:");
    mes l("@@/55 @@", countitem(Rose), getitemlink(Rose));
    mes l("@@/55 @@", countitem(Tulip), getitemlink(Tulip));
    mes l("@@/50 @@", countitem(Blueberries), getitemlink(Blueberries));
    mes l("@@/10 @@", countitem(GrassSeeds), getitemlink(GrassSeeds));

    mes l("@@/80 @@", countitem(MauveHerb), getitemlink(MauveHerb));
    mes l("@@/80 @@", countitem(GambogeHerb), getitemlink(GambogeHerb));
    mes l("@@/80 @@", countitem(CobaltHerb), getitemlink(CobaltHerb));
    mes l("@@/80 @@", countitem(AlizarinHerb), getitemlink(AlizarinHerb));
    next;
    select
        l("Not now, thanks"),
        l("To be honest, I have that with me!");

    mes "";
    if (@menu == 1)
        goto L_Main;
    if (@menu == 2) {
        if (
            countitem(Rose)             < 55 ||
            countitem(Tulip)            < 55 ||
            countitem(Blueberries)      < 50 ||
            countitem(GrassSeeds)       < 10 ||
            countitem(MauveHerb)        < 80 ||
            countitem(GambogeHerb)      < 80 ||
            countitem(CobaltHerb)       < 80 ||
            countitem(AlizarinHerb)     < 70
            ) goto L_Lying;

        inventoryplace Wreath, 1;
        delitem Rose, 55;
        delitem Tulip, 55;
        delitem Blueberries, 50;
        delitem GrassSeeds, 10;
        delitem MauveHerb, 80;
        delitem GambogeHerb, 80;
        delitem CobaltHerb, 80;
        delitem AlizarinHerb, 80;
        getitem Wreath, 1;
        getexp (210*BaseLevel), 120;
        setq1 SQuest_Spring, 1;
        mesn;
        mesq l("Many thanks! At least he'll know he haven't impressed me yet!");
        next;
        mesn;
        mesq l("Here, take the reward as promised!");
        goto L_Main;
    }

    close;

L_Lying:
    mesn;
    mesq l("Please don't lie to me...");
    goto L_Main;

OnInit:
    .sex=G_FEMALE;
    .distance=8;
	end;

}
