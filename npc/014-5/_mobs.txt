// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 014-5: North Woodlands mobs
014-5,156,106,48,44	monster	Log Head	1066,21,20000,20000
014-5,68,178,48,44	monster	Mouboo	1023,16,20000,20000
014-5,90,82,62,55	monster	Poison Spiky Mushroom	1043,21,20000,20000
014-5,56,114,36,30	monster	Red Mushroom	1042,6,40000,20000
014-5,130,132,48,44	monster	Forest Mushroom	1060,9,20000,20000
014-5,76,70,48,44	monster	Plushroom Field	1011,16,80000,80000
014-5,66,110,44,38	monster	Chagashroom Field	1128,16,80000,80000
014-5,153,88,48,44	monster	Cobalt Herb	1136,8,80000,80000
014-5,132,140,61,33	monster	Gamboge Herb	1134,6,80000,80000
014-5,91,132,54,63	monster	Mauve Herb	1135,9,80000,80000
