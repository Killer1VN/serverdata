// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Sign

004-1,65,74,0	script	Sign#HalinarzoGoto	NPC_SWORDS_SIGN,{
    mesc "↑ "+l("Tulimshar");
    mesc "→ "+l("Halinarzo Route");
    mesc "← "+l("Tulimshar Beach");
    mesc "↓ "+l("Tulimshar Mines");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

