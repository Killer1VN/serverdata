// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Intense Beard is the crafting master

003-8,27,42,4	script	Intense Beard	NPC_HUMAN_M_ARTIS,{
    function IntenseBeardTutorial;
    if (BaseLevel < 18)
        goto L_SemNivel;
    if (!CRAFTQUEST)
        goto L_SemQuest;
    if (!getskilllv(TMW2_CRAFT))
        goto L_Learn;
    if (!countitem(RecipeBook) && !countitem(JesusalvaGrimorium))
        goto L_SemLivro;

    mesn;
    mesq l("Hello my friend! I see you've brought your @@ with you this time!", getitemlink(RecipeBook));
    next;
    mesn;
    mesq l("Let's learn new recipes! Do you wanna?");
    mesc l("Bonus recipes must be enabled with @@ after learned!", b("@ucp"));
    next;
    .@bool=false;
    do {
        csysGUI_Report(.@bool);
        mesc l("Learn what?");
        mes "";
        .@opt$="Nothing";
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_BASE);

        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_ATK);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_DEF);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_ACC);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_EVD);

        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_REGEN);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_SPEED);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_DOUBLE);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_MAXPC);

        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_SCRESIST);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_SCINFLICT);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_MANAUSE);
        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_BOSSATK);

        .@opt$+=":"+csysGUI_OptLearnMenu(CRGROUP_FINAL);

        .@opt$+=":I would like a tutorial";

        select (.@opt$);
        mes "";
        switch (@menu) {
        case 1:
            break;

        case 2:
			if (csysGUI_RaiseOpt(CRGROUP_BASE))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;

        case 3:
			if (csysGUI_RaiseOpt(CRGROUP_ATK))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 4:
			if (csysGUI_RaiseOpt(CRGROUP_DEF))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 5:
			if (csysGUI_RaiseOpt(CRGROUP_ACC))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 6:
			if (csysGUI_RaiseOpt(CRGROUP_EVD))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;

        case 7:
			if (csysGUI_RaiseOpt(CRGROUP_REGEN))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 8:
			if (csysGUI_RaiseOpt(CRGROUP_SPEED))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 9:
			if (csysGUI_RaiseOpt(CRGROUP_DOUBLE))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 10:
			if (csysGUI_RaiseOpt(CRGROUP_MAXPC))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;

        case 11:
			if (csysGUI_RaiseOpt(CRGROUP_SCRESIST))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 12:
			if (csysGUI_RaiseOpt(CRGROUP_SCINFLICT))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 13:
			if (csysGUI_RaiseOpt(CRGROUP_MANAUSE))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        case 14:
			if (csysGUI_RaiseOpt(CRGROUP_BOSSATK))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;

        case 15:
			if (csysGUI_RaiseOpt(CRGROUP_FINAL))
				mesc l("Success!"), 3;
			else
				mesc l("Not enough Monster Points!"), 1;
			break;
        default:
            IntenseBeardTutorial();
            break;
        }

        // Update boolean
        if (!.@bool)
            .@bool=true;
    } while (@menu != 1);
    close;

L_SemNivel:
    mesn;
    mesq l("Please don't disturb me, I'm busy crafting my own customized power mega blaster weapon. I just keep failing to apply +100% exp gain bonus on it!");
    close;

L_SemLivro:
    mesn;
    mesq l("Heya, noob. Where is your @@? Are you really so noob to have forgot such important book?!", getitemlink(RecipeBook));
    next;
    mesn;
    mesq l("That's too bad, because I could teach you some amazing craft skills! This will need to wait another day, I guess. Oh well.");
    close;

L_SemQuest:
    mesn;
    mesq l("Heya, noob. Where is your @@? Are you really so noob to have forgot such important book?!", getitemlink(RecipeBook));
    next;
    mesn strcharinfo(0);
    mesc l("Was I supposed to have one? How should I reply?");
    mes "";
    select
        l("Erm... I don't have a recipe book."),
        l("That's nice, but... I don't have a recipe book."),
        l("Pardon me, what is a recipe book?");
    mes "";
    mesn;
    mesq l("WHAT? Have you not gained one at birth? That's absurd!");
    next;
    select
        l("Erm... I don't know my parents."),
        l("Maybe, but... I don't know my parents."),
        l("Pardon me, I don't remember my parents.");
    mes "";
    mesn;
    mesq l("WHAT? That's twice absurd! Now you'll say that you were found stranded on a desert island without equipment and can't remember anything but your own name!");
    next;
    select
        l("Erm... You're right."),
        l("Well... You're right."),
        l("How did you guess that?");
    mes "";
    mesn;
    mesq l("... ... ...");
    next;
    mesn;
    mesq l("...Okay, this is not fun anymore.");
    next;
    mesn;
    mesq l("Here, you can have this spare one. Hahah! Crafting is an art, and I help teaching it. Just be sure to don't lose it, you will NEVER get another copy EVER again, was I clear?!");
    next;
    mesn;
    mesq l("While we're at that, do you know how to craft? No? First, you'll need a Cauldron, in case you've rented a house. Otherwise, you can rent a work table on this Forge.");
    next;
    mesn;
    mesq l("You need to read this recipe. Without the essential crafting skill you won't go very far. There will be no bonuses on the items you craft because you are not a craftsman.");
    // Obtain a blueprint and a recipe book
    inventoryplace EquipmentBlueprintA, 1, RecipeBook, 1;
    getitem EquipmentBlueprintA, 1;
    getitem RecipeBook, 1;
    CRAFTQUEST=1;
    next;

L_Learn:
    mesn;
    mesq l("I see you don't know how to craft. Do you want me to teach the basic for you? I'll charge 1000 GP.");
    if (Zeny < 1000)
        close;
    next;
    if (askyesno() == ASK_YES) {
        // Pay the money
        Zeny-=1000;

        // Give you the craft skill itself
        skill TMW2_CRAFT, 1, 0;

        // We should aid you getting basic skills - for free!
        if (!CRAFTSYS[CRGROUP_BASE])
            CRAFTSYS[CRGROUP_BASE]+=1;
        if (!CRAFTSYS_CURRENT)
            CRAFTSYS_CURRENT=CRAFTSYS_CURRENT|CRGROUP_BASE;

        mesn;
        mesq l("There, now you can not only craft items, but craft AWESOME ITEMS!");

        RECIPES_EQUIPMENT[CraftDagger]=true;
        mesc l("You've also learned how to craft Daggers! It serves solely for training purposes. You get crafting points for every 40 levels crafted on the item.");
    }
    close;

function IntenseBeardTutorial {
    mesn;
    mesq l("Well, first of, you'll need an @@ and an Equipment Recipe.", getitemlink(RecipeBook));
    next;
    mesn;
    mesq l("You can craft items in forges. You can use it on your house, or within this forge for a tax.");
    next;
    mesn;
    mesq l("That will help you to make your very first first craft! Remember to use @@ to change which bonuses can be applied to your craft items.", b("@ucp"));
    next;
    mesn;
    mesq l("I can teach you these bonuses, but more powerful bonuses require more skill to learn... And I don't know anyone who could improve your crafting skill...");
    next;
    mesn;
    mesq l("...The ones who know a lot are dwarves, but they don't live here - they live with elves, it seems.");
    next;
    return;
}

OnInit:
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;

}


