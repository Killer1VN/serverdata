// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    Legendary hero

// I started with 'Greetings, citzen' and then I thought on 4144.
// He would never do such formal, hero-ish introduction. So I decided for something
// more realistic, which sounded like a human hero (ie. a normal player).

012-1,75,58,0	script	Andrei Sakar	NPC_ANDREI,{
    // NLib_question (  )
    function NLib_question {
        //freeloop(true);
        do {
            @qid=rand(0,7);
        } while (! (2**@qid) & @sortable);
        debugmes "Question "+@qid;
        @sortable-=(2**@qid);
        setarray @ans$, $@NLIBa1$[@qid], $@NLIBa2$[@qid], $@NLIBa3$[@qid], $@NLIBa4$[@qid];
        setarray @val, $@NLIBmr1[@qid], $@NLIBmr2[@qid], $@NLIBmr3[@qid], $@NLIBmr4[@qid];
        /*
        @nm=15;
        @i=0;
        debugmes "BG WHILE S00002";
        while (@nm > 0) {
            do {
                @nai=rand(0,3);
            } while ((2**@nai) & @nm);
            debugmes "BG WS2 NODE";
            @nm-=@nai;
            @ans$[@i]=@as$[@nai];
            @val[@i]=@nai;
        }
        debugmes "BG NO FREE LOOP";
        //freeloop(false);
        */
        // Final: @ans$ ; @val ; @qid
        //        Question, menuret, QID

        mes "";
        addtimer(30000, "Andrei Sakar::OnTooLong");
        .@q=getq3(Q_NivalisLibday);
        setq3 Q_NivalisLibday, .@q-7;
        mesn;
        mesq $@NLIBqs$[@qid];
        menuint
            @ans$[0], @val[0],
            @ans$[1], @val[1],
            @ans$[2], @val[2],
            @ans$[3], @val[3];
        mes "";
        deltimer("Andrei Sakar::OnTooLong");
        if (@menuret == 1) {
            .@q=getq3(Q_NivalisLibday);
            setq3 Q_NivalisLibday, .@q+7;
            mesn;
            mesq l("That's right.");
            next;
        } else {
            mesn;
            mesq l("Sorry, but that's wrong.");
            close;
        }
        debugmes "BG END";
    // end
    }

    mesn l("Andrei Sakar, Legendary Hero");
    if ($NLIB_DAY || ($@GM_OVERRIDE && is_admin())) goto L_Quizz;

L_Default:
    if (strcharinfo(0) == $MOST_HEROIC$)
        mesq l("Hi, @@.", strcharinfo(0));
    else
        mesq l("Hi.");
    close;

L_Quizz:
    .@q=getq3(Q_NivalisLibday);
    if (.@q == 0) {
        setq3 Q_NivalisLibday, 100;
        .@q=100;
    } else if (.@q > 100) {
        goto L_Default;
    }

    mesq l("We are assembling forces to take Nivalis back.");
    mesq l("I'll make you five questions about lore and general knowledge. You'll have 30 seconds to read and answer each.");
    mesq l("If you finish everything, and answer everything right, you may get a reward.");
    mesq l("If you take too long you'll be penalized.");
    mesq l("Do you want to start?");
    next;
    if (askyesno() == ASK_NO)
        close;
    setarray @sortable, 255;

    NLib_question();
    NLib_question();
    NLib_question();
    NLib_question();
    NLib_question();


    mesn;
    .@q=getq3(Q_NivalisLibday);
    // You cannot go above 100 points.
    if (.@q > 100) {
        mesq l("...More bugs.");
        mesc l("A bug was found. Aborting script."), 1;
        close;
    } else if (.@q == 100) {
        mesq l("Congratulations. You really know about the world lore.");
        getitem HastePotion, 2;
        getitem StrengthPotion, 2;
        getitem Bread, 5;
    } else if (.@q > 90) {
        mesq l("Outstanding. Congratulations.");
        getitem HastePotion, 2;
        getitem StrengthPotion, 2;
        getitem Bread, 4;
    } else if (.@q > 75) {
        mesq l("Good, knowing the world lore is important.");
        getitem HastePotion, 1;
        getitem StrengthPotion, 1;
        getitem Bread, 4;
    } else if (.@q > 50) {
        mesq l("Good job.");
        getitem HastePotion, 1;
        getitem StrengthPotion, 1;
        getitem Bread, 2;
    } else if (.@q > 25) {
        mesq l("Well, that was bad, but at least you know a bit from story.");
        getitem Bread, 2;
    } else if (.@q > 0) {
        mesq l("Terrible. You know almost nothing from world lore...");
        getitem Bread, 1;
    } else {
        mesq l("You really should read the dialogs.");
    }
    // If you got a negative value, this will default to 1.
    getexp .@q*BaseLevel, .@q*JobLevel;
    setq3 Q_NivalisLibday, 9999;
    close;

OnTooLong:
    .@q=getq3(Q_NivalisLibday);
    setq3 Q_NivalisLibday, .@q-21;
    npctalk3 l("You took too long to answer.");
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    /* Because I was lazy to calculate array size, it is hard-coded to eight questions numbered from 0 to 7.
    setarray $@NLIBqs$;
    setarray $@NLIBa1$;
    setarray $@NLIBa2$;
    setarray $@NLIBa3$;
    setarray $@NLIBa4$;
    setarray $@NLIBmr1;
    setarray $@NLIBmr2;
    setarray $@NLIBmr3;
    setarray $@NLIBmr4;
    */
    end;

}
