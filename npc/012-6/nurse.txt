// TMW-2 Script
// Author:
//    Jesusalva
//    Crazyfefe
// Notes:
//  HurnscaldQuest_BloodDonor
//      1 - Donation finished
//      2 - Donation in progress
//  Structure
//      Status, Next Attempt Timer

012-6,56,67,6	script	Hurnscald's Nurse	NPC_FEMALE,{
    .@q2=getq2(HurnscaldQuest_BloodDonor);
    if (.@q2 > gettimetick(2)) {
        Nurse(.name$, 10, 6);
        close;
    }
    mesn;
    mesq l("Hello there. Do you want to donate blood?");
    mesc l("Donating blood will BLOCK YOUR MOVEMENT for three minutes.");
    mesc l("Do not disconnect while waiting. You need 100% HP to donate.");

L_BLCore:
    mes "";
    select
        l("I need infirmary services."),
        l("Info"),
        rif(readparam(Hp) == readparam(MaxHp), l("Yes, please use my blood to save lifes."));
    mes "";
    switch (@menu) {
        case 1:
            Nurse(.name$, 10, 6);
            close;
        case 2:
            mesn;
            mesq l("Donating blood is a noble act, which allows to save lifes.");
            next;
            mesn;
            mesq l("When people loses too much blood, they die, unless we can lend them somebody's else blood.");
            next;
            mesn;
            mesq l("Not everyone can be a blood donor. For example, you must be healthy.");
            mesq l("You can think more info about this on these links:");
            mes "";
            mes "[@@https://www.blood.co.uk/|https://www.blood.co.uk/@@]";
            mes "[@@https://www.nhsbt.nhs.uk/what-we-do/blood-services/blood-donation/|https://www.nhsbt.nhs.uk/what-we-do/blood-services/blood-donation/@@]";
            mes l("France: [@@https://dondesang.efs.sante.fr/|https://dondesang.efs.sante.fr/@@]");
            mes l("Brazil: [@@http://www.prosangue.sp.gov.br/home/Default.html|http://www.prosangue.sp.gov.br/home/Default.html@@]");
            next;
            goto L_BLCore;
        case 3:
            mesn;
            mesq l("Please don't sound like I was some sort of vampire or something...");
            next;
            mesn;
            mesq l("This will take only a short while.");
            next;
            setq1 HurnscaldQuest_BloodDonor, 2;
            percentheal -90, 0;
            addtimer(180000, "Hurnscald's Nurse::OnDonationComplete");
            slide 34, 29;
            setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_MOVE, true);
            dispbottom l("Any movement/skill/item will be without effect until time is up.");
            closedialog;
            close;
    }

    close;

OnDonationComplete:
    if (checkpcblock() & PCBLOCK_ATTACK)
        setpcblock(PCBLOCK_ATTACK|PCBLOCK_SKILL|PCBLOCK_USEITEM|PCBLOCK_MOVE, false);
    slide 58, 67;
    percentheal 100, 0;
    getexp readparam(Hp)*2, readparam(Hp)/100;
    setq HurnscaldQuest_BloodDonor, 1, gettimetick(2)+(2592000); // 60*60*24*30
    if (BaseLevel >= 30)
        getitem Bread, 1;
    if (BaseLevel >= 50)
        getitem ChocolateBar, 1;
    if (BaseLevel >= 70)
        getitem BottleOfWoodlandWater, 1;
    mesn;
    mesq l("Thanks for donating blood. You can donate again in 30 days.");
    mesq l("You should eat and drink water after donating blood. Thanks for saving lifes!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, BrimmedFeatherHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, MiniSkirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, ShortTankTop);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 16);

    .sex = G_FEMALE;
    .distance = 5;
    end;

}

