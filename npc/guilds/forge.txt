// Moubootaur Legends Script
// Author:
//    Jesusalva
// Description:
//  Guild Facility - Blacksmith Area

guilds,24,27,0	script	Guild Blacksmith	NPC_NO_SPRITE,{
    // Script begins here
    do
    {
        mesc l("What will you forge today?");
        //mesc l("You might need to have a @@ equipped for some recipes!", getitemlink(Knife));
        mesc l("Note: Items forged here will be guild-bound, but will use player recipes and bonuses!");
        mesc b(l("\"WHAT IS CRAFTED IN GUILD BELONGS TO GUILD.\"")), 1;
        if (SmithSystem(CRAFT_GUILD))
        {
            mesc l("Success!"), 3;
            next;
        }
        else
        {
            mesc l("That didn't work!"), 1;
            next;
        }
        mesc l("Try again?");
    } while (askyesno() == ASK_YES);
    close;

OnInit:
    .distance=2;
    end;
}

