// Moubootaur Legends Script
// Author:
//    Jesusalva
// Description:
//  Guild Facility - Contains recipe list

guilds,47,39,0	script	Guild Logs	NPC_NO_SPRITE,{
    function hudRecipe;
    function showRecipe;
    function calcRecipe;
    function clearRecipe;
    .@gid=getcharid(2);
    .@s=0; // How many was displayed, to use next(); if needed

    mes ".:: " + l("Alchemy Recipes") + " ::.";
    // Healing
    .@s+=showRecipe(CraftPiberriesInfusion, PiberriesInfusion,
                    5, Piberries, 1, Curshroom);

    if (.@s > 1)
        next;

    // General Boosts
    .@s+=showRecipe(CraftHastePotion, HastePotion,
                    15, Plushroom);
    .@s+=showRecipe(CraftStrengthPotion, StrengthPotion,
                    15, Chagashroom);
    .@s+=showRecipe(CraftResetPotion, StatusResetPotion,
                    90, ManaPiouFeathers, 10, Curshroom);
    .@s+=showRecipe(CraftSpeedPotion, MoveSpeedPotion,
                    1, GemPowder, 5, FluoPowder);
    .@s+=showRecipe(CraftPrecisionPotion, PrecisionPotion,
                    3, Piberries, 1, MountainSnakeEgg);
    .@s+=showRecipe(CraftDodgePotion, DodgePotion,
                    3, Piberries, 1, SnakeEgg);

    if (.@s > 5)
        next;

    // Stats Boosts
    .@s+=showRecipe(CraftLukPotionA, LukPotionA,
                    1, EmeraldPowder, 1, HerbalTea);
    .@s+=showRecipe(CraftLukPotionB, LukPotionB,
                    1, Emerald, 2, HerbalTea);
    .@s+=showRecipe(CraftLukPotionC, LukPotionC,
                    1, PolishedEmerald, 3, HerbalTea);

    .@s+=showRecipe(CraftDexPotionA, DexPotionA,
                    1, AmethystPowder, 1, HerbalTea);
    .@s+=showRecipe(CraftDexPotionB, DexPotionB,
                    1, Amethyst, 2, HerbalTea);
    .@s+=showRecipe(CraftDexPotionC, DexPotionC,
                    1, PolishedAmethyst, 3, HerbalTea);

    .@s+=showRecipe(CraftIntPotionA, IntPotionA,
                    1, SapphirePowder, 1, HerbalTea);
    .@s+=showRecipe(CraftIntPotionB, IntPotionB,
                    1, Sapphire, 2, HerbalTea);
    .@s+=showRecipe(CraftIntPotionC, IntPotionC,
                    1, PolishedSapphire, 3, HerbalTea);

    .@s+=showRecipe(CraftVitPotionA, VitPotionA,
                    1, DiamondPowder, 1, HerbalTea);
    .@s+=showRecipe(CraftVitPotionB, VitPotionB,
                    1, Diamond, 2, HerbalTea);
    .@s+=showRecipe(CraftVitPotionC, VitPotionC,
                    1, PolishedDiamond, 3, HerbalTea);

    .@s+=showRecipe(CraftAgiPotionA, AgiPotionA,
                    1, TopazPowder, 1, HerbalTea);
    .@s+=showRecipe(CraftAgiPotionB, AgiPotionB,
                    1, Topaz, 2, HerbalTea);
    .@s+=showRecipe(CraftAgiPotionC, AgiPotionC,
                    1, PolishedTopaz, 3, HerbalTea);

    if (.@s > 14)
        next;

    // Limit Boosts
    .@s+=showRecipe(CraftSacredManaPot, SacredManaPotion,
                    1, GoldenApple, 15, CelestiaTea);
    .@s+=showRecipe(CraftSacredLifePot, SacredLifePotion,
                    1, GoldenApple, 1, ElixirOfLife);

    if (.@s == 0)
        mesc l("Your guild doesn't knows any recipes!"), 1;

    if (getguildrole(.@gid, getcharid(3)) > GPOS_VICELEADER)
        close;
    do
    {
        select
            l("Do nothing"),
            rif(strcharinfo(0) == getguildmaster(.@gid), l("Raise max members")),
            l("Learn Alchemy Recipes");
        mes "";

        switch (@menu) {
            case 2:
                .@cur_lv=getguildlvl(.@gid);
                .@min_lv=(getskilllv(GD_EXTENSION)+1)*4;
                .@price=.@min_lv*274;
                if (.@cur_lv < .@min_lv)
                {
                    mesc l("Guild Level is not enough: @@/@@", .@cur_lv, .@min_lv);
                    next;
                    break;
                }
                mesc l("Raising this skill will allow to recruit 4 more members.");
                mesc l("The cost for Guild Vault is @@ GP.", .@price);
                next;
                select
                    rif($GUILD_BANK[.@gid] >= .@price, l("Upgrade it")),
                    l("Don't upgrade it");
                mes "";
                if (@menu == 1 && $GUILD_BANK[.@gid] >= .@price)
                {
                    $GUILD_BANK[.@gid]-=.@price;
                    // guildskill()?
                    skill GD_EXTENSION, getskilllv(GD_EXTENSION)+1, 0;
                    break;
                }
                break;
            case 3:
                mes ".:: " + l("Alchemy Recipes") + " ::.";
                do {
                    clearRecipe();

                    // Healing Recipes
                    if (!showRecipe(CraftPiberriesInfusion, false))
                        calcRecipe(CraftPiberriesInfusion, 3, 10000, l("Piberries Infusion"));

                    // General Boosts
                    if (!showRecipe(CraftHastePotion, false))
                         calcRecipe(CraftHastePotion, 2, 5000, l("Haste Potion"));
                    if (!showRecipe(CraftStrengthPotion, false))
                         calcRecipe(CraftStrengthPotion, 2, 5000, l("Strength Potion"));

                    if (!showRecipe(CraftResetPotion, false))
                         calcRecipe(CraftResetPotion, 4, 50000, l("Status Reset Potion"));
                    if (!showRecipe(CraftSpeedPotion, false))
                         calcRecipe(CraftSpeedPotion, 4, 50000, l("Movement Speed Potion"));

                    if (!showRecipe(CraftPrecisionPotion, false))
                         calcRecipe(CraftPrecisionPotion, 5, 20000, l("Precision Potion"));
                    if (!showRecipe(CraftDodgePotion, false))
                         calcRecipe(CraftDodgePotion, 5, 20000, l("Dodge Potion"));

                    // Stats Boosts
                    if (!showRecipe(CraftLukPotionA, false))
                         calcRecipe(CraftLukPotionA, 4, 15000, l("Luck Potion"));
                    if (!showRecipe(CraftLukPotionB, false))
                         calcRecipe(CraftLukPotionB, 6, 25000, l("Luck+ Potion"));
                    if (!showRecipe(CraftLukPotionC, false))
                         calcRecipe(CraftLukPotionC, 8, 35000, l("Luck++ Potion"));

                    if (!showRecipe(CraftDexPotionA, false))
                         calcRecipe(CraftDexPotionA, 4, 15000, l("Dex Potion"));
                    if (!showRecipe(CraftDexPotionB, false))
                         calcRecipe(CraftDexPotionB, 6, 25000, l("Dex+ Potion"));
                    if (!showRecipe(CraftDexPotionC, false))
                         calcRecipe(CraftDexPotionC, 8, 35000, l("Dex++ Potion"));

                    if (!showRecipe(CraftIntPotionA, false))
                         calcRecipe(CraftIntPotionA, 4, 15000, l("Int Potion"));
                    if (!showRecipe(CraftIntPotionB, false))
                         calcRecipe(CraftIntPotionB, 6, 25000, l("Int+ Potion"));
                    if (!showRecipe(CraftIntPotionC, false))
                         calcRecipe(CraftIntPotionC, 8, 35000, l("Int++ Potion"));

                    if (!showRecipe(CraftVitPotionA, false))
                         calcRecipe(CraftVitPotionA, 4, 15000, l("Vit Potion"));
                    if (!showRecipe(CraftVitPotionB, false))
                         calcRecipe(CraftVitPotionB, 6, 25000, l("Vit+ Potion"));
                    if (!showRecipe(CraftVitPotionC, false))
                         calcRecipe(CraftVitPotionC, 8, 35000, l("Vit++ Potion"));

                    if (!showRecipe(CraftAgiPotionA, false))
                         calcRecipe(CraftAgiPotionA, 4, 15000, l("Agi Potion"));
                    if (!showRecipe(CraftAgiPotionB, false))
                         calcRecipe(CraftAgiPotionB, 6, 25000, l("Agi+ Potion"));
                    if (!showRecipe(CraftAgiPotionC, false))
                         calcRecipe(CraftAgiPotionC, 8, 35000, l("Agi++ Potion"));

                    // Limit Boosts
                    if (!showRecipe(CraftSacredManaPot, false))
                         calcRecipe(CraftSacredManaPot, 7, 100000, l("Sacred Mana Potion"));
                    if (!showRecipe(CraftSacredLifePot, false))
                         calcRecipe(CraftSacredLifePot, 7, 100000, l("Sacred Life Potion"));

                } while (!hudRecipe());
                break;
        }

    } while (@menu != 1);
    close;

// showRecipe (Craft, Bonus, Req1No, Req1Id, Req2No, Req2Id)
// Bonus must NOT be zero to display text
function showRecipe {
    /*
    debugmes "Exist: %d", getd("$RECIPES_ALCHEMY_"+getcharid(2)+"["+getarg(0)+"]");
    debugmes "Seeking for slot %d on guild %d", getarg(0), getcharid(2);
    */
    if (getd("$RECIPES_ALCHEMY_"+getcharid(2)+"["+getarg(0)+"]")) {
        debugmes "Hooray! It exists! We have %d defined", getarg(1);
        if (getarg(1)) {
            mesn l("Craft @@", getitemlink(getarg(1)));
            if (getarg(2,0))
                mesc l("* @@ @@", getarg(2), getitemlink(getarg(3)));
            if (getarg(4,0))
                mesc l("* @@ @@", getarg(4), getitemlink(getarg(5)));
            mes "";
        }
        //debugmes "You got it: %d (global: %d)", getarg(0), $RECIPES_ALCHEMY[getcharid(2)];
        return 1;
    }
    //debugmes "Nope, nothing here";
    return 0;
}

// calcRecipe (CraftID, GLV, GGP, TXT)
function calcRecipe {
    array_push(@tmp_alcrep_id, getarg(0));
    array_push(@tmp_alcrep_glv, getarg(1));
    array_push(@tmp_alcrep_ggp, getarg(2));
    array_push(@tmp_alcrep_txt$, getarg(3) + " - GLV "+getarg(1)+", "+format_number(getarg(2))+" GP");
    return;
}

// clearRecipe ()
function clearRecipe {
    deletearray(@tmp_alcrep_id);
    deletearray(@tmp_alcrep_glv);
    deletearray(@tmp_alcrep_ggp);
    deletearray(@tmp_alcrep_txt$);
    @tmp_alcrep_id[0]=-1;
    @tmp_alcrep_glv[0]=-1;
    @tmp_alcrep_ggp[0]=-1;
    @tmp_alcrep_txt$[0]=l("Learn Nothing");
    return;
}

// hudRecipe ()
function hudRecipe {
    .@gid=getcharid(2);
    // Select
    select (implode(@tmp_alcrep_txt$, ":"));
    @menu=@menu-1;

    /* DEBUG prints
    debugmes "You choose: %d", @menu;
    debugmes getd("$RECIPES_ALCHEMY_"+.@gid);
    copyarray(.@v, getd("$RECIPES_ALCHEMY_"+.@gid), getarraysize(getd("$RECIPES_ALCHEMY_"+.@gid)) );
    debugmes "Width: %d (out of %d)", getarraysize(.@v), getarraysize(getd("$RECIPES_ALCHEMY_"+.@gid));
    debugmes "Slot 44: %d", .@v[44];
    debugmes "Slot 43: %d", .@v[43];
    debugmes "Slot 42: %d", .@v[42];
    debugmes "GLVL Options: %d-%d-%d-%d", @tmp_alcrep_glv[0], @tmp_alcrep_glv[1], @tmp_alcrep_glv[2], @tmp_alcrep_glv[3];
    */

    if (@tmp_alcrep_glv[@menu] <= 0)
        return 1;

    if (getguildlvl(.@gid) < @tmp_alcrep_glv[@menu]) {
        mesc l("Insufficient Guild Level! (@@/@@)", getguildlvl(.@gid), @tmp_alcrep_glv[@menu]), 1;
        next;
        return 0;
    }
    if ($GUILD_BANK[.@gid] < @tmp_alcrep_ggp[@menu]) {
        mesc l("Insufficient Guild Money! (Guild has: @@ GP)", format_number($GUILD_BANK[.@gid])), 1;
        next;
        return 0;
    }
    // All fine, proceed
    $GUILD_BANK[.@gid] -= @tmp_alcrep_ggp[@menu];
    setd("$RECIPES_ALCHEMY_"+getcharid(2)+"["+@tmp_alcrep_id[@menu]+"]", true);
    mesc l("Skill learnt!"), 2;
    next;
    return 0;
}

OnInit:
    .distance=2;
    end;
}

