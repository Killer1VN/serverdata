// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Lena
// Variables:
//      $HELENA_ST
//          Tracks how many set of Treasure Keys were given by all players thus far.
//          Affects beer prices on Hurnscald.
//      HurnscaldQuest_Bandits
//          q1 -> Current status
//          q2 -> Treasure Key Timer
//          q3 -> Bandit Hood Timer

012-2,47,40,0	script	Helena	NPC_FEMALE,{
    .@q=getq(HurnscaldQuest_Bandits);

    // Stage 1: Level 30, collect Treasure Key
    if (BaseLevel < 30) goto L_TooWeak;
    if (.@q == 0) goto L_Start;
    if (.@q == 1) goto L_Return;

    // Stage 2: Level 40, collect Bandit Hood
    if (BaseLevel < 40) goto L_Weak;
    if (.@q == 2) goto L_Quest;
    if (.@q == 3) goto L_Hood;

    // Stage 3: Level 45, defeat Bandit Lord
    if (BaseLevel < 45) goto L_Busy;
    if (.@q == 4) goto L_BanditLord;
    if (.@q == 5) goto L_Busy;
    if (.@q == 6) goto L_Busy;
    if (.@q == 7) goto L_Finish;

    goto L_Daily;

L_Daily:
    .@k=getq2(HurnscaldQuest_Bandits);
    .@h=getq3(HurnscaldQuest_Bandits);
    mesn;
    mesq l("Ah, @@, my friend! Are you here to help us with 5 @@? Or perhaps you have 5 more @@ to show that Hurnscald is getting safer?", strcharinfo(0), getitemlink(TreasureKey), getitemlink(BanditHood));
    mes "";
    select
        rif((.@k < santime()) && countitem(TreasureKey) >= 5, l("I have 5 Treasure keys with me.")),
        rif((.@h < santime()) && countitem(BanditHood) >= 5, l("I have 5 Bandit Hoods with me.")),
        l("Nothing at the moment.");
    mes "";
    switch (@menu) {
        case 1:
            setq2 HurnscaldQuest_Bandits, santime()+(60*60*24);
            delitem TreasureKey, 5;
            Zeny=Zeny+600; // 600/550 = 9.09% bonus
            //Zeny=Zeny+800; // 800/550 = 45.45% bonus
            getexp 2000, 0;
            $HELENA_ST=$HELENA_ST+2;
            mesn;
            mesq l("Many thanks! %%s");
            next;
            goto L_Daily;
        case 2:
            setq3 HurnscaldQuest_Bandits, santime()+(60*60*24);
            delitem BanditHood, 5;
            Zeny=Zeny+250; // 250/155 = 61.29% bonus
            getexp 3000, 5;
            $HELENA_ST=$HELENA_ST+1;
            mesn;
            mesq l("Many thanks! %%s");
            next;
            goto L_Daily;
    }
    close;

L_TooWeak:
    mesn;
    mesq lg("Hello, madam!", "Hello, sir!");
    next;
    mesn;
    mesq l("Ah, we have serious problems of robbery. I need someone really strong to help me, and you don't qualify.");
    close;

L_Weak:
    mesn;
    mesq l("You still need to grow a few levels more before being able to help me out again.");
    close;

L_Busy:
    mesn;
    mesq l("Ah, @@! I'm busy now, can we talk again later?", strcharinfo(0));
    close;

L_DoIt:
    mesn;
    mesq l("@@, we are counting on you! We, the whole Hurnscald town!", strcharinfo(0));
    close;

///////////////////////// Stage 1
L_Start:
    mesn;
    mesq l("Ah, hello.");
    next;
    mesn;
    mesq l("We actually have a problem. Bandits ransacked this tavern, and took a huge loot.");
    next;
    mesn;
    mesq l("We pursued them until the mines, slayed them, and took the chests where they locked our stuff into.");
    next;
    mesn;
    mesq l("The problem is... The slimes ate the keys for the chests. This is not the first time such thing happens.");
    next;
    // 5 ÷ 4.5% = 112 Copper Slimes. You can kill Yellow Slimes too
    mesn;
    mesq l("If you bring us 5 @@, we'll be forever grateful.", getitemlink(TreasureKey));
    select
        l("Don't worry ma'm, I'll recover the Treasure Keys at once."),
        l("Ah... Slimes... Sorry, not my cup of tea...");
    mes "";
    if (@menu == 2)
        close;
    setq HurnscaldQuest_Bandits, 1;
    mesn;
    mesq l("Wonderful! I'll be expecting you back.");
    close;

L_Return:
    mesn;
    mesq lg("Adventurer, did you brought me what I asked? I see you have @@/5 @@.","Adventurer, did you brought me what I asked? I see you have @@/5 @@.", countitem(TreasureKey), getitemlink(TreasureKey));
    mes "";
    select
        rif(countitem(TreasureKey) >= 5, l("Yes, take it.")),
        rif(countitem(TreasureKey) < 5, l("No, I'll be back with them.")),
        l("Hm, can we talk again later?");
    mes "";
    if (@menu != 1)
        close;
    delitem TreasureKey, 5;
    Zeny=Zeny+1000;
    getexp 1599,0; // 20% from needed exp
    setq HurnscaldQuest_Bandits, 2;
    mesn;
    mesq l("Hey hey... Good job! We can now use again the stuff we recovered from the bandits.");
    next;
    mesn;
    mesq l("Here is 1000 GP for your efforts. Thanks for making Hurnscald a better place to live.");
    next;
    mesn;
    mesq l("This happens quite often, too. My friends and I are always collecting keys to reduce beer price on Hurnscald. %%2");
    close;


///////////////////////// Stage 2
L_Quest:
    mesn;
    mesq l("Ah, @@, good thing you are here.", strcharinfo(0));
    next;
    mesn;
    mesq l("Bandits are a huge threat to Hurnscald. We're just a small farming town, and they're countless.");
    next;
    mesn;
    mesq l("I did a travel to their cave, I wondered why their faces are never seen. Reason is that they're monsters.");
    next;
    mesn;
    mesq l("Well, perhaps a few of them are rebels, I mean, people like us, but many of them are monsters.");
    next;
    mesn;
    mesq l("Unfortunately, I was cursed to never enter their cave again. They used a @@ to do that!", getitemlink(BlueManaPearl));
    next;
    if (countitem(BlueManaPearl) >= 1) {
        mesn;
        mesq l("Ah... You have one too. Be careful with it, please. Many people lost their lives because they didn't handled that correctly.");
        next;
    }
    mesn;
    mesq l("Thankfully that item is too rare. I want somebody to slay the bandit leader, but if you want to do it, you must prove yourself.");
    next;
    mesn;
    mesq l("I will reward whoever kills the current bandit leader, of course.");
    select
        l("I would gladly aid you to get rid of that scourge."),
        l("Sorry, I forgot my courage on my other set of pants.");
    mes "";
    if (@menu == 2)
        close;
    setq HurnscaldQuest_Bandits, 3;
    mesn;
    mesq l("Wonderful! So, how about a warm up?");
    next;
    // 8% drop. 10 / 8% = avg. 125 bandits to kill
    // And for once, I won't require these bandits to be from Hurnscald Bandit Cave.
    mesn;
    mesq l("Bring me 10 @@. I'll pay you some money for that, of course.", getitemlink(BanditHood));
    close;

L_Hood:
    mesn;
    mesq lg("Adventurer, did you brought me what I asked? I see you have @@/10 @@.","Adventurer, did you brought me what I asked? I see you have @@/10 @@.", countitem(BanditHood), getitemlink(BanditHood));
    mes "";
    select
        rif(countitem(BanditHood) >= 10, l("Yes, take it.")),
        rif(countitem(BanditHood) < 10, l("No, I'll be back with them.")),
        l("Hm, can we talk again later?");
    mes "";
    if (@menu != 1)
        close;
    inventoryplace LeatherPatch, 1;
    delitem BanditHood, 10;
    Zeny=Zeny+2000;
    getexp 6842,0; // 20% from needed exp
    getitem LeatherPatch, 1;
    setq HurnscaldQuest_Bandits, 4;
    mesn;
    mesq l("Hey hey... Good job! I was worried you would ruin their hoods before being able to take them.");
    next;
    mesn;
    mesq l("Here is 2000 GP for your efforts. Thanks for making Hurnscald a better place to live.");
    next;
    mesc l("You also gained a @@. Bows are very slow, so you should talk to the Blacksmith to make a Quiver.", getitemlink(LeatherPatch));
    close;


///////////////////////// Stage 3
L_BanditLord:
    mesn;
    mesq l("So... I won't say you can't do it, @@. I will just say killing the Bandit Lord is no easy task.", strcharinfo(0));
    next;
    mesn;
    mesq l("The @@ is not only a fearsome and ruthless leader. He is strong, and he have tricks on his sleeve.", getmonsterlink(BanditLord));
    next;
    mesn;
    mesq l("He will summon allies if he think you have the upper hand. So take care if you are going ranged.");
    next;
    mesn;
    mesq l("I know another Bandit Lord will take up his place, but the loss of their leader will make bandits scatter long enough.");
    next;
    mesn;
    mesq l("Will you help me- no, I mean, will you help us, the whole town of Hurnscald?");
    next;
    select
        l("Sorry, I need to better prepare myself."),
        l("I would gladly give my life for such noble goal.");
    mes "";
    if (@menu == 1)
        close;
    setq HurnscaldQuest_Bandits, 5;
    mesn;
    mesq l("...You have courage. Many people tried and failed.");
    next;
    mesn;
    mesq l("I know where the Bandit Lord room is, and I have a guard stationed not far from there. Ask him for the key.");
    next;
    mesn;
    mesq l("One last thing... Good luck. This is a long shot, so don't hesit in running away.");
    close;

L_Finish:
    mesn;
    mesq l("Ah... You did it!");
    next;
    mesn;
    mesq l("That's easy to know, because the bandits are less coordinated. Perhaps we will be able to sleep in peace this night!");
    next;
    inventoryplace ForestArmor, 1;
    getitem ForestArmor, 1;
    getexp 5842,93; // 10% from needed exp + JExp level 6
    setq HurnscaldQuest_Bandits, 8;
    mesn;
    mesq l("Here is the @@, like my armor, and one of the best for rangers.", getitemlink(ForestArmor));
    next;
    mesn;
    mesq l("Any friend of Hurnscald is my friend too. Come to me again, if you want to do daily quests!");
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FairyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ForestArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonSkirt);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 17);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 7);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
