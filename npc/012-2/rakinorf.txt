// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    Current Hurnscald Mayor
// Variable:
//      SQuest_Sponsor
//      Quest ID: 4

012-2,34,79,0	script	Rakinorf, Mayor	NPC_ANSELMO_BR,{
    function resetSQS {
        setq SQuest_Sponsor, 0, gettime(GETTIME_MONTH);
    }
    // Check monthly quest
    .@d=getq2(SQuest_Sponsor);
    if (.@d != gettime(GETTIME_MONTH)) resetSQS();

    .@n=getq(General_Narrator);
    .@q=getq(SQuest_Sponsor);
    mesn;
    mesq l("Ah... I need more beer to keep going... This is so awful...");
    // Handle main storyline
    if (.@n >= 5 && .@n <= 6)
        goto L_MainStory;
    // Handle main storyline

    // Core
    .@q=getq(SQuest_Sponsor);
    if (!(.@q & .questID) && getgmlevel()) goto L_Menu;
    close;

L_Menu:
    mesq l("I will reward you for 7 @@.", getitemlink(.itemID));
    mes "";
    menu
        rif(countitem(.itemID) >= 7, l("Here they are!")), L_Finish,
        l("Where can I find them?"),L_Where,
        l("No, thanks."),L_Close;

L_Finish:
    delitem .itemID, 7;
    getitem StrangeCoin, rand(2,4);
    .@q=getq(SQuest_Sponsor);
    setq1 SQuest_Sponsor, .@q | .questID;
    mes "";
    mesn;
    mesq l("Many thanks! Come back later to bring me extra @@!", getitemlink(.itemID));
    close;

L_Where:
    mes "";
    mesq l("Ah, there are lots with Melina, downstairs.");
    next;

L_Close:
    closedialog;
    goodbye;
    close;

// Main Storyline
L_MainStory:
    next;
    // 5 - First time speaking
    // setq2
    //      0 - Not Assigned
    //      1 - Need to bring Beer
    //      2 - Need to find out how Arkim is doing
    //      3 - Complete, Reluctancy node
    //      4 - Told about Hurnscald Liberation Day, but didn't told where previous mayor is
    //      5 - Told you to tell Airlia about.
    //      6 - Airlia told you what she knew, must talk to Rakinorf
    //      7 - Need to get levels (standard quest)
    // 6 - Must visit Halinarzo Library
    // Note: Expires on logout: @returnpotion_expiredate

    if (.@n == 5)
        .@n2=getq2(General_Narrator);
    else
        .@n2=99;

    if (.@n2 == 5) {
        mesn;
        mesq l("You should talk to Airlia again to understand what Lua wants me to do.");
        mes "";
    }

    select
        rif(getgmlevel(), l("Maybe I could give you more beer?")),
        rif(.@n2 == 0, l("I know it may sound silly, but I need your help to find out who I am.")),
        rif(.@n2 == 1, l("I have brought you the beer.")),
        rif(.@n2 == 2, l("I know how many bat teeth and wings Arkim collected.")),
        rif(.@n2 == 3, l("I know it may sound silly, but I need your help to find out who I am.")),
        rif(.@n2 == 4, l("Where can I find the previous mayor?")),
        rif(.@n2 == 6, l("Airlia told me you should lend me a Return Potion.")),
        rif(.@n2 == 7, l("I think I'm ready.")),
        rif(.@n == 6 && countitem(BrokenWarpCrystal), l("The warp crystal broke.")),
        //rif(.@n == 6 && (!countitem(ReturnPotion)||@returnpotion_expiredate<gettimetick(2)), l("I need more return potions.")),
        l("Ok, thanks.");

    mes "";
    switch (@menu) {
        case 1:
            goto L_Menu; break;
        case 2:
            mesn;
            mesq l("Not now... *hic* I need @@... *hic* Bring me @@ if you *hic* can...", getitemlink(Beer), l("four"));
            setq2 General_Narrator, 1;
            break;
        case 3:
            if (countitem(Beer) < 4) {
                mesn;
                mesq l("That's not *hic*... That's not what I asked you for...");
                next;
                mesn;
                mesq l("Please bring me *hic*... Bring me @@ @@!", 4, getitemlink(Beer));
                close;
            }
            delitem Beer, 4;
            getexp BaseLevel*10, JobLevel*3;
            setq2 General_Narrator, 2;
            mesn;
            mesq l("Aaaaaaaahhhhh.... Much better now *hic*.");
            next;
            mesn;
            mesq l("Could you *hic* do me a favor? Arkim, on a cave southeast of *hic* here, is collecting some stuff.");
            next;
            mesn;
            mesq l("He's an *hic* hermit, and collects *hic* Bat teeth and wings... Tell me how many he collected!");
            break;
        case 4:
            mesn;
            mesq l("So? How many *hic* he collect?");
            mesc l("Protip: Arkim is in a cave southeast of here and is an hermit.");
            input .@am;
            mes "";
            // If players give up to 10 wings/teeths since you check, I don't mind
            if (.@am < ($ARKIM_ST-10) || .@am > $ARKIM_ST)
                goto L_Fail;
            mesn;
            mesq l("Thanks. You can go, now.");
            getexp BaseLevel*2, JobLevel;
            setq2 General_Narrator, 3;
            break;
        case 5:
            mesn;
            mesq lg("Aaaaaaah, gal... Just let me drink in peace.", "Aaaaaaah, boy... Just let me drink in peace.");
            next;
            mesn strcharinfo(0);
            mes l("T.T \"Why I think you are withdrawing information all along?\"");
            next;
            mesn;
            mesq l("Sure, sure... Why are you even *hic* asking me this, anyway? Do I look like your... *hic* your father or something?!");
            next;
            select
                l("Maybe you do, your jerk! Stop drinking! Lua told me that you could help me! DO YOUR JOB!!"),
                l("Sorry, Mister Rakinorf, but it was Lua that said you could help me.");
            next;
            /*
            if (@menu == 1)
                Karma=Karma+1;
            else
                Karma=Karma-1;
            */
            setq2 General_Narrator, 4;
            mesn strcharinfo(0);
            mesq l("All she told me was that I needed to visit Halinarzo, but was too weak, and should look for you instead.");
            next;
            // Rakinorf gets somber after that
            mesn;
            mesq l("Ah, Halinarzo... Dangerous place. Tulimshar route is plagued with snakes, Hurnscald route is more often than not flooded.");
            next;
            mesn;
            mes l(">.> \"Sorry pal, I have no idea what she meant by that.\"");
            next;
            mesn;
            mesq l("You see, just @@ ago, Hurnscald was liberated from a massive monster attack.", FuzzyTime($HURNS_LIBDATE));
            mesq l("Dragonstar and Aisen did their best along many others, and managed to liberate Hurnscald, though!");
            next;
            mesn;
            mesq l("Well, problem is, after that, there was demand for an election. The previous mayor lost. I won.");
            next;
            mesn;
            mesq l("That's basically how democracy works. You are warranted to stay on the office until something goes gravely wrong.");
            mesq l("Now I just drink down my worries and hope for the best.");
            next;
            mesn;
            mesq l("The previous mayor was an amazing mayor, but even so, he lost the office after the Monster King attacked.");
            next;
            mesn;
            mesq l("I'm afraid I can't be of any help to you.");
            next;
            mesn;
            mesq l("And now... Somber time is over! Time to get back to drinking! Yaaaay!!");
            break;
        case 6:
            setq2 General_Narrator, 5;
            mesn;
            mesq l("His daughter, Airlia, is not air-headed as her mother Lia. Try asking her instead.");
            break;
        case 7:
            mesn;
            mesq l("Whaaaaaaat *hic* is she *hic* have she gotten crazy? *hic*");
            next;
            mesn;
            mesq l("That's a *hic* precious item, and you're *hic* not nearly *hic* strong or worth enough to use it!");
            next;
            mesn;
            mesq l("You will need to *hic* prove yourself to *hic* me and the town first...");
            next;
            mesc b(l(".:: Main Quest 2-2 ::.")), 3;
            msObjective(BaseLevel >= 35, l("* @@/@@ Base Level", BaseLevel, 35));

            if (JobLevel >= 15)
                mesc l("* @@/@@ Job Level", JobLevel, 15), 2;
            else
                mesc l("* @@/@@ Job Level", JobLevel, 15), 9;

            if (countitem(Scythe))
                mesc l("* @@/@@ @@", countitem(Scythe), 1, getitemlink(Scythe)), 2;
            else
                mesc l("* @@/@@ @@", countitem(Scythe), 1, getitemlink(Scythe)), 9;

            if (getq(HurnscaldQuest_BloodDonor))
                mesc l("* Donate blood at least once."), 2;
            else
                mesc l("* Donate blood at least once."), 9;

            setq2 General_Narrator, 7;
            next;
            //mesn;
            if (!countitem(Scythe))
                mes l("- Help the farmers. We rely a lot on agriculture.");
            if (!getq(HurnscaldQuest_BloodDonor))
                mes l("- To donate blood, go to the hospital and ask about it.");
            if (JobLevel < 15)
                mes l("- Boss give more Job experience, but as long that you keep killing, you'll keep gaining.");
            if (BaseLevel < 35)
                mes l("I think Lieutenant Paul had a bounty for extra experience.");
            break;

        case 8:
            mesc b(l(".:: Main Quest 2-2 ::.")), 3;
            msObjective(BaseLevel >= 35, l("* @@/@@ Base Level", BaseLevel, 35));

            msObjective(JobLevel >= 15, l("* @@/@@ Job Level", JobLevel, 15));

            msObjective(countitem(Scythe), l("* @@/@@ @@", countitem(Scythe), 1, getitemlink(Scythe)));

            msObjective(getq(HurnscaldQuest_BloodDonor), l("* Donate blood at least once."));

            next;
            if (BaseLevel >= 35 &&
                JobLevel >= 15 &&
                countitem(Scythe) &&
                getq(HurnscaldQuest_BloodDonor))
                goto L_Complete;
            mesn;
            if (!countitem(Scythe))
                mes l("- Help the farmers. We rely a lot on agriculture.");
            if (!getq(HurnscaldQuest_BloodDonor))
                mes l("- To donate blood, go to the hospital and ask about it.");
            if (JobLevel < 15)
                mes l("- Boss give more Job experience, but as long that you keep killing, you'll keep gaining.");
            break;

        case 9:
            mesn;
            mesq l("I don't have a spare. They're *hic* done by ANISE INC., their headquarters are in Frostia.");
            next;
            mesn;
            mesq l("Frostia is a *hic* town way way north of here. They *hic* hate humans, so... Good luck?");
            break;
        /*
        case 10:
            if (countitem(ReturnPotion))
                delitem ReturnPotion, countitem(ReturnPotion);
            inventoryplace ReturnPotion, 5;
            getitem ReturnPotion, rand(2,5);
            @returnpotion_expiredate=gettimetick(2)+1200+rand(300,1200); // 20 minutes + 5~20 minutes == last 25~40 minutes
            mesn;
            mesq l("There... *hic* They will expire *hic* in just @@... Or on logout.", FuzzyTime(@returnpotion_expiredate));
            break;
        */
    }
    close;

L_Fail:
    mesn;
    mes l("Eh? You sure?");
    if (.@am > $ARKIM_ST)
        mes l("I don't think he could have collected that many!");
    else
        mes l("Last time they told me he collected more than that...");
    next;
    mesn;
    mesq l("Could you try again, please?");
    // No penalty? Really??
    close;

L_Complete:
    inventoryplace ReturnPotion, 5, HalinWarpCrystal, 1, HurnsWarpCrystal, 1;
    mesn;
    mesq l("Good, you *hic* proved your *hic* worth. I'll give you them.");
    next;
    mesn;
    mesq l("Here are EXTREMELY VALUABLE warp crystals, and the *hic* fabled return potions.");
    next;
    mesn;
    mesq l("Halinarzo is a *hic* level 50 area. So please *hic* be careful!");
    mesc l("WARNING: Warp Crystals can break after use and have a cooldown.");
    mesc l("Return Potions works instantly. Talk to Wyara to get more.");
    setq General_Narrator, 6, 0;
    getitem ReturnPotion, 10;
    getitem TulimWarpCrystal, 1;
    getitem HurnsWarpCrystal, 1;
    getexp BaseLevel*400, JobLevel*50;// Reference Levels: (35, 15)
    close;

OnInit:
    .questID=4;
    .itemID=Beer;
    .sex=G_MALE;
    .distance=5;
    end;
}

