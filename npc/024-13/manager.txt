// TMW2: Moubootaur Legends scripts.
// Author:
//    Jesusalva
// Description:
//    Real Estate System

// ESTATE_ID → Instance ID of the Estate (required for NPCs, expire)
// ESTATE_RENTTIME → When the rent will expire
// ESTATE_MOBILIA_2 → Bitmask of mobilia currently purchased on Monster Collision (6) (Use on walls only)
// ESTATE_MOBILIA_4 → Bitmask of mobilia currently purchased on Air Collision (2)
// ESTATE_MOBILIA_8 → Bitmask of mobilia currently purchased on Water Collision (3)
// ESTATE_MOBILIA_32 → Bitmask of mobilia currently purchased on Yellow Collision (4)
// ESTATE_MOBILIA_64 → Bitmask of mobilia currently purchased on Normal Collision (1)
// ESTATE_MOBILIA_128 → Bitmask of mobilia currently purchased on Player Collision (5)

// REAL_ESTATE_CREDITS → Credits equivalent to GP the player have. Will be used first.

// The sign is the main controller
024-13,31,32,0	script	Apartment Manager	NPC_ELF,{
    if (ESTATE_RENTTIME < gettimetick(2))
        goto L_RentAvailable;

    mesn;
    mesq l("Your rent is valid for @@.", FuzzyTime(ESTATE_RENTTIME));
    mesc l("Apartment rents cannot be renewed until they expire. Furniture won't be lost.");
    close;

L_RentAvailable:
    do
    {
        mesc l("This Real Estate is available for rent for only @@ GP!", format_number(.price));
        .@gp=REAL_ESTATE_CREDITS+Zeny;
        mesc l("You currently have: @@ GP and mobiliary credits", format_number(.@gp));
        next;
        select
            rif(.@gp > .price, l("Rent it! Make it mine!")),
            l("Information"),
            l("Don't rent it");

        // You want to rent
        if (@menu == 1) {
            realestate_payment(.price);

            // Payment done, you can now acquire the house for a month
            ESTATE_RENTTIME=gettimetick(2)+.time;

            mesc l("Rent successful for 30 days!");
        } else if (@menu == 2) {
            mesc l("You can rent this house to make it yours.") + " " + l("The rent lasts 30 days.");
            mesc l("Then you'll be able to buy furniture and utility.");
            mesc l("This is an apartment. You cannot renew until it expire, and cannot invite guests.");
            next;
            mesc l("Both rent and furniture are bought using money, however, there are mobiliary credits.");
            mesc l("Mobiliary Credits is a special currency which can only be used on real estate.");
            mesc l("It's obtained with ADMINS or by selling furniture. It is sumed to money and used first.");
            next;
        }
    } while (@menu == 2);
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;

    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, BowlerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, LeatherTrousers);

    .price=5000; // Monthly rent price.
    .time=2592000; // Defaults to 30 days
    end;

}

// Door entrance
024-13,29,28,0	script	#RES_PPL	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if (ESTATE_RENTTIME < gettimetick(2))
        goto L_RentAvailable;

    // Warp you to your apartment if exists in memory already.
    // Build the instance otherwise.

    // Well, "checking if instance exist by mapname" is an illusion.
    // So we try to build and if we fail, we warp the player to the instance.
    .@ID=getcharid(0);
    @MAP_NAME$="ples@"+str(.@ID); // Max 4 chars for map name

    .@INSTID = instance_create("ples@a"+(.@ID), getcharid(3), IOT_CHAR);

    // Instance already exists - .@INSTID returns "-4"
    if (.@INSTID == -4) {
        warp @MAP_NAME$, 33, 33;
        end;
    }

    // Attach the map
    .@instanceMapName$ = instance_attachmap("024-14", .@INSTID, 0, @MAP_NAME$);

    // Record important stuff & load furniture
    ESTATE_ID=.@INSTID;
    addtimer(20, instance_npcname("Doorbell#RES_PPL", .@INSTID)+"::OnReload");
    addtimer(70, instance_npcname("NPCs#RES_PPL", .@INSTID)+"::OnReload");

    // It'll be self-destroyed eventually...
    instance_set_timeout(1000000, 1000000, .@INSTID);
    instance_init(.@INSTID);
    warp @MAP_NAME$, 33, 33;
    end;

L_RentAvailable:
    dispbottom l("You do not have booked an apartment here.");
    close;

OnInit:
    .distance=1;
    end;

}

