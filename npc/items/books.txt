// TMW-2 script.
// Author:
//    Jesusalva
//    gumi
//    Tirifto
// Description:
//    Books used by TMW-2. Some are from evol.

function	script	FishingBook	{
    narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("To get started with fishing, you'll need two things: a fishing rod and a bait."),
        l("You just need one fishing rod, although you should take more than one single bait.");

    @menu = 0; // reset for the rif

    do
    {
        narrator S_NO_NPC_NAME,
            l("Please select a chapter:");

        mes "";

        select
            rif2(1, true, l("Ch 1 — Fishing apparatus")),
            rif2(2, true, l("Ch 2 — Baits")),
            rif2(3, true, l("Ch 3 — Location")),
            rif2(4, true, l("Ch 4 — Casting")),
            rif2(5, true, l("Ch 5 — Reeling")),
            l("Close");

        switch(@menu)
        {
            case 1:
                narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("You'll want your fishing rod to be flexible but solid."),
                    l("Comfortable grip is important especially for newcomers, since they'll be holding it for quite a while.");
                break;
            case 2:
                narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("You can use many diverse items to lure fishes."),
                    l("Most common and widely popular in the fish realm are @@ and pieces of @@.",
                        getitemlink(SmallTentacles), getitemlink(Bread)),
                    l("Some types of fish also enjoy @@ quite a bit.",
                        getitemlink(Aquada)),
                    l("Some people, however, prefer to fish with more unorthodox baits, such as @@ or @@.",
                        getitemlink(RoastedMaggot), getitemlink(CaveSnakeTongue)),
                    l("Other food can be used as a bait, too.");
                break;
            case 3:
                narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Find yourself a nice dry spot on a coast where you can easily reach into deep water."),
                    l("Fishing next to shallow water is not going to work well, because fishes seldom go there."),
                    l("You can easily identify fishing spots, small bubbles and fishes are visible from the surface."),
                    l("Don't forget to come as close as possible to these spots!");
                break;
            case 4:
                narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Toss the hook into deep water by clicking on where you want to cast it."),
                    l("Make sure to put on a bait after you click, though!"),
                    l("After that, stay still and be patient, but also alert!");
                break;
            case 5:
                narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("To successfully catch a fish, you need to pull up your hook by clicking it, right after it submerges."),
                    l("Should you be too quick or wait too long, you will most likely fail.");
                break;
        }
    } while (@menu != 6);
    return;
}

000-0,0,0,0	script	#Book-Fishing1	NPC_HIDDEN,{

    function read_book {
        setnpcdialogtitle l(.book_name$);
        FishingBook();
        closeclientdialog;
        end;
    }

OnShelfUse:
    if (openbookshelf())
        read_book;
    bye;

OnUse:
    if (openbook())
        read_book;
    bye;

OnInit:
    .book_name$ = getitemname(FishingGuideVolI);
    .sex = G_OTHER;
    .distance = 1;
    end;
}



function	script	PetcaringBook	{
    narrator 1,
    l("So you have now a pet, who is loyal to you. It'll follow you everywhere, but there are two things you must know."),
    l("Do not let intimacy and hunger get to zero. If any of those get to zero, it'll leave you forever."),
    l("Pets must keep a strict diet. Pious eats Piberries, Bhoppers eat Aquadas, and Maggots eats Bug Legs."),
    l("White Cats drink Milk, Forest Mushroom eats Moss, Black Cats eats marshmallow. Keep in mind whatever they eat."),
    l("However, you should only give food when it's hungry, otherwise it'll believe you're a bad owner and intimacy will decrease."),
    l("Dying will also decrease the pet intimacy, and there are bonuses when your intimacy is high!"),
    l("To perform most actions, like feeding and renaming, just right-click it. You can even put it back on the egg if its following gets too annoying. When in the egg, they will not feel hunger."),
    l("Give your pet a nice name, and keep it healthy, and you'll be a successful pet owner!"),
    l("Some pets will also collect loot for you, right click on it so it drop whatever it is holding for you."),
    l("...And if you're still trying to check your pet stats, just hover it with your mouse. Thanks."),
    l("-- Animals Protection Agency of Hurnscald");
    return;
}

000-0,0,0,0	script	#Book-Petcaring	NPC_HIDDEN,{
    function read_book {
        PetcaringBook();
        close;
    }

OnShelfUse:
    @book_name$ = .bookname$;
    if (openbookshelf ())
        read_book;
    close;
OnUse:
    @book_name$ = .bookname$;
    if (openbook ())
        read_book;
    close;
OnInit:
    .bookname$ = "Fluffy Animals who Love Their Owners";
    .sex = G_OTHER;
    .distance = 1;
    end;
}























000-0,0,0,0	script	#Book-JGrimorium	NPC_HIDDEN,{

    function read_book {

        setnpcdialogtitle l(.book_name$);

        narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("I, second sage of Fate, write this book. The knowledge on it shall guide you to the Secret Of Mana.");

        @menu = 0; // reset for the rif

        do
        {
            narrator S_NO_NPC_NAME,
                l("Please select a chapter:");

            mes "";

            select
                rif2(1, MAGIC_LVL, l("Ch 1 — Prologue")),
                rif2(2, MAGIC_EXP, l("Ch 2 — Mana Magic vs Common Magic")),
                rif2(3, MAGIC_LVL, l("Ch 3 — Subclass")),
                rif2(4, true, l("Open Fishing Book")),
                rif2(5, true, l("Open Petcaring Book")),
                rif2(6, CRAFTQUEST, l("Open Recipe Book")),
                rif2(7, true, l("Read Rules")),
                l("Close");

            switch(@menu)
            {
                case 1:
                    narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                        l("Mana is something which existed since the being, but nobody knows much about."),
                        l("This book will write itself, and reveal you the Secret Of Mana."),
                        l("Give it time, increase your magic power, and you'll find out the truth."),
                        l("You are a @@º degree mage. This book allows you many new possibilities.", MAGIC_LVL);
                    break;
                case 2:
                    narrator S_FIRST_BLANK_LINE | S_LAST_NEXT,
                        l("There are two kind of magic: Mana Skills and Magic Skills."),
                        l("They look like the same, but they're not. Mana Skills have a Magic Experience meter and have different rules."),
                        l("Re-casting the same mana skill won't give you magic experience. Magic Skills doesn't have this system, and level up on quest or, depending on the skill, on the skill window, using Job Level."),
                        l("The easiest way to identify is: Mana Skills never target a single foe. This grimorium reports your Mana Skills.");
                    if (getskilllv(TMW2_ZARKOR))
                        mesc l("Summon Cave Maggot - @sk-zarkor");
                    if (getskilllv(TMW2_KALMURK))
                        mesc l("Summon Maggot - @sk-kalmurk");
                    if (getskilllv(TMW2_PARUM))
                        mesc l("Wood Manipulation - @sk-parum");
                    if (getskilllv(TMW2_TRANSMIGRATION))
                        mesc l("Transmutation - @sk-trans");
                    if (getskilllv(TMW2_DEMURE))
                        mesc l("Divine Rage - @sk-demure");
                    if (getskilllv(TMW2_DRAGOKIN))
                        mesc l("Summon Dragons - @sk-dragokin");
                    if (getskilllv(TMW2_LIMERIZER))
                        mesc l("Summon Slimes - @sk-limerizer");
                    if (getskilllv(TMW2_HALHISS))
                        mesc l("Summon Snakes - @sk-halhiss");
                    if (getskilllv(TMW2_KALWULF))
                        mesc l("Summon Wolverns - @sk-kalwulf");
                    if (getskilllv(TMW2_FAIRYKINGDOM))
                        mesc l("Summon Fairies - @sk-fairykingdom");
                    if (getskilllv(TMW2_FROZENHEART))
                        mesc l("Summon Yetis - @sk-frozenheart");
                    if (getskilllv(TMW2_STONEHEART))
                        mesc l("Summon Terranites - @sk-stoneheart");
                    if (getskilllv(TMW2_KALBOO))
                        mesc l("Summon Mouboo - @sk-kalboo");
                    if (getskilllv(TMW2_KALSPIKE))
                        mesc l("Summon P. Spiky Mushroom - @sk-kalspike");
                    if (getskilllv(TMW2_CUTEHEART))
                        mesc l("Summon Fluffies - @sk-cuteheart");
                    if (getskilllv(TMW2_PLANTKINGDOM))
                        mesc l("Summon Plants - @sk-plantkingdom");

                    next;
                    ShowAbizit(true);
                    next;
                    break;
                case 3:
                    /*
                    mesc l("You currently have @@/@@ subclass(es).", total_subclass(),max_subclass());
                    if (MAGIC_SUBCLASS & CL_PALADIN)
                        mesc lg("Paladin");
                    if (MAGIC_SUBCLASS & CL_TANKER)
                        mesc lg("Tanker");
                    if (MAGIC_SUBCLASS & CL_BERSERKER)
                        mesc lg("Berserker");
                    if (MAGIC_SUBCLASS & CL_RANGER)
                        mesc lg("Ranger");
                    if (MAGIC_SUBCLASS & CL_SNIPER)
                        mesc lg("Sniper");
                    if (MAGIC_SUBCLASS & CL_WIZARD)
                        mesc lg("Wizard");
                    if (MAGIC_SUBCLASS & CL_SAGE)
                        mesc lg("Sage");
                    if (MAGIC_SUBCLASS & CL_PRIEST)
                        mesc lg("Priest");
                    */
                    mesc l("You have @@/@@ magic skill points available.", b(sk_points()), sk_maxpoints());
                    mesc l("Currently, there is no way to reset them. So use them wisely!");
                    next;
                    break;
                case 4:
                    FishingBook();
                    break;
                case 5:
                    PetcaringBook();
                    break;
                case 6:
                    mesc l("WARNING: You still need the @@ to learn new recipes!", getitemlink(RecipeBook)), 1;
                    next;
                    closeclientdialog;
                    doevent("#RecipeBook::OnUse");
                    end;
                    break;
                case 7:
                    GameRules();
                    break;
                default:
                    close;
            }
        } while (true);

        end;
    }

OnShelfUse:
    if (openbookshelf())
        read_book;
    bye;

OnUse:
    read_book;
    bye;

OnInit:
    .book_name$ = getitemname(JesusalvaGrimorium);
    .sex = G_OTHER;
    .distance = 1;
    end;
}

