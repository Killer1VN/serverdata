// TMW-2 Script.
// Evol scripts.
// Author:
//    Reid
//    Jesusalva
// Description:
//    Random heal every x seconds.
//
// Variables:
//    @type
//          0 - Sweeties (lowest)
//          1 - Vegetables
//          2 - Proteins
//          3 - Proccessed
//          4 - Magical (highest)
//   @delay
//      Overrides the lasting time
//   @rarity
//      How rare the item is (how much should it effect)
//      Ranges from 1 to 10.
//
//  Formula:
//      MinHeal %: @rarity * ((@type*1) + 1)
//      MaxHeal %: @rarity * ((@type*2) + 1)
//      Delay: 1 + (@type*2)
//          Sweeties: 1s
//          Vegetables: 3s
//          Proteins: 5s
//          Proccessed: 7s
//          Magical: 9s
//
// *getequipoption(EQI_HEAD_TOP,1,168); → Heal Bonus (should be first bonus on Chef Hat)


-	script	rand_sc_heal	-1,{

    // Add remaning bonus if the last one hasn't finished
    /*
    function remaining_bonus
    {
        if (getstatus(getarg(0)))
        {
            .@old_val1 = getstatus(getarg(0), 1);
            .@old_delay = getstatus(getarg(0), 4) * 1000;

            // Penalty to healing item stack: -20% on previous item bonus
            .@old_val1 = (.@old_val1*8/10);

            // change the delay to prevent fast healing
            if (.@old_delay > @delay)
            {
                @delay = .@old_delay;
                @val1 += .@old_val1;
            }
            else
            {
                @val1 += (.@old_val1 * .@old_delay) / @delay;
            }
        }
        else
        {
            @val1 = @val3;
        }
        return;
    }
    */

OnUse:
    if (@rarity <= 0) {
        Exception("Invalid healing item, deleting without healing effect.");
        end;
    }

    // Calculate healing value in %
    @min=@rarity * ((@type*1) + 1);
    @max=@rarity * ((@type*1) + 1);

    // Vitality raises the minimum healing value in 1%, capped at maximum vlaue
    // It also raises @max up to double
    @max = min(@max*2, @min+(readparam2(bVit)/50));
    @min = min(@max, @min+(readparam2(bVit)/30));

    // Make these abstract % in absolute values
    @min=max(1, MaxHp*@min/100);
    @max=max(3, MaxHp*@max/100);

    // Calculate how much you'll heal
    @val1 = rand2(@min, @max);

    // Calculate delay if it was not given
    if (!@delay || @delay > 60) {
        @delay=1 + ((@type*3)/2);
    }

    // Update val1
    @val1 = (@val1 / @delay) + 1;

    // Decide the healing bonus type. We have four types: S, L, G and M
    // By default, we use 'S'
    .@skill = SC_S_LIFEPOTION;

    // We now have @val1 (new effect), @delay (new delay)
    // But do we have .@v and .@d (old effect and delay)?
    //Meh, just override it
    /*
    if (getstatus(.@skill)) {
        .@v=getstatus(.@skill, 1);
        .@d=getstatus(.@skill, 5);
    }

    // If there WAS an effect previously, get ponderate average
    if (.@v > 0) {
        @val1=ponderate_avg(@val1, @delay, .@v, .@d);
        @delay=ponderate_avg(@delay, @val1, .@d, .@v);
    }
    */

    // Put the delay in ms
    @delay *= 1000;

    // Apply the effect and finish
    sc_end .@skill;
    sc_start2 .@skill, @delay, @val1, 1;

    // Clear stuff
    // @val1 must be preserved for cross-reading
    @delay=0;
    @type=0;
    @rarity=0;
    @min=0;
    @max=0;
    // @val1=0;
    end;
}
