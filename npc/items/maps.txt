// TMW2 Script.
// Authors:
//    Jesusalva
// Description:
//    World Map Items

function	script	wmap	{
    .@loc$=getarg(0, LOCATION$);
    setnpcdialogtitle l("World Map - @@", l(.@loc$));
    setskin "map_"+.@loc$;
    mes "Please keep your ManaPlus updated.";
    select("Ok");
    setskin "";
    closeclientdialog;
    return;
}

