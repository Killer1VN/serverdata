// Evol scripts.
// Author:
//    Reid
// Description:
//    Blacksmith's assistant of Artis
// Variables:
//    ArtisQuests_Enora
// Values:
//    0    Default.
//    1    BlackSmith quest delivered.
//    2    Chelios Quest given.
//    3    Chelios Quest done.
//    4    BlackSmith gave the sword.

test,2,4,0	script	karim	NPC_PLAYER,{

    .Item1 = RightCraftyWing;
    .Item2 = LeftCraftyWing;
    .ItemReward = FlightTalisman;

    function quest_beggining{
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("my name is karim can you help me?.");

        setq Karim_Quest, 1;
        return;
    }

    function quest_left {
        if (countitem(.Item2) > 0)
        {
            delitem .Item2, 1;
            if (rand(10) == 1)
            {
                mesq l("Yay, it worked! You get a good wing.");
                getitem .ItemReward, 1;
                setq Karim_Quest, 2;
                close;
            }
            else
            {
                mesq l("This one is useless! Give me another @@.", getitemlink(.Item2));
                return;
            }
        }
        else
        {
            mesq l("You don't have any @@, are you mocking me?", getitemlink(.Item2));
            return ;
        }
    }

    function quest_right {
        if (countitem(.Item1) > 0)
        {
            delitem .Item1, 1;
            if (rand(5) == 1)
            {
                mesq l("Yay, it worked! You get a good wing.");
                getitem .ItemReward, 1;
                setq Karim_Quest, 2;
                close;
            }
            else
            {
                mesq l("This one is useless! Give me another @@.", getitemlink(.Item1));
                next;
                return;
            }
        }
        else
        {
            mesq l("You don't have any @@, are you mocking me?", getitemlink(.Item1));
            return ;
        }
    }

    function quest_started {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("Can you give me a @@ or a @@", getitemlink(.Item1), getitemlink(.Item2));

        do
        {
            select
            l("a Right Crafty Wing"),
            l("a Left Crafty Wing"),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                quest_right;
                break;
            case 2:
                quest_left;
                break;
        }
        } while (@menu != 3);
        return;
    }

    function quest_completed {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Quest completed.");
        return;
    }

    function quest_restart {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Quest restart.");
        setq Karim_Quest, 0;
        return;
    }

    speech S_LAST_NEXT, l("Can you help me?");

    do
    {
        .@karim = getq(Karim_Quest);
        select
            rif(.@karim == 2, lg("quest completed.")),
            rif(.@karim == 1, l("Hello again can you give you give me some tentacles.")),
            rif(.@karim == 0, l("Hello")),
            l("Debug"),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                quest_completed;
                break;
            case 2:
                quest_started;
                break;
            case 3:
                quest_beggining;
                break;
            case 4:
                quest_restart;
                break;
        }
    } while (@menu != 5);

    closedialog;
    goodbye;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 3;
    end;
}

