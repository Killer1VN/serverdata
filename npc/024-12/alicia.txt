// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Status Reset. (+ Alchemy Recipe?)

024-12,28,29,0	script	Alicia	NPC_ELF_F,{

    speech S_LAST_NEXT,
        l("I am @@, an alchemist specialized in reset potions.", .name$);

L_Menu:
    .@plush_count = BaseLevel*200-(9*200);
    // Lv 10: 200 GP
    // Lv 90: 16.000 GP
    if (BaseLevel > 10)
        .@plush_count = .@plush_count/(BaseLevel/10);

    select
        l("Can you reset my stats please?"),
        l("Can you teach me some Alchemy Recipe?"),
        lg("You are weird, I have to go sorry.");

    switch (@menu)
    {
        case 1:
            goto L_ResetStats;
        case 2:
            goto L_Recipe;
        case 3:
            goto L_Quit;
    }

L_ResetStats:
    mesn;
    mesq l("Status point reset can't be undone. Do you really want this?");

L_ConfirmReset:
    ConfirmStatusReset();
    goto L_Quit;

L_Recipe:
    mes "";
    mesn;
    mesq l("Oh my! Did you rent a house or an apartment and now want to brew stuff, like @@ or @@?", getitemlink(PrecisionPotion), getitemlink(StatusResetPotion));
    next;
    mesn;
    mesq l("...That's your problem, not mine. I am an elf if you haven't noticed.");
    next;
    goto L_Menu;

L_Quit:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Earmuffs);
    setunitdata(.@npcId, UDT_HEADMIDDLE, TneckSweater);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, FurBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 11);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_FEMALE;
    .distance = 5;
    end;

}

