// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    Makes the Time Potion Of Ozthokk

017-6,27,48,0	script	Sheila the Clockmaker	NPC_SHEILA,{
    showavatar NPC_SHEILA;
    mesn;
    mesq l("Ah, hello... is it @@? Nice to meet you!", strcharinfo(0));
    next;
    mesn;
    mesq l("I am @@, student from the ancient secrets of the Mage of Time, Ozthokk.", .name$);
    next;
    mesn;
    mesq l("My greatest realization thus far, besides some time rewinding clocks, is the @@!", getitemlink(TimeFlask));
    next;
    mesn;
    mesq l("I'm not sure what it needs to move on time, but I managed to make it move in space, back to this village, in no time!");
    mesq l("Well, maybe it takes a few hours or days of your life, but who cares! It is instant teleport!");
    next;
    mesn;
    mesq l("If you bring me a @@ and 500 GP I can make one for you!", getitemlink(DragonScales));
    if (!countitem(DragonScales) || Zeny < 500)
        close;
    select
        l("Nice, but no, thanks."),
        l("Pretty cool! I am interested!");
    mes "";
    if (@menu == 1)
        close;
    inventoryplace TimeFlask, 1;
    delitem DragonScales, 1;
    Zeny=Zeny-500;
    getitem TimeFlask, 1;
    mesn;
    mesq l("Neat, neat! Thanks! Here you go, this amazing bottle! It is life-bond, so having multiple won't reduce the time you need to wait...");
    next;
    mesn;
    mesq l("Do not open. Do not attempt to eat. Be careful with it, to don't cause any time paradoxes. I also heard a NPC may be interested on that, by the way!");
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}
