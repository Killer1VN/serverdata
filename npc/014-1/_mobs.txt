// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 014-1: Woodland Mining Camp mobs
014-1,97,59,86,45	monster	Silk Worm	1034,15,30000,30000
014-1,97,67,50,45	monster	Mouboo	1023,12,30000,45000
014-1,85,82,54,35	monster	Squirrel	1032,12,30000,45000
014-1,87,80,52,18	monster	Tipiou	1016,3,30000,45000
014-1,135,91,6,28	monster	Blub	1008,2,30000,30000
014-1,95,58,105,59	monster	Cobalt Plant	1136,2,45000,50000
014-1,88,68,104,43	monster	Mauve Plant	1135,3,45000,50000
014-1,66,53,43,62	monster	Gamboge Plant	1134,3,45000,50000
