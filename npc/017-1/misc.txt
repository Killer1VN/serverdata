// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    Essential scripts any city must have

// Description:
//    The Travelers travel around the world telling stories.
017-1,144,204,0	script	Elen The Traveler	NPC_F_COINKEEPER,{

    mesn;
    if (strcharinfo(0) == $MOST_HEROIC$) mesq l("Wow! Are you @@? Everyone, in every city, talks about you!", $MOST_HEROIC$);
    if (strcharinfo(0) == $MOST_HEROIC$) next;

    mesq l("Hello. I am @@, and I am from a family of travellers. We travel though the whole world, looking for exotic goods.", .name$);
    next;
    mesq l("You can buy rare items with me, or I can tell you about different cities in our world.");

L_Menu:
    mes "";
    menu
        l("I want to trade with you."), L_Trade,
        l("Tell me about Tulimshar."),  L_Tulim,
        l("Tell me about Hurnscald."),  L_Hurns,
        l("Tell me about Nivalis."),    L_Nival,
        l("Tell me about Esperia."),    L_Esper,
        l("Tell me about Artis."),      L_Artis,
        l("Tell me about Frostia."),    L_Frost,
        l("Tell me about Halinarzo."),  L_Halin,
        l("Sorry, I'll pass."), L_Close;

L_Tulim:
    mes "";
    mesn;
    mesq l("Tulimshar is the oldest human city, and its foundation is the year zero of our calendar.");
    next;
    mesq l("The city only flourished because Janett Platinum had the idea to build city walls surrounding this city.");
    next;
    mesq l("The desert climate means you'll find mostly maggots and scorpions. Their drops include cactus drinks, cake, knifes, black pearls, gold, and other common things.");
    next;
    mesq l("You can find for a good price desert equipment and some kind of dyes. You find all sort of crafters, artisans and warriors here.");
    next;
    goto L_Menu;

L_Hurns:
    mes "";
    mesn;
    mesq l("Hurnscald was founded after Tulimshar, in more fertile lands. Their walls are not so sturdy as the ones of Tulimshar.");
    next;
    mesq l("Under the leadership of King Wusher, they were the first to accept immigrants from other races. You will find humans and non-humans there.");
    next;
    mesq l("The fertile climate is ideal for mushrooms. You can also find lots of wood.");
    next;
    mesq l("Their economy provide many edible items and potions.");
    next;
    goto L_Menu;

L_Nival:
    mes "";
    mesn;
    mesq l("Nivalis was the last human settlement built during the First Era.");
    next;
    mesq l("It's cold, harsh climate makes difficult to live there. It was founded by people thrown away from Tulimshar and Hunrscald for political reasons.");
    next;
    mesq l("The cold climate is ideal for slimes, penguins, and other icy creatures. You can find lots of... ice, of course!");
    next;
    mesq l("Some items are only produced in Nivalis. After all, it is hard to work properly with ice in a desert!");
    next;
    goto L_Menu;


L_Esper:
    mes "";
    mesn;
    mesq l("Esperia is the human capital founded at the other continent.");
    next;
    mesq l("After the Great Famine which happened after the Red Queen death, it was founded by people who sailed away in look of new opportunities.");
    next;
    mesq l("It offers better living conditions than Tulimshar, and is bigger and more prosperous. Few monsters live there.");
    next;
    mesq l("However, taxes there are the highest. Living cost is high, and you should avoid buying things there.");
    next;
    goto L_Menu;

L_Artis:
    mes "";
    mesn;
    mesq l("Artis is a city port founded after the Great Famine on the other continent.");
    next;
    mesq l("People say it is the second biggest city from the world.");
    next;
    mesq l("Different kind of monsters live near the city. For example, blubs. I have no idea of what are those.");
    next;
    mesq l("People usually dock there when travelling to the second continent. Nothing exceptional about economy.");
    next;
    goto L_Menu;


L_Frost:
    mes "";
    mesn;
    mesq l("Frostia is the only city known that was not founded by humans.");
    next;
    mesq l("They are strict with who is allowed inside, so you'll need either elf or ukar friends to pass.");
    next;
    mesq l("It is on a huge, icy mountain peak. Rumors about dragons and legendary items to be found.");
    next;
    mesq l("Some of finest elven craftmanship can be found there, like bows, for example.");
    next;
    goto L_Menu;

L_Halin:
    mes "";
    mesn;
    mesq l("Halinarzo was founded to explore Mana Stones.");
    next;
    mesq l("You can find both huge swamps, as huge desertic areas near and on it.");
    next;
    mesq l("Lizards are the main monster found, and they steal gold from innocent bypassers.");
    next;
    mesq l("Without any mana stone left, and because the walls were not very strong, most of the city was destroyed.");
    next;
    mesq l("Unlike many other cities, if you want people in eternal need of items, there is a good place to look.");
    next;
    goto L_Menu;


L_Trade:
    mesn;
    mesq l("Use your @@ as currency!", getitemlink(StrangeCoin));
    next;
    openshop "Aeros Trader";
    closedialog;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, UglyChristmasSweater);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansShorts);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 27);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 11);
    npcsit;

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

// Description:
//    Banker.
017-3,85,41,0	script	Stalman	NPC_LLOYD,{
    Banker(.name$, "Land Of Fire Village", 10000);
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}

// Description:
//    Barber.

017-1,147,176,0	script	Milocat	NPC_ELVEN_FEMALE_ARMOR_SHOP,{
    function setRace {
        clear;
        setnpcdialogtitle l("Debug - Modify Race");
        mes l("Race") + ": " + $@allraces$[Class];
        next;
        mes l("Please select the desired race.");
        select("Human:Ukar:Redy:Elf:Orc:Raijin:Tritan");
        jobchange max(0, @menu-1);
        return;
    }


    mesn;
    mesq l("Hi! Do you want a hair cut?");

    do
    {
        select
            l("What is my current hairstyle and hair color?"),
            l("I'd like to get a different style."),
            l("Can you do something with my color?"),
            rif(is_staff(), l("I am a GM, and I want to change my Race!")),
            l("I'm fine for now, thank you.");

        switch (@menu)
        {
            case 1:
                BarberSayStyle 3;
                break;
            case 2:
                BarberChangeStyle;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Enjoy your new style.");
                    l("Anything else?");
                break;
            case 3:
                BarberChangeColor;
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("I hope you like this color.");
                    l("Anything else?");
                break;
            case 4:
                setRace;
                break;
            case 5:
                speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                    l("Feel free to come visit me another time.");

                goodbye;
        }
    } while (1);
    close;


OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;
}








// Whatever event use #RARE_POINTS
// Which is a script variable, meaning it will be a really special event.
// Side Note: Might as well add a special cap for such situations.
017-1,118,83,0	script	Pydisgner#spoints	NPC_GUGLI,{
    mesn;
    mesq l("Have you came here from Land Of Fire TMWA Server? Do you have the merge points with you?");
    next;
    closeclientdialog;
    openshop;
    end;

OnInit:
    .sex = G_OTHER;
    .distance=5;

	tradertype(NST_CUSTOM);

    // Rares
	sellitem BlacksmithAxe, 64000;
	sellitem MiereCleaver,  32000;
	sellitem Kanabo,        13500;
	sellitem PiouEgg,        8350;
	sellitem AxeHat,         7000;
	sellitem Googles,        5000;

    // Uncommon
	sellitem CrystallizedMaggot, 6000;
	sellitem Sunglasses,         6000;
	sellitem BunnyEars,           800;
	sellitem StrangeCoin,          30;

    // Gamble
	sellitem SupremeGift,30000;
	sellitem PrismGift,  11000;
	sellitem GoldenGift,  4000;
	sellitem SilverGift,  1500;
	sellitem BronzeGift,   500;
	sellitem CasinoCoins,   35;

    // Equipment
	sellitem WoodenSword,      5500;
    sellitem Chainmail,        2500;
    sellitem LeatherShirt,      800;
    sellitem JeansShorts,       600;
    sellitem ShortBow,          500;
    sellitem RoundLeatherShield,200;
    sellitem WoodenBow,         100;
    sellitem Dagger,            100;

    // Consumables
	sellitem TonoriDelight, 80;
	sellitem Aquada,        60;
	sellitem Manana,        50;
	sellitem Cheese,        40;
	sellitem Plushroom,     15;
	sellitem Chagashroom,   15;

    // Useful ores and money stuff
	sellitem IronIngot, 2500;
    sellitem EmptyBottle, 60;
    sellitem Arrow,        4;
    sellitem TolchiArrow,  1;
	sellitem PiouFeathers, 1;


    end;

OnCountFunds:
	setcurrency(#RARE_POINTS);
	end;

OnPayFunds:
    // Due a script bug, you must always retain a point
	if( #RARE_POINTS <= @price )
		end;
    #RARE_POINTS=#RARE_POINTS-@price;
	purchaseok();
	end;

}
