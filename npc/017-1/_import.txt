// Map 017-1: Land Of Fire Village
// This file is generated automatically. All manually added changes will be removed when running the Converter.
"npc/017-1/017-1_stranger_blackbox.txt",
"npc/017-1/_mobs.txt",
"npc/017-1/_warps.txt",
"npc/017-1/boringnpc.txt",
"npc/017-1/drowned_man.txt",
"npc/017-1/estate.txt",
"npc/017-1/fairy_collector.txt",
"npc/017-1/guards.txt",
"npc/017-1/guild.txt",
"npc/017-1/mapflags.txt",
"npc/017-1/misc.txt",
"npc/017-1/nowhere_man.txt",
"npc/017-1/paxel.txt",
"npc/017-1/pet_detective.txt",
"npc/017-1/roger.txt",
"npc/017-1/shops.txt",
"npc/017-1/signs.txt",
"npc/017-1/soul-menhir.txt",
"npc/017-1/stranger.txt",
"npc/017-1/town.txt",
"npc/017-1/townhall.txt",
"npc/017-1/wateranimation.txt",
