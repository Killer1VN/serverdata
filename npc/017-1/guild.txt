// Moubootaur Legends Script
// Author:
//    Jesusalva
// Description:
//  Guild House

017-1,57,33,0	script	#GDoor	NPC_NO_SPRITE,0,0,{
    end;
OnTouch:
    if (getcharid(2) < 1) {
        dispbottom l("The door is locked.");
        end;
    }
    // Warp you to your guild house if exist.
    // Build the instance otherwise.

    // Well, "checking if instance exist by mapname" is an illusion.
    // So we try to build and if we fail, we warp the player to the instance.
    .@ID=getcharid(2);
    @MAP_NAME$="guild@"+str(.@ID); // Max 4 chars for map name

    .@INSTID = instance_create("guilds@a"+(.@ID), getcharid(2), IOT_GUILD);

    // Instance already exists - .@INSTID returns "-4"
    if (.@INSTID == -4) {
        warp @MAP_NAME$, any(34,35), 48;
        end;
    }

    .@instanceMapName$ = instance_attachmap("guilds", .@INSTID, 0, @MAP_NAME$);

    // It'll be self-destroyed eventually...
    instance_set_timeout(1000000, 1000000, .@INSTID);
    instance_init(.@INSTID);
    warp @MAP_NAME$, any(34,35), 48;
    end;

}

017-1,58,32,0	script	#GSign	NPC_NO_SPRITE,{
    mesc l("Guild Hall");
    close;

OnInit:
    .distance=3;
    end;
}
