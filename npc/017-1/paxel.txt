// TMW2/LoF Script
// This is from LoF Forums, to replace LoF Paxel
//----------------------------------------------------
// PRSM Helmet quest (seasonal quest)
// Create Quest: Darlord (Depredador)
//----------------------------------------------------

// Old Paxel turned Raw Coal in Coal... But we don't have Raw Coal.
// He probably used the nice furnace NPC too... We will need another NPC to use that...

017-1,131,22,0	script	Paxel	NPC_PLAYER,0,0,{
	.@q=getq(SQuest_Paxel);
	if(.@q == 2) goto L_Done;
	if (season() != SPRING && !$@GM_OVERRIDE) goto L_OutOfSeason;
	if(.@q == 1) goto L_Ask2;

	if (BaseLevel >= 55) goto L_Ask1;

L_OutOfSeason:
	mesn;
	mesq l("Hello! Calm wind today!");
	menu
		l("Yes, but I like windy days."), L_Wind,
		l("Hmm... sorry but gotta go..."), L_Go;
	close;

L_Go:
	mesn;
	mesq l("Ok, good trip then.");
	close;

L_Wind:
	mesn;
	mesq l("I don't like them, I can't fly straight.");
	next;
	mesn;
	if (BaseLevel < 55)
		mesq l("Well, not like you could understand that with your level! %%a");
	else
		mesq l("This is why I love spring!");
	close;

// ----------------------------------------------------
// Desert Helmet quest start
// ----------------------------------------------------
L_Ask1:
	mesn;
	mesq l("Hello @@! Do you have some @@ with you?", strcharinfo(0), getitemlink(TerraniteOre));

	menu
		rif(countitem(TerraniteOre),l("Yes... right... what about Terranite Ore?")), L_Quest1,
		rif(countitem(TerraniteOre),l("Yes, but I have grown attached to Terranite Ore...")), L_No,
		l("Right, but now I have business in other places."), L_Rude;
	close;

L_Quest1:
	mesn;
	mesq l("Terranite Ore is a really special item. People without helmets can't understand @@'s real power, anyway if you could give me some of them you would understand why they are so special.", getitemlink(TerraniteOre));

	menu
		l("Certainly, you can have all my Terranite Ore."), L_Check1,
		l("Sorry but I am not interested in that."), L_No;
	close;

L_No:
	mesn;
	mesq l("Ok, come back me when you want to know the @@ real power!", getitemlink(TerraniteOre));
	close;

L_Rude:
	mesn;
	mesq l("Well, good luck with your business.");
	close;


// ----------------------------------------------------
// Arc 1: Terranite Ores
// ----------------------------------------------------
L_Check1:
	if (countitem(TerraniteOre) < 4)
		goto L_More_Needed1;

	delitem TerraniteOre, 4;
	getexp 15000, 150;
	setq SQuest_Paxel, 1;
	mesn;
	mesq l("Perfect! you have enough Terranite Ore, I will take them for now but I need other items in order to bring them to life.");
    next;

L_Ask2:
	mesn;
	mesq l("I need a suitable base helmet, some herbs to use my magical dye, and gold:");
	mesc l("@@/1 @@", countitem(MinerHat), getitemlink(MinerHat));
    mesc l("@@/5 @@", countitem(GrassSeeds), getitemlink(GrassSeeds));
	mesc l("@@/100 @@", countitem(PinkAntenna), getitemlink(PinkAntenna));
	mesc l("@@/?? @@", countitem(CobaltHerb), getitemlink(CobaltHerb));
	mesc l("@@/@@ GP", format_number(Zeny), format_number(10000));

	menu
		l("Please have a look, I have what you asked"), L_Check2,
		l("On my way to get what you need."), -;
	close;


// ----------------------------------------------------
// Arc 2: Prsm Helmet
// ----------------------------------------------------
L_Check2:
	if (countitem(PinkAntenna) < 100 || countitem(MinerHat) < 1 || countitem(GrassSeeds) < 5 || countitem(CobaltHerb) < 60)
		goto L_More_Needed2;
	if (Zeny < 10000) {
		mesn;
		mesq l("You can't afford my work! Do some odd jobs and come back.");
		close;
	}

	inventoryplace PrsmHelmet, 1;
	delitem PinkAntenna, 100;
	delitem MinerHat, 1;
	delitem GrassSeeds, 5;
	delitem CobaltHerb, 60;
	//delitem TerraniteOre, 20;
	getexp 60000, 0;
	getitem PrsmHelmet, 1;
	setq SQuest_Paxel, 2;
	mesn;
	mesq l("There you go, a special and rare @@!", getitemlink(PrsmHelmet));
	close;

// ----------------------------------------------------
// Failure: Insufficient material
// ----------------------------------------------------
L_More_Needed1:
	mesn;
	mesq l("Your @@ is not enough, please look for more.", getitemlink(TerraniteOre));
	close;

L_More_Needed2:
	mesn;
	mesq l("I am sorry but I can see that you don't have all that I asked you.");
	next;
	mesn;
	mesq l("Please bring me that or I won't be able to make something really special for you.");
	close;


// ----------------------------------------------------
// Quest complete
// ----------------------------------------------------
L_Done:
	mesn;
	mesq l("Hello winged friend! How is the wind today?");

	menu
		rif(season() == SPRING || $@GM_OVERRIDE, l("It's spring, when the wind is always good.")), L_Spring,
		l("Calm, perfect for a fly!"), L_Fly,
		l("Gale, not good to fly."), L_Wind2;
	close;

L_Fly:
	mesn;
	mesq l("Haha yes, you are right!");
	close;

L_Wind2:
	mesn;
	mesq l("Yep, I never fly in these days.");
	close;

// TODO: Maybe we can add something else here
L_Spring:
	mesn;
	mesq l("Hahah, good to hear! Do you know some items are only dropped on spring? You should go after them!");
	close;

// Saulc/Omatt/Prsm minigame
OnTouch:
    if (BaseLevel < 20)
    {
        percentheal -100, -100; // Shouldn't be here
        end;
    }
    addtimer(100, "Paxel::OnOmattizator");
    end;

OnOmattizator:
    if (!isin("017-1", 131, 22, 0))
        end;
    if (!issit())
    {
        addtimer(100, "Paxel::OnOmattizator");
        end;
    }
    npctalk3 l("Unsit me at once!");
    emotion E_FURIOUS;
    addtimer(3000, "Paxel::OnOmattizator2");
    end;

OnOmattizator2:
    if (!isin("017-1", 131, 22, 0))
        end;
    if (!issit())
    {
        addtimer(100, "Paxel::OnOmattizator");
        end;
    }
    npctalk3 l("Unsit me OR I'LL KILL YOU in name of my father, Prsm!");
    emotion E_FURIOUS;
    addtimer(3000, "Paxel::OnOmattizator3");
    end;

OnOmattizator3:
    if (!isin("017-1", 131, 22, 0))
        end;
    if (!issit())
    {
        addtimer(100, "Paxel::OnOmattizator");
        end;
    }
    npctalk3 l("I HAVE WARNED YOU!!!!!");
    emotion E_FURIOUS;
    addtimer(3000, "Paxel::OnOmattizator4");
    end;

OnOmattizator4:
    if (!isin("017-1", 131, 22, 0))
        end;
    if (!issit())
    {
        addtimer(100, "Paxel::OnOmattizator");
        end;
    }
    .@q=getq(General_EasterEggs);
    npctalk3 l("DIE!");

    if (!(.@q & EE_PRSM)) {
        setq General_EasterEggs, .@q|EE_PRSM;
        dispbottom l("For finding an Easter Egg, you got Strange Coins!");
        getitem StrangeCoin, 3;
    }

    slide 130, 23;
    emotion E_FURIOUS;
    percentheal -100, -100;
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, PrsmHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, WarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 24);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 14);

    .sex=G_MALE;
    .distance=5;
    end;

}
