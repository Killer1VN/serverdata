// TMW2/LoF scripts.
// Authors:
//    Jesusalva
// Description:
//    LoF Townhall Access Control

017-1,180,20,0	script	#LoF_Townhall01	NPC_NO_SPRITE,0,0,{
    end;

OnTouch:
    // Sponsor and Main Quest 17+ - access granted
    .@q=getq(General_Narrator);
    if (.@q >= 17 || getgmlevel()) {
        warp "017-10", 56, 78;
    }
    end;

}

017-1,182,20,0	script	#LoF_Townhall02	NPC_NO_SPRITE,0,0,{
    end;

OnTouch:
    // Sponsor and Main Quest 17+ - access granted
    .@q=getq(General_Narrator);
    if (.@q >= 17 || getgmlevel()) {
        warp "017-10", 60, 78;
    }
    end;
}

017-1,171,20,0	script	#LoF_Townhall03	NPC_NO_SPRITE,0,0,{
    end;

OnTouch:
    // Sponsor and Main Quest 17+ - access granted
    .@q=getq(General_Narrator);
    if (.@q >= 17 || getgmlevel()) {
        warp "017-10", 36, 78;
    }
    end;
}

017-1,190,20,0	script	#LoF_Townhall04	NPC_NO_SPRITE,0,0,{
    end;

OnTouch:
    // Sponsor and Main Quest 17+ - access granted
    .@q=getq(General_Narrator);
    if (.@q >= 17 || getgmlevel()) {
        warp "017-10", 80, 78;
    }
    end;
}
/////////////////////////////
// Main Quest 6
// LoF Arc
/////////////////
// Check instances and rebuild if needed
// Returns map name
// BarbaraInstCheck( {submap=mines} )
// 0- Chamber ; 1 - Mines ; 2 - Hall ; 3 - Shrine
function	script	BarbaraInstCheck	{
	.@house=getarg(0, true);

	.@q2=getq2(LoFQuest_Barbara);
	// Map name limit: 4 chars (sgt1)
	.@map0$="brb0@"+getcharid(0);
	.@map1$="brb1@"+getcharid(0);
	.@map2$="brb2@"+getcharid(0);
	.@map3$="brb3@"+getcharid(0);
	if (!(isinstance(.@q2) && .@q2 != 0)) {
		.@inst = instance_create("Forgotten Shrine "+getcharid(0), getcharid(3), IOT_CHAR);
	    instance_attachmap("018-6-0", .@inst, false, .@map0$);
	    instance_attachmap("018-6-1", .@inst, false, .@map1$);
	    instance_attachmap("018-6-2", .@inst, false, .@map2$);
	    instance_attachmap("018-6-3", .@inst, false, .@map3$);
	    // Instance lasts two hours
		instance_set_timeout(7200, 7200, .@inst);
		instance_init(.@inst);
	    setq2 LoFQuest_Barbara, .@inst;
	}

    // It broke
    if (getmapinfo(MAPINFO_SIZE_X, .@map1$) <= 0) {
        setq2 LoFQuest_Barbara, 0;
        // Infinite Loop?
        return callfunc("BarbaraInstCheck", .@house);
    }

    // Return map name
    return getd(".@map"+.@house+"$");
}

017-1,32,44,0	script	#LoF_EleniumMines	NPC_NO_SPRITE,0,0,{
    end;

OnTouch:
    .@q=getq(LoFQuest_Barbara);
    if (.@q >= 1) {
        addmapmask BarbaraInstCheck(1), 2;
        warp BarbaraInstCheck(1), 80, 199;
    } else {
        warp "018-6-1", 80, 199;
    }
    end;
}

