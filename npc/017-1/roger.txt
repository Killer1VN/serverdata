// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Part of George quest.
//    PS. (96,120) save these coordinates

017-1,68,87,0	script	Roger	NPC_JOURNALMAN,{
    .@q=getq(LoFQuest_George);
    mesn;
    mesq l("Paper! Paper! Get your copy of The Land of Fire Gazette here!");
    next;
    mesn;
    mesq l("Hello @@, may I interest you in a copy of the Gazette? It's only 1 GP!", strcharinfo(0));
    next;
    menu
        l("Yes, please."), L_GetRead,
        l("No, thank you, kid."), L_TooBad,
        l("Do you have anything else to read?"), L_ExtraRead;

L_GetRead:
    mesn;
    mesq l("Let me see.....");
    next;
    if (.@q == 5)
        goto L_LOF_NEWS;
    mesq l("Oh yes, here you go, that will be 1 GP.");
    if (Zeny < 1)
        goto L_Poor;
    Zeny = (Zeny-1);
    mesc l("You are handed the Land of Fire Gazette. You look at the headline...");
    if (!getskilllv(TMW2_ANCIENTLANGUAGES)) {
        mesc l("...but it is written in Mananese, a language you know nothing about.");
    } else {
        mesc l("...but it is saying some stuff about a merge which happened literally eons ago.");
    }
    next;
    mesc l("You toss the paper in the trash.");
    close;

L_TooBad:
    mesn;
    mesq l("Too bad, There is some really good news this week. Things like how to go to Candor for free.");
    mesq l("And a special coupon section to get rare items for little or no money!");
    next;
    menu
        l("I changed my mind, I think I would like a copy!"), L_GetRead,
        l("That's ok, kid... I can't read, anyway."), L_TooBadTwo;

L_TooBadTwo:
    mesn;
    mesq l("Too bad, you're really missing out!");
    close;

L_ExtraRead:
    mesn;
    mesq l("Now that you ask....");
    next;
    if (.@q == 5)
        goto L_MadMag;
    mesn;
    mesq l("I might be able to get you a rare copy of the Maritime Almanac Scroll. A yearly publication for seafaring men.");
    mesq l("It is very hard to come by, however, my father has a copy he might sell to you for the right price.");
    menu
        l("I will trade you a Dagger and Sailor's Hat for it."), L_BadOffer1,
        l("I will pay you 5,000 GP in pure gold for it!"), L_BadOffer2,
        l("I can give you a rare...(cough, cough).. Mouboo Figurine for it."), L_Success,
        l("I have no interest in things such as that."), -;
    close;

L_Success:
    .@q2=getq2(LoFQuest_George);
    if (.@q2 == 1)
        goto L_HaveScroll;
    if (countitem(MoubooFigurine) < 1)
        goto L_NoMouboo;
    if (countitem(SailorHat) == 0)
        goto L_StatNone;
    mesn;
    mesq l("This is great! My collection of rare figurines is now complete.");
    next;
    mesn;
    mesq l("I asked my father and he agreed to trade it to you, so here is the last copy of the Maritime Almanac Scroll.");
    /*
    //TODO: Add the ScrollOfPaper or the AlmanacScroll to finish this quest
    inventoryplace FireScroll, 1;
    delitem MoubooFigurine, 1;
    getitem FireScroll, 1;
    setq2 LoFQuest_George, 1;
    */
    close;

L_BadOffer1:
    mesn;
    mesq l("Wow, a Dagger and a Sailor's Hat!");
    next;
    mesn;
    mesq l("Although I really want the Sailor's Hat, but daggers are dangerous for kids to play with.");
    mesq l("Besides that, do I look like a warrior to you? Why would you give a kid a sharp dagger?");
    close;

L_BadOffer2:
    mesn;
    mesq l("5000 GP? That sure is a lot of gold...");
    mesq l("but I was hoping for something more valuable. No, thanks.");
    close;

L_NoMouboo:
    mesn;
    mesq l("You said you would trade a real Mouboo Figurine, but now I see you don't have one. Come back when you do.");
    close;

L_StatNone:
    mesq l("You might have the Mouboo Figurine, but I don't trust everyone.");
    if(.@q >= 3)
        goto L_ShowHat;
    mesq l("My friend George is much older than I am, go see him and if he trusts you, so will I.");
    if (.@q < 2)
        setq LoFQuest_George, 1;
    close;

L_ShowHat:
    mesn;
    mesq l("I heard you have met my good friend George, and you gave him some items that he needed...");
    next;
    mesn;
    mesq l("Didn't he give you anything?");
    next;
    mesn;
    mesq l("If you could show me what he gave you, I might trust you.");
    close;

L_MadMag:
    mesn;
    mesq l("I have my favorite book of all times, Moby Dick. Let me read some of it to you....");
    next;
    mesc l("Roger begins to read.");
    mesn;
    mesq l("Towards thee I roll, thou all-destroying but unconquering whale; to the last I grapple with thee; from hell’s heart I stab at thee; for hate’s sake I spit my last breath at thee. Sink all coffins and all hearses to one common pool! And since neither can be mine, let me then tow to pieces, while still chasing thee, though tied to thee, thou damned whale! Thus, I give up the spear!"); // SORRY TRANSLATORS
    next;
    mesn;
    mesq l("I don't really understand it either, but it's not for sale. Maybe you could come back later so I can read more of it to you.");
    next;
    mesn;
    mesq l("Have a nice day!");
    close;

L_Poor:
    mesn;
    mesq l("You don't seem to have enough gold, not even 1 GP...you should really let go of some useless stuff on your inventory.");
    close;

L_LOF_NEWS:
    mesn;
    mesc l("Well I see that you have completed the George Quest!");
    mesc l("I am actually surprised you clicked me again. Well then, here's the Land of Fire News...");
    next;
    mesc l("As you may know, LOF ran on a modified version of The Mana World game, before being merged on TMW2: Moubootaur Legends.");
    mesc l("However, LoF admins and developers are working on a new game to offer a new experience for players.");
    next;
    mesc l("The project is involving only a small group of members, but would welcome anyone who knows how to assist in development.");
    mesc l("You can always come to #devel on our Discord, or drop by #landoffire on IRC.");
    next;
    mesc l("You can also visit our website. And if you want to assist on this server (TMW2: ML), you're welcome too!");
    mesc l("Oh, and about the Candor travel for 200 GP... Which would be a rare for little or no gold... Well, that's just an attention grabber.");
    next;
    mesc l("BUT you can reduce the travel price to everywhere, to as low as 250 GP, by completing QUESTS!");
    mesc l("And some from the Grand Hunter Quests give you @@, which can be traded with the travelers for rares! Good luck!", getitemlink(StrangeCoin));
    close;

L_HaveScroll:
    mesn;
    mesq l("Looks like I already gave you the Maritime Almanac Scroll. You need to go see George again. I am unable to get another copy for you.");
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

