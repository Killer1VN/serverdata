// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    023-3-1 The First Monster King's Throne Room Configuration File

023-3-1	mapflag	zone	MMO

023-3-1,45,27,0	script	#Finish02331	NPC_HIDDEN,0,0,{
    end;

OnTouch:
    .@m$=getmap();
    .@n=getq(General_Narrator);
    .@q=getq(FrostiaQuest_Homunculus);
    // Cheater Detected
    if (.@n < 15) { 
        warp "Save", 0, 0;
        percentheal -100, -100;
        end;
    }
    if (.@q < 3) {
        dispbottom l("The magic power outflowing in the room prevents you from leaving.");
        end;
    }
    if (mobcount(.@m$, "#Core02331::OnMobDie")) {
        dispbottom l("These assassins will catch me if I do that now!");
        end;
    }

	.@mapn$="023-3-2";
    warp .@mapn$, any(39,40), 58;
    end;
}

023-3-1,45,80,0	script	#Exit02331	NPC_HIDDEN,1,0,{
    end;
OnTouch:
    .@n=getq(General_Narrator);
    .@q=getq3(FrostiaQuest_Homunculus);
    // Cheater Detected
    if (.@n < 15) { 
        warp "Save", 0, 0;
        percentheal -100, -100;
        end;
    }
    if (.@n == 15) {
        dispbottom lg("I'm not a coward! I must press forward!");
        end;
    }
	.@mapn$="001-7";
    warp .@mapn$, 59, 45;
    end;
}
// To the traps!

023-3-1,45,63,0	script	#Ambush02331	NPC_HIDDEN,10,0,{
OnTouch:
    .@q=getq(FrostiaQuest_Homunculus);
    if (.@q < 2) {
        dispbottom l("Error, cheater detected");
        percentheal -100, -100;
        end;
    }
    .@q=getq3(FrostiaQuest_Homunculus);
    if (!.@q) {
        .@m$=getmap();
        .@mobID1=monster(.@m$, 43, 55, l("Assassin"), HoodedNinja, 1, "#Core02331::OnMobDie");
        .@mobID2=monster(.@m$, 48, 55, l("Assassin"), HoodedNinja, 1, "#Core02331::OnMobDie");
        monster(.@m$, 43, 60, l("Assassin"), Assassin, 1, "#Core02331::OnMobDie");
        monster(.@m$, 43, 65, l("Assassin"), Assassin, 1, "#Core02331::OnMobDie");
        monster(.@m$, 48, 60, l("Assassin"), Assassin, 1, "#Core02331::OnMobDie");
        monster(.@m$, 48, 65, l("Assassin"), Assassin, 1, "#Core02331::OnMobDie");

        if (any(true,false))
            unittalk(.@mobID1, l("Kill 'em!"));
        else
            unittalk(.@mobID2, l("Kill 'em!"));

        setq3 FrostiaQuest_Homunculus, 1;
    }
    end;
}

// Main event core
023-3-1,45,52,0	script	#Core02331	NPC_HIDDEN,10,0,{
OnTouch:
    if (instance_id() < 0)
        end;
    .@q=getq(FrostiaQuest_Homunculus);
    if (.@q < 2) {
        dispbottom l("Error, cheater detected");
        percentheal -100, -100;
        end;
    }
    .@n$=instance_npcname(.name$);
    .@q=getq3(FrostiaQuest_Homunculus);
    if (.@q == 1) {
        // Begin the event core
        setq3 FrostiaQuest_Homunculus, 2;
        addtimer(1100, .@n$+"::OnEvent01");
		attachnpctimer();
        initnpctimer();
    }
    end;
OnEvent01:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    slide 45, 42;
    @ISBAMUTH=monster(.@m$, 45, 34, l("Isbamuth"), Isbamuth, 1, .name$+"::OnIsbamuthDefeat");
    .@g1=monster(.@m$, 42, 37, l("Assassin"), HoodedNinja, 1, .name$+"::OnMobDie");
    .@g2=monster(.@m$, 48, 37, l("Assassin"), HoodedNinja, 1, .name$+"::OnMobDie");
    .@g3=monster(.@m$, 39, 40, l("Assassin"), Assassin, 1, .name$+"::OnMobDie");
    .@g4=monster(.@m$, 51, 40, l("Assassin"), Assassin, 1, .name$+"::OnMobDie");

    // Block everyone for cutscene (includes invencibility boost)
    setpcblock(255, true);
    sc_start(SC_STUN, 7500, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, @ISBAMUTH);
    sc_start(SC_STUN, 7500, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .@g1);
    sc_start(SC_STUN, 7500, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .@g2);
    sc_start(SC_STUN, 7500, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .@g3);
    sc_start(SC_STUN, 7500, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, .@g4);

    unittalk(@ISBAMUTH, l("Seems like the rat have come after the cheese."));
    addtimer(1500, .@n$+"::OnE02");
    end;

OnE02:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("HAHAHAHA! How foolish of you, didn't even bother trying to sneak in!"));

    addtimer(1500, .@n$+"::OnE03");
    end;

OnE03:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("Remember my name: I am Isbamuth, and I've took the Throne which rightfully belongs to me."));

    addtimer(1500, .@n$+"::OnE04");
    end;

OnE04:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("This throne is not from the Monster King... IT BELONGS ONLY TO ME!"));

    addtimer(1500, .@n$+"::OnE05");
    end;

OnE05:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("Now, as you gave yourself the trouble of coming here..."));

    addtimer(1500, .@n$+"::OnE06");
    end;

OnE06:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    setpcblock(255, false);
    unittalk(@ISBAMUTH, l("LET'S DANCE!"));
    mapannounce(.@m$, "##2"+l("Victory Conditions: Survive!"), 0);
    mapannounce(.@m$, "##1"+l("Defeat Conditions: Your death!"), 0);

    addtimer(15000, .@n$+"::OnW01");
    addtimer(60000, .@n$+"::OnW02");
    addtimer(180000, .@n$+"::OnE07");
    end;

OnE07:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    @SCOUT=monster(.@m$, 43, 29, l("Assassin"), HoodedNinja, 1, .name$+"::OnMobDie");
    sc_start(SC_STUN, 12000, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK, @SCOUT);

    unitemote(@SCOUT, E_KITTY);
    unitstop(@ISBAMUTH);

    deltimer(.@n$+"::OnW01");
    deltimer(.@n$+"::OnW02");
    addtimer(1500, .@n$+"::OnE08");
    end;

OnE08:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("What's it, scout."));
    unitwalk(@ISBAMUTH, 45, 34);

    addtimer(1500, .@n$+"::OnE09");
    end;

OnE09:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unitemote(@SCOUT, E_THUMBUP);
    unittalk(@ISBAMUTH, l("So, it is ready?"));

    addtimer(1500, .@n$+"::OnE10");
    end;

OnE10:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("I hate to leave you now, @@, but I have more important things to do.", strcharinfo(0)));

    addtimer(1500, .@n$+"::OnE11");
    end;

OnE11:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("Enjoy this world while it lasts. Heh. It's time to... detonate."));

    addtimer(1500, .@n$+"::OnE12");
    end;

OnE12:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    // A warp to non-instanced version to prevent death sprite from showing up.
    unitwarp(@ISBAMUTH, "023-3-1", 45, 45);
    unitwarp(@SCOUT, "023-3-1", 45, 45);
    .@isb=@ISBAMUTH;
    @ISBAMUTH=0;
    unitkill(.@isb);
    unitkill(@SCOUT);
    setq1 FrostiaQuest_Homunculus, 3;
    mapannounce(.@m$, "##2"+l("Victory Conditions: Defeat all enemies!"), 0);
    mapannounce(.@m$, "##1"+l("Defeat Conditions: Your death!"), 0);
    end;

// War events
OnW01:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("Be cursed, you fool!"));
    sc_start(SC_CURSE, 3000, 1, 10000, SCFLAG_NOAVOID|SCFLAG_FIXEDTICK);

    addtimer(45000, .@n$+"::OnW01");
    end;

OnW02:
    .@m$=getmap();
    .@n$=instance_npcname(.name$);

    unittalk(@ISBAMUTH, l("Come to my aid! Vanish this fool!"));
    monster(.@m$, 42, 37, l("Assassin"), any(Assassin,Bandit,RobinBandit), 1, .name$+"::OnMobDie");
    monster(.@m$, 48, 37, l("Assassin"), HoodedNinja, 1, .name$+"::OnMobDie");
    monster(.@m$, 39, 40, l("Assassin"), any(Assassin,Bandit,RobinBandit), 1, .name$+"::OnMobDie");
    monster(.@m$, 51, 40, l("Assassin"), Assassin, 1, .name$+"::OnMobDie");

    addtimer(60000, .@n$+"::OnW02");
    end;

// Secret events. Do not handle Isbamuth death as it should be impossible...
OnIsbamuthDefeat:
    if (!@ISBAMUTH)
        end;
    Exception(l("Why do you bully me! - This is a bug: 02331.LOGIC.OID"), RB_DISPBOTTOM|RB_DEBUGMES);
    deltimer(.@n$+"::OnW01");
    deltimer(.@n$+"::OnW02");
    getexp 0, 1000;
    @ISBAMUTH=monster(.@m$, 45, 34, l("Isbamuth"), Isbamuth, 1, .name$+"::OnIsbamuthDefeat");
    end;

// For mobcount() only
OnMobDie:
    end;

OnTimer1000:
    .@m$=instance_mapname("023-3-1");
    if (getmapusers(.@m$))
        initnpctimer;
OnTimerQuit:
    // Cleanup - you fail.
    killmonsterall(.@m$);
	stopnpctimer();
	detachnpctimer();
    end;
}



