// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Manages warps at Aeros

001-1,235,25,0	script	Worlds Gate	NPC_HIDDEN,1,0,{

OnTouch:
    mesn;
    mes l("This Portal can send your soul back to the world, along any items, money and/or experience gained.");
    mes l("Would you like to leave this place?");
    menu
        l("No."), L_Close,
        rif(is_admin() && !$@GM_EVENT, l("Enable Event")), L_Enable,
        rif(is_admin() && $@GM_EVENT, l("Disable Event")), L_Disable,
        l("Yes."), L_Leave;

L_Leave:
    warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    goto L_Close;

L_Enable:
    $@GM_EVENT=1;
    close;

L_Disable:
    $@GM_EVENT=0;
    close;

// Uses l() to translate utilities
L_TranslationFix:
    mes l("Magic Maggot");
    mes l("Monster");
    mes l("Monster King");
    mes l("Monster General");
    mes l("Monster Admiral");
    mes l("Monster Major");
    mes l("Monster Captain");
    mes l("Monster Lieutenant");
    mes l("Monster Sergeant");
    mes l("Monster Soldier");
    mes l("Random Bif");
    // Configs
	mes l("Human");
	mes l("Ukar");
	mes l("Redy");
	mes l("Elf");
	mes l("Orc");
	mes l("Raijin");
	mes l("Tritan");
    // Messages
    mes l("All monsters summoned!");
    mes l("Mercy has been granted.");
    mes l("Judgement has passed.");
    mes l("Warping to save point.");
    mes l("Your save point has been changed.");
    mes l("Warped.");
    mes l("Item created.");
    mes l("Welcome to TMW-2: Moubootaur Legends! We hope you have a great time in our server!");
    mes l("You have been jailed by a GM.");
    mes l("A GM has discharged you from jail.");
    mes l("This item cannot be dropped.");
    mes l("This item cannot be sold.");
    mes l("This item cannot be auctioned.");
    mes l("This item cannot be traded.");
    mes l("This item cannot be stored.");
    mes l("I can't handle it anymore! NO MORE!");
    mes l("Come, my minions! Lay siege to towns! LEAVE NO OPPOSITION TO ME!");
    mes l("Burn, destroy, do whatever you need, until your last breath, my lieutenants and colonels!");
    mes l("Summoned Monster");
    mes l("Scout");

    mes l("sample");
    close;

L_Close:
    closedialog;
    close;
}

// This is a copy for west Aeros
001-1,23,107,0	script	World Gate 2	NPC_HIDDEN,1,0,{
OnTouch:
    mesn;
    mes l("This Portal can send your soul back to the world, along any items, money and/or experience gained.");
    mes l("Would you like to leave this place?");
    select
        l("No."),
        l("Yes.");

    if (@menu == 2)
        warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    closedialog;
    close;
}
