// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Trades Strange Coins for useful items

001-1,243,26,0	script	Aeros Trader	NPC_M_COINKEEPER,{
    mesn;
    mesq l("Oh, hello there! Welcome to the Mana Plain Of Existence!");
    next;
    mesn;
    mesq l("In this wonderful realm, you can find and earn many @@, our currency!", getitemlink(StrangeCoin));
    next;
    mesn;
    mesq l("You can then trade these coins for items with me!");
    next;
    openshop;
    closedialog;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, TopHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HEADBOTTOM, AssassinBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 25);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex = G_MALE;
    .distance = 5;

	tradertype(NST_CUSTOM);

	// Rare Equipment selection.
    // *these* are really rare!
    // Maximum 30,000
	sellitem DarkPulsar,6000;
	sellitem ThunderStaff,5000;
	sellitem PiouEgg,3915;
	sellitem DeliciousCookie,3240;
    sellitem TopHat, 1620;
    sellitem BowlerHat, 720;
	sellitem AshUrn,630;
	sellitem Googles,540;
	sellitem Barrel,400;

    // Temporary, Seasonal, for events, rare drops, next release
    sellitem MercBoxEE,2000;
    sellitem MercBoxDD,1450;
    sellitem MercBoxCC,1000;
    sellitem MercBoxBB,700;
    sellitem MercBoxC,600;
    sellitem ArcmageBoxset,500;
    sellitem MercBoxB,400;
    sellitem LeatherShirt, 315;
    sellitem JeansShorts, 270;
    sellitem MercBoxA,200;
    sellitem ThornAmmoBox,180;
    sellitem CursedAmmoBox,80;
    sellitem ArrowAmmoBox,50;

    // Temporary, but later may sell rare dyes (eg. purple and... golden?)
    sellitem RedDye, 45;
    sellitem GreenDye, 45;
    sellitem BlueDye, 36;

    // Consumables
	sellitem MagicApple,115;
	sellitem SacredLifePotion,60;
	sellitem SacredManaPotion,60;
	sellitem ElixirOfLife,32;
	sellitem Grenade,28;
	sellitem WhiskeyAle,28;
	sellitem CelestiaTea,17;
    sellitem BottleOfDivineWater, 15;
	sellitem HastePotion,11;
	sellitem StrengthPotion,11;
	sellitem PrecisionPotion,9;
	sellitem DodgePotion,9;
	sellitem Curshroom,6;
	sellitem PetcaringGuide,5; // I needed to add this somewhere
	sellitem SmokeGrenade,3;
	end;

/* set currency to be item 828 */
OnCountFunds:
	setcurrency(countitem(StrangeCoin));
	end;

/* @price is total cost. @points is if we accept two items as currency. */
OnPayFunds:
	//dispbottom "Hi: price="+@price+" and points="+@points;
	if( countitem(StrangeCoin) < @price )
		end;
	delitem StrangeCoin,@price;
	purchaseok();
	end;

}
