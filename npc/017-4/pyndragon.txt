// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Forge firearms
017-4,34,23,0	script	Pyndragon	NPC_PLAYER,{
    // craft_gun( BaseItem1, Amount, BaseItem2, Amount, BaseItem3, Amount, PrizeItem, Price )
    function craft_gun {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@base3=getarg(4);
        .@amon3=getarg(5);
        .@prize=getarg(6);
        .@price=getarg(7);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ @@", countitem(.@base3), .@amon3, getitemlink(.@base3));
        mesc l("@@/@@ GP", format_number(Zeny), .@price);
        next;

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            countitem(.@base3) >= .@amon3 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
            delitem .@base3, .@amon3;
            Zeny = Zeny - .@price;
            getitem .@prize, 1;

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;
    }

    mesn;
    if (is_night())
        mesq l("Good @@. My name is @@ and I make @@.", l("evening"), .name$, l("firearms"));
    else
        mesq l("Good @@. My name is @@ and I make @@.", l("day"), .name$, l("firearms"));
    next;
    mesn strcharinfo(0);
    mesq l("Firearms? What would that be? @@", "%%4");
    next;
    mesn;
    mesq l("Oh, just an empty staff with black powder inside it. Which explodes. And then kills monsters.");
    next;
    mesn;
    next;
   if (BaseLevel < 50)
        goto L_TooWeak;
    else if (BaseLevel < 60)
        goto L_Weak;
    goto L_Menu;

L_TooWeak:
    mesn;
    mesq l("These weapons are only for masters, so you must get levels before being able to use them.");
    close;

L_Weak:
    mesn;
    mesq l("You need level 60 to use these guns, but if you want to start collecting materials, you're allowed to.");
    next;
    goto L_Menu;

L_Menu:
    menu
        l("I would like some information"), L_Info,
        l("I want a gun!"), L_Craft,
        l("I don't want anything right now, bye."), L_Close;

L_Info:
    mes "";
    mesn;
    mesq l("There are four basic class:");
    mesc l("SHORT GUNSTAFF");//aka revolver
    mesc l("* The only one hand ranged weapon you'll ever find!");
    mes l("  I cut it in half, and to fix shooting speed, I added some extra cogs.");
    mes l("  It didn't got too much lighter, and it got really weak compared to a bow.");
    mesc l("POWERFUL GUNSTAFF");
    mesc l("* Huge damage and more criticals, but slow fire rate.");
    mes l("  The standard gunstaff! It is way stronger than a bow!");
    mes l("  Oh, and it comes with added shooting range, but it is slow as hell to reload.");
    mesc l("RAPID GUNSTAFF");
    mesc l("* Low damage, highest attack speed from all.");
    mes l("  One have lots of powder to attack non-stop, but oh god that is heavy.");
    mes l("  It is also much weaker and less precise than a bow. But it is so fun using...");
    mesc l("EXPLOSIVE GUNSTAFF");
    mesc l("* Causes splash damage, and are very expensive.");
    mes l("  You know, I wanted to make the powder explode when hitting!");
    mes l("  It is the same as a bow, including in penalty. But it deals splash damage!");
    next;
    mesn;
    mesq l("Select carefully which weapon you want, so there are no regrets.");
    next;
    goto L_Menu;

L_Craft:
    select
        l("I changed my mind."),
        l("I want a SHORT GUNSTAFF."),
        l("I want a POWERFUL GUNSTAFF."),
        l("I want a RAPID GUNSTAFF."),
        l("I want an EXPLOSIVE GUNSTAFF.");
    mes "";

    switch (@menu) {
        case 2:
            craft_gun( LeadIngot, 4, TitaniumIngot, 5, Coal, 11, PynRevolver, 3000 );
            goto L_Craft;
        case 3:
            craft_gun( LeadIngot, 6, TitaniumIngot, 7, Coal, 14, PynRifle, 3000 );
            goto L_Craft;
        case 4:
            craft_gun( LeadIngot, 6, TitaniumIngot, 7, Coal, 14, PynGatling, 4000 );
            goto L_Craft;
        case 5:
            craft_gun( LeadIngot, 9, TitaniumIngot, 10, Coal, 16, PynShotgun, 6000 );
            goto L_Craft;
    }
    goto L_Menu;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, BromenalPants);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;

}
