// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_ZARKOR
//
// Spawns a Cave Maggot.
// This is to test stuff, more than to create anything really.


-	script	sk#zarkor	32767,{
    end;

/*
OnFriendlyDeath:
    emote 4;
    end;
*/

OnCall:
    // Other requeriments
    if (countitem(ZarkorScroll) < 1) {
        dispbottom l("You need @@ to cast this skill.", getitemlink(ZarkorScroll));
        end;
    }

    // Check cooldown
    // This will vanish upon logout =/
    if (@zark_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@zark_at));
        end;
    }

    // Setup
    @sk=TMW2_ZARKOR;
    @mp=40;
    @amp=7;

    // Check if you have mana to cast
    // MagicCheck(SkillID, Mana{, MP per level})
    if (!MagicCheck(@sk, @mp, @amp))
        end;

    // Summon Magic
    SummonMagic(@sk, CaveMaggot, 2);

    // Get a single mana experience point (this is NOT used by Mana Stone)
    GetManaExp(@sk, 1);

    // set cooldown
    @zark_at=gettimetick(2);
    @zark_at=@zark_at+80;
    end;

OnInit:
    bindatcmd "sk-zarkor", "sk#zarkor::OnCall", 0, 100, 0;
    end;
}
