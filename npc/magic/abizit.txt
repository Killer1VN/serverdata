// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: -
//
// Abizit() reports your magic power

-	script	sk#abizit	32767,{
    end;

OnCall:
    // Must have magic
    if (!MAGIC_LVL)
        end;

    // Check cooldown
    if (@abizit_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@abizit_at));
        end;
    }

    ShowAbizit(false);

    // set cooldown
    @abizit_at=gettimetick(2);
    @abizit_at=@abizit_at+4;
    end;

OnInit:
    bindatcmd "sk-abizit", "sk#abizit::OnCall", 0, 100, 0;
    end;
}
