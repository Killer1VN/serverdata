// TMW2 Script
// Author:
//    Jesusalva
// Description:
//  Begs for food. Lots of it. And in the end, an armor piece awaits you.
//  Enliven Reva Foxhound is an anagram of Lunovox Heavenfinder

009-1,89,57,0	script	Enliven Reva Foxhound	NPC_IVAN,{
    .@q=getq(HalinarzoQuest_Foxhound);
    if (BaseLevel < .@q+30) {
        mesn;
        mesq l("Ah, living in Halinarzo is so difficult...");
        next;
        mesn;
        mesq l("If there were strong people to give us food, ah, how that would be good...");
        next;
        mesn;
        mesq l("Sorry for making you listen my rambles. I need someone strong, you see...");
        close;
    }
    mesn;
    mesq l("Please traveler, help my starving family!");
    next;
    mesn;
    mesq l("We from Halinarzo are very poor, and in constant need of food. You have to help us!");
    mes "";
    menu
        l("I'll help you."), -,
        l("Sorry, I am so hungry as you."), L_Close;

    switch (.@q) {

    case 0:
        mesn;
        mesq l("There's a long time we don't eat anything. Bring us @@! Five should do it.", getitemlink(Bread));
        select
            rif(countitem(Bread) >= 5, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem Bread, 5;
            getexp 20, 0;
            Zeny=Zeny+25*5;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, 1);
            next;
        }
        break;


    case 1:
        mesn;
        mesq l("Bread is fine, but it would taste better with @@. Could you bring us three?", getitemlink(Cheese));
        select
            rif(countitem(Cheese) >= 3, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem Cheese, 3;
            getexp 25, 0;
            Zeny=Zeny+25*3;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }
        break;

    case 2:
        mesn;
        mesq l("Do you know @@? It's a poisonous food you can't find around here. It can be made edible with special prepare.", getitemlink(PinkBlobime));
        select
            rif(countitem(PinkBlobime) >= 1, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem PinkBlobime, 1;
            getexp 30, 0;
            Zeny=Zeny+10*1;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }
        break;

    case 3:
        mesn;
        mesq l("I saw Purple the other day... Ah, now I want to eat @@... A dozen should do...", getitemlink(Plushroom));
        select
            rif(countitem(Plushroom) >= 12, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem Plushroom, 12;
            getexp 35, 0;
            Zeny=Zeny+15*12;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }
        break;

    case 4:
        mesn;
        mesq l("There exist more food than you know. But this time I don't want anything extravagant, just 6 @@ should be fine.", getitemlink(CaveSnakeEgg));
        select
            rif(countitem(CaveSnakeEgg) >= 6, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem CaveSnakeEgg, 6;
            getexp 40, 0;
            Zeny=Zeny+27*6;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }
        break;

    case 5:
        mesn;
        mes l("Thanks for feeding us earlier. I wanted to give a great dinner to my family, even if just once.");
        mes l("Could you please give us a full meal, with 5 @@, 5 @@, 5@@, 5 @@ and 5 @@?", getitemlink(SnakeEgg), getitemlink(Bread), getitemlink(Cheese), getitemlink(Aquada), getitemlink(Manana));
        select
            rif(countitem(SnakeEgg) >= 5 && countitem(Bread) >= 5 && countitem(Cheese) >= 5 && countitem(Aquada) >= 5 && countitem(Manana) >= 5, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            mes "";
            mesn;
            mesq l("Thanks for attending even this selfish request of mine.");
            next;
            delitem SnakeEgg, 5;
            delitem Bread, 5;
            delitem Cheese, 5;
            delitem Aquada, 5;
            delitem Manana, 5;
            getexp 11500, 15;
            getitem Bucket, 1;
            mesn;
            mesq l("This is just a bucket, but I did some cuts on it so you can see though it. It protects your whole face.");
            next;
            mesn;
            mesq l("This is the least I could do for you. Many thanks. And if you have more food, you can keep feeding my family.");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            setq2(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }
        break;

    default:
        .@q=getq2(HalinarzoQuest_Foxhound);
        // Manana (Death Penalty) or Aquadas (Hard to get item)?
        mesn;
        mesq l("I only need @@ @@. Please! You have to help me!", .@q, getitemlink(Manana));
        select
            rif(countitem(Manana) >= .@q, l("I have it right here. You can take it!")),
            l("I'm sorry... I can't help you either...");
        if (@menu == 1) {
            delitem Manana, .@q;
            Zeny=Zeny+(60*.@q);
            getexp 0, .@q;
            mes "";
            mesn;
            mesq lg("Thank you! Thank you! Savior!");
            setq(HalinarzoQuest_Foxhound, .@q+1);
            next;
        }

    }

    goto L_Close;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    //.@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    //setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    //setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    //setunitdata(.@npcId, UDT_HAIRSTYLE, 6);
    //setunitdata(.@npcId, UDT_HAIRCOLOR, 3);

    .sex = G_MALE;
    .distance = 5;
    end;



}
