// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 023-1: Frozen Cave warps
023-1,59,20,0	script	#023-1_59_20	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 36,46; end;
}
023-1,62,32,0	script	#023-1_62_32	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 50,56; end;
}
023-1,37,28,0	warp	#023-1_37_28	0,0,022-1,58,53
023-1,35,33,0	script	#023-1_35_33	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 31,61; end;
}
023-1,57,43,0	warp	#023-1_57_43	0,0,022-1,80,66
023-1,50,43,0	script	#023-1_50_43	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 50,49; end;
}
023-1,26,36,0	warp	#023-1_26_36	0,0,022-1,49,56
023-1,36,47,0	script	#023-1_36_47	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 59,21; end;
}
023-1,50,48,0	script	#023-1_50_48	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 50,42; end;
}
023-1,50,55,0	script	#023-1_50_55	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 62,31; end;
}
023-1,47,59,0	warp	#023-1_47_59	0,0,022-1,73,63
023-1,52,62,0	warp	#023-1_52_62	0,0,022-1,70,82
023-1,31,62,0	script	#023-1_31_62	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 35,34; end;
}
023-1,45,70,0	warp	#023-1_45_70	0,0,022-1,68,91
023-1,46,33,0	script	#023-1_46_33	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 39,21; end;
}
023-1,39,20,0	script	#023-1_39_20	NPC_HIDDEN,0,0,{
	end;
OnTouch:
	slide 46,32; end;
}
023-1,42,33,0	warp	#023-1_42_33	0,0,024-1,75,135
