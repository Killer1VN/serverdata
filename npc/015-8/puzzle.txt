// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    PUZZLES and TRAPS submodule - Sagratha's Cave - 015-8
// Uses setq3 - setq1 stage 3
// NPC_FAKIR + NPC_STATUE_FAFA

// Chasm at third seal
015-8,120,158,0	script	#SaggyPuzzleChasm	NPC_HIDDEN,0,4,{
    end;
OnTouch:
    do {
        if (@saggychasmok) {
            slide 125, 160;
            closedialog;
            end;
        }

        mesc l("There is a chasm.");
        select
            l("Drop a coin to hear how deep it is"),
            l("Climb on the walls!"),
            l("Run and try jumping it!"),
            l("Build a bridge with wooden logs!"),
            l("Uhh... I'm scared!");
        mes "";
        switch (@menu) {
        case 1:
            if (!Zeny) {
                mesc l("You don't have money.");
            } else {
                Zeny-=1;
                mesc l("You throw a coin on the chasm.");
                next;
                mesc l("... ... ...");
                next;
                mesc l("You don't hear anything. This chasm must be incredibly deep.");
                next;
                mesc l("Falling means certain death.");
            }
            break;
        // Let's climb the wall!
        case 2:
            if (readparam2(bStr) < 20 || (!countitem(MinerGloves) && !countitem(LeatherGloves))) {
                mesc l("You don't have proper equipment for it.");
            } else {
                mesc l("Taking your sturdy glove, you attempt to walk by the wall! You are betting your life on it!");
                next;
                mesc l("A stone seemed to be loose, but you barely managed to avoid falling!");
                next;
                mesc l("This is difficult, but you are giving your best!!");
                next;
                mesc l("You are close... You are almost there!");
                next;
                // We get a bit more than 50% - we take 60%, so your chances are
                // higher than jumping... But you need two stats, instead of one!
                // (Top will be 120 instead of 100 - thus, 720 out of 640)
                .@ponderate=(readparam2(bStr)+readparam2(bVit))/100*60;
                if (rand(0,1000) < 640-(.@ponderate*6)) {
                    mesc l("But unfortunately, a loose stone lodges and you fall to your death."), 1;
                    percentheal -100, -100;
                    close;
                }
                mesc l("You succeed in crossing the chasm!");
                @saggychasmok=true;
            }
            break;
        // Let's run it!
        case 3:
            if (readparam2(bAgi) < 20 || Weight > 1500) {
                mesc l("You don't have enough agility / is carrying too much to attempt it.");
            } else {
                mesc l("You'll give your best and attempt to run!");
                next;
                mesc l("You take distance and...");
                next;
                mesc l("YOU JUMP FOR ALL IT IS WORTH!!");
                // (You can always fall as top is 600 of 640)
                if (rand(0,1000) < 640-(readparam2(bAgi)*6)) {
                    mesc l("But unfortunately, it was harder than you thought. You are dead."), 1;
                    percentheal -100, -100;
                    close;
                }
                mesc l("You succeed in crossing the chasm!");
                @saggychasmok=true;
            }
            break;
        // Let's make a bridge!
        case 4:
            if (countitem(WoodenLog) < 5 || !getskilllv(TMW2_CRAFT)) {
                mesc l("You don't have enough wood (5 @@) or crafting skills to do it.", getitemlink(WoodenLog));
            } else {
                mesc l("...");
                next;
                mesc l("After hours of hard work...");
                next;
                mesc l("You succeed in crossing the chasm!");
                delitem WoodenLog, 5;
                @saggychasmok=true;
            }
            break;
        default:
            close;
        }
        next;
    } while (true);
    close;
}

015-8,124,158,0	script	#SaggyPuzzleChasmSkip	NPC_HIDDEN,0,4,{
    end;
OnTouch:
    slide 119, 160;
    end;
}


// Controls a chest/mimic on the trap room
015-8,0,0,0	script	#SaggyChestCtrl	NPC_HIDDEN,{
    end;

OnDelay:
    initnpctimer;
    end;

OnTimer180000:
    stopnpctimer;
OnInit:
    monster "015-8", 119+rand2(-3, 3), 35+rand2(-3, 3), "Treasure Chest", any(BronzeChest,BronzeMimic,SilverChest,SilverMimic), 1, "#SaggyChestCtrl::OnDelay";
    end;
}

