// TMW2 scripts.
// Author:
//    Saulc
// Description:
//    Candor Armor&Weapon shop keeper.
// Variables:
//    CandorQuest_Rosen
//      Suggestion: Deliver a letter to Zegas, giving player background about
//      Candor Island and Saxso. Requires level 5. Reward: 150 GP.
//      Could have an additional step related to Bifs. Even a daily quest asking
//      for (day % 8) ore, with suitable prices.
//
//  0 - Not assigned
//  1 - Quest Accepted
//  2 - Quest Complete
//  3 - Reward Taken
//  4 - Second Quest Accepted
//  5 - Second Quest Complete

005-4,29,36,0	script	Rosen	NPC_GUARD1,{
    function explain_ironingot {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("Did you see Jhedia the blacksmith in Tulimshar? She might know how you could get this."),
            l("Nevertheless, you probably need some base materials from Bifs. Who knows what it will drop if you are lucky?");

        return;
    }

    speech S_LAST_NEXT,
        l("Welcome to Tolchi and Rosen Shop."),
        l("What would you like today?");

    do
    {
        select
            menuaction(l("Trade")),
            l("How can I get iron ingot?"),
            l("I want to improve my equipment."),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                closedialog;
                shop "Shop#Candor";
                close;
            case 2:
                explain_ironingot;
                break;
            case 3:
                goto L_Gloves;
                break;
            case 4:
                closedialog;
                goodbye;
                close;
        }
    } while (1);


L_Gloves:
    .@q=getq(CandorQuest_Rosen);
    .@b=getq(ShipQuests_ChefGado);
    mes "";
    if (BaseLevel < 4) goto L_NoLevel;
    if (.@b < 2) goto L_NoGloves;
    if (.@q >= 3) goto L_Complete;
    .@k=getq2(CandorQuest_Rosen); // Get number of kills (via getq2)

    mesn;
    if (.@q == 0) {
        mesq l("Ah, I see you have some used gloves. I'm not sure if you can even mine with it...");
        next;
        mesn;
        mesq l("Uhm, maybe I could teach you something, too. Go mine 5 @@. You should find some at northeast of the Island.", getmonsterlink(DiamondBif));
        if (TUTORIAL) mesc l("Bif is a monster shaped like weird rocks. Diamond Bif is a Bif with higher chances to drop Diamonds.");
        next;
        mesn;
        mesq l("These monsters are a great source of raw crafting materials.");
        setq CandorQuest_Rosen, 1, 0;
    } else if (.@q == 1) {
        mesq l("You didn't mine enough @@. The perfect spot is at northeast of this island. It takes a while to them respawn, so don't hurry.", getmonsterlink(DiamondBif));
    } else if (.@q == 2) {
        mesq l("Wow! Those pitiable gloves sure weren't made for mining. They're almost ruined!");
        mesq l("Here, take this @@. It will be better suited!", getitemlink(CandorGloves));
        inventoryplace CandorGloves, 1;
        getexp 30, 5;
        getitem CandorGloves, 1;
        setq CandorQuest_Rosen, 3, 0;
    }
    close;

L_NoLevel:
    mesn;
    mesq l("You aren't strong enough.");
    next;
    mesn;
    mesq l("Go see someone else for now. Yes, you need level to take most tasks available on the world!");
    close;

L_NoGloves:
    mesn;
    mesq l("You should have some decent gloves, dude. These offer defense, as it's easier to handle your weapon and parry attacks.");
    next;
    mesn;
    mesq l("For sure the chef of Nard's ship could spare you a pair of gloves.");
    close;

L_Complete:
    if (BaseLevel > 5 && .@q < 5 && countitem(TolchiArrow))
        goto L_Task;
    mesn;
    mesq l("Ah, uhm, I'm not sure. We at Candor don't need much.");
    next;
    mesn;
    mesq l("You could try to get new equipment by doing more quests. You need level to use them, though.");
    next;
    mesn;
    mesq l("Alternatively, I think someone at the Land Of Fire Village is able to refine some items. Why don't you try it sometime?");
    close;

L_Task:
    if (.@q != 4) {
        mesn;
        mesq l("Actually, I see you have some @@. Ever tried a bow before?", getitemlink(TolchiArrow));
        next;
        mesn;
        mesq l("Bows give you a good attack range, in exchange of all your evasion.");
        mesq l("Meaning that once you equip a bow, you likely won't be able to dodge attacks.");
        next;
        mesn;
        mesq l("Well, if you are good, you can just not get hit. If you're not so good, then bows will be a pain.");
        setq CandorQuest_Rosen, 4;
    }
    mesq l("I was thinking, maybe I could make a @@ for you. But I want a few items:", getitemlink(TrainingBow));
    mesc l("@@/@@ @@", countitem(CactusDrink), 1, getitemlink(CactusDrink)); // Less than 1% drop
    mesc l("@@/@@ @@", countitem(Piberries), 1, getitemlink(Piberries)); // Can be bought, or 6% drop from Mana Bug
    next;
    mesq l("Do you have that with you?");
    if (askyesno() == ASK_YES) {
        mes "";
        if (!countitem(CactusDrink) ||
            !countitem(Piberries)) {
            mesn;
            mesq l("Now, listen closely. Jesusalva desgined most of the quests. And he hates cheaters and liars.");
            next;
            mesn;
            mesq l("Actually, he's just too lazy to add proper checks everywhere. If you try to cheat, you'll suffer some penalty.");
            next;
            mesn;
            mesq l("In this case, haven't I counted, I would have deleted only part of the items, then I would go silent. No refunds.");
            mesq l("That's how this world inhabitants deal with cheaters... So don't be one, my friend. You have been warned!");
            close;
        }
        inventoryplace TrainingBow, 1;
        delitem CactusDrink, 1;
        delitem Piberries, 1;
        setq CandorQuest_Rosen, 5;
        getitem TrainingBow, 1;
        mesn;
        mesq l("Here you go, my friend. Uhm, good luck with archery.");
    }
    close;

    function rosen_add_kills
    {
        .@qp=getq(CandorQuest_Rosen);
        .@kp=getq2(CandorQuest_Rosen); // Get number of kills (via getq2)
        setq CandorQuest_Rosen, .@qp, .@kp+1;
        //message strcharinfo(0), l("Set status @@ with @@ kills", .@qp, .@kp);
    }

    function rosen_max_kills
    {
        .@qp=getq(CandorQuest_Rosen);
        setq CandorQuest_Rosen, .@qp+1, 0;
        //message strcharinfo(0), l("End status @@", .@qp);
    }

OnKillMBif:
    .@q=getq(CandorQuest_Rosen);
    .@k=getq2(CandorQuest_Rosen); // Get number of kills (via getq2)
    if (.@q == 1) {
        if (.@k+1 >= 5) {
            rosen_max_kills();
            message strcharinfo(0), l("Go back to Rosen!");
        } else {
            rosen_add_kills();
            message strcharinfo(0), l("@@/5 @@", .@k+1, getmonsterlink(DiamondBif));
        }
    }
    end;


OnTimer1000:
    domovestep;

OnInit:
    initpath "move", 28, 36,
             "dir", DOWN, 0,
             "wait", 31, 0,
             "move", 31, 36,
             "dir", DOWN, 0,
             "wait", 31, 0,
             "move", 25, 35,
             "dir", UP, 0,
             "wait", 2, 0,
             "move", 29, 36,
             "dir", DOWN, 0,
             "wait", 31, 0;
    initialmove;
    initnpctimer;
    .distance = 5;
}
