// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

000-1,22,22,0	script	Emergency Exit	NPC_SOUL_CURSED,2,2,{
OnTouch:
OnTalk:
OnTalkNearby:
    // At any time, if you can't leave Nard ship, you must go to nard ship
    if (getq(ShipQuests_Julia) < 3) {
        warp "002-1", 53, 38;
        end;
    }

    // Switch LOCATION$ and warp to nearest town's Soul Menhir
    .@lx=array_find($@LOCMASTER_LOC$, LOCATION$);
    if (.@lx >= 0) {
        warp $@LOCMASTER_MAP$[.@lx], $@LOCMASTER_X[.@lx], $@LOCMASTER_Y[.@lx];
        end;
    }

    //if (getsavepoint(0) != "000-1") warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    if (getsavepoint(0) != "000-1") warp "Save", 0, 0;
    if (getsavepoint(0) != "000-1") end;
    //savepoint "002-1", 53, 38;
    warp "002-1", 53, 38;
    end;

// Save your location to last visited town for @ucp. I won't hate you for this.
OnSetLX:
    @lx=array_find($@LOCMASTER_LOC$, LOCATION$);
    savepoint $@LOCMASTER_MAP$[@lx], $@LOCMASTER_X[@lx], $@LOCMASTER_Y[@lx];
    end;

// Warp you according to LX
OnVisitLX:
    @lx=array_find($@LOCMASTER_LOC$, LOCATION$);
    warp $@LOCMASTER_MAP$[@lx], $@LOCMASTER_X[@lx], $@LOCMASTER_Y[@lx];
    end;

OnInit:
    setarray $@LOCMASTER_LOC$, "Candor", "Tulim", "Halin", "Hurns",   "LoF", "Lilit", "Nival", "Frostia";
    setarray $@LOCMASTER_MAP$,  "005-1", "003-1", "009-1", "012-1", "017-1", "018-5", "020-1",   "024-1";
    setarray $@LOCMASTER_X,          43,      40,      26,      86,     119,     111,      56,        94;
    setarray $@LOCMASTER_Y,          99,      49,      30,      69,      87,      53,      62,        42;

    //debugmes "Locmaster: Index 0: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[0], $@LOCMASTER_MAP$[0], $@LOCMASTER_X[0], $@LOCMASTER_Y[0];
    //debugmes "Locmaster: Index 2: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[2], $@LOCMASTER_MAP$[2], $@LOCMASTER_X[2], $@LOCMASTER_Y[2];
    //debugmes "Locmaster: Index 5: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[5], $@LOCMASTER_MAP$[5], $@LOCMASTER_X[5], $@LOCMASTER_Y[5];
    end;

}

