// TMW2 scripts.
// TMW2 Authors:
//      Saulc
//      Jesusalva
// Description:
//      Miner leader asks you to purge mine from monsters.
// Variable:
//      MineQuest_Tycoon
// Reference:
//      npc name from https://www.idleminertycoon.com/
// Steps:
//  0:  Not started
//
//          Experience Table
//              15  16  17    18   19   20
//              830,970,1120,1260,1420,1620,
//				1860,1990,2240,2504,2950,3426,3934,4474,6889,7995,
//              21   22    23   24   25   26   27  28   29    30
//
//
//  1:  Took ratto Quest 10 kills (lvl 15)
//  2:  Completed ratto Quest - 50 EXP 50 GP
//  3:  Awaiting next quest
//
//  4:  Took Cave Maggot Quest 25 kills (lvl 19)
//  5:  Completed Cave Maggot Quest - 200 EXP 150 GP + Miner Knife
//  6:  Awaiting next quest
//
//  7:  Took Cave Snake Quest 45 kills (lvl 24)
//  8:  Completed Cave Snake Quest - 1000 EXP 350 GP
//  9:  Awaiting next quest
//
// 10:  Took Red Scorpion Quest 100 kills (lvl 29)
// 11:  Completed  Red Scorpion Quest - Miner Hat
// 12:  Awaiting next quest
//
// 13:  Took  Black Scorpion Quest 200 kills (lvl 34)
// 14:  Completed Black Scorpion Quest - 12 000 EXP 5 000 GP
// 15:  Finished all trainment

007-1,71,65,0	script	Tycoon	NPC_PLAYER,{
    .@q=getq(MineQuest_Tycoon);

    mesn;
    mesq lg("Hello my friend!");
    next;
    mesq lg("Some of my miners friends died in this mine, because there are plenty of dangerous monsters.");
    next;
    mesq lg("Would you like to help us? Maybe you just need some information.");
    next;
    goto L_Menu;

L_PreMenu:
    mesn;
    mesq l("I need your help dude!");
    next;
    goto L_Menu;

L_Menu:
    mesn strcharinfo(0);
    menu
        rif(getq(MineQuest_Tycoon) < 15, l("I am a brave adventurer! How can I help you?")),           L_Purge,
        rif(getq(MineQuest_Pickaxe) == 1, l("Can I become a miner?")), L_Miner,
        l("What does miners do in cave?"),             L_CaveMiner,
        l("Can you give me a tip?"),                   L_Tip,
        l("Are you a terranite?"),                     L_Terranite,
        l("I don't care about dirty miners problem."), L_Close,
        l("No, thanks, it's not my job!"),             L_Close;

L_Purge:
    .@q=getq(MineQuest_Tycoon);
    mes "";
    if (BaseLevel < 15) goto L_NoLevel;
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)

    mesn;
    if (.@q == 0) {
        mesq l("Ok, you look strong enough. Now, let me think on an easy task for you...");
        next;
        mesq l("Ah! I know. Kill 10 @@. They usually are near the mine's inn.", getmonsterlink(Ratto));
        setq MineQuest_Tycoon, 1, 0;
    } else if (.@q == 1) {
        mesq l("You are killing @@/10 @@. They usually are near the mine's Inn.", .@k, getmonsterlink(Ratto));
    } else if (.@q == 2) {
        mesq l("Good job! Here is your reward!");
        getexp 300, 0; // 10% of level requeriment (r7.4) (53->83->300)
        set Zeny, Zeny + 50;
        setq MineQuest_Tycoon, 3, 0;
    } else if (.@q == 3 && BaseLevel >= 19) {
        mesq l("Less rattos means more food. So, this time the monsters will be a little stronger.");
        next;
        mesq l("Ok, now I want you to purge the cave from maggots. Kill 25 @@. They keep respawning and annoy miners team.", getmonsterlink(CaveMaggot));
        setq MineQuest_Tycoon, 4, 0;
    } else if (.@q == 3 && BaseLevel < 19) {
        mesq l("You are too weak for my task, please come back later with some levels.");
    } else if (.@q == 4) {
        mesq l("You are killing @@/25 @@ at the cave. Good progress!", getmonsterlink(CaveMaggot));
    } else if (.@q == 5) {
        mesq l("Perfect! Here is your reward!");
        getexp 800, 0; // (r7.5 158→800)
        set Zeny, Zeny + 150;
        getitem MinerKnife, 1;
        setq MineQuest_Tycoon, 6, 0;
    } else if (.@q == 6 && BaseLevel >= 24) {
        mesq l("That was fast. Do not forget to sell their... parts... for some extra cash.");
        next;
        mesq l("Now, please kill 45 @@, Try to collect some lamps in the same time, they could be useful later.", getmonsterlink(CaveSnake));
        setq MineQuest_Tycoon, 7, 0;
    } else if (.@q == 6 && BaseLevel < 24) {
        mesq l("You are too weak for my task, please come back later with some levels.");
    } else if (.@q == 7) {
        mesq l("You are killing @@/45 @@ at the cave.", .@k, getmonsterlink(CaveSnake));
    } else if (.@q == 8) {
        mesq l("I hope you got a Jean Shorts, but that depends on luck. Anyway, here is your reward.");
        getexp 1150, 0; // (r7.5 1050 → 1150)
        set Zeny, Zeny + 350;
        setq MineQuest_Tycoon, 9, 0;
    } else if (.@q == 9 && BaseLevel >= 29) {
        mesq l("If you want to be a real miner friend, do this hard task for me. I will reward you with a @@.", getitemlink(MinerHat));
        next;
        mesq l("I'll ask you to kill 100 @@. This will prove your worth. Good luck.", getmonsterlink(RedScorpion));
        setq MineQuest_Tycoon, 10, 0;
    } else if (.@q == 9 && BaseLevel < 29) {
        mesq l("You are too weak for my task, please come back later with some levels.");
    } else if (.@q == 10) {
        mesq l("You are killing @@/100 @@. It's still in progress.", .@k, getmonsterlink(RedScorpion));
    } else if (.@q == 11) {
        mesq l("Congratulations! Here is your reward, a @@!", getitemlink(MinerHat));
        mesq l("Mining opearations advance steadly day by day with your help, thanks! Maybe one day, we find a Mana Stone here!");
        inventoryplace MinerHat, 1;
        getitem MinerHat, 1;
        getexp 3800, 0; // r7.5 1365→3800
        setq MineQuest_Tycoon, 12, 0;
    } else if (.@q == 12 && BaseLevel >= 40) {
        mesq l("The last task I could give you is to exterminate 200 @@. They actually stop miners progress on the mine.", getmonsterlink(BlackScorpion));
        if (BaseLevel < 45)
            mesc l("WARNING: This is a level 45 quest!"), 1;
        setq MineQuest_Tycoon, 13, 0;
    } else if (.@q == 12 && BaseLevel < 40) {
        mesq l("You are too weak for my last task, please come back later with some levels.");
    } else if (.@q == 13) {
        mesq l("You are killing @@/200 @@ at the cave.", .@k, getmonsterlink(BlackScorpion));
        if (BaseLevel < 45)
            mesc l("WARNING: This is a level 45 quest!"), 1;
    } else if (.@q == 14) {
        mesq l("Wow! You did it! I do not think anyone else could have done that.");
        mesq l("Here, take this @@ - you deserve it! And here is some gold and experience.", getitemlink(PolishedRuby));
        inventoryplace PolishedRuby, 1;
        getitem PolishedRuby, 1;
        getexp 55000, 0; // About 94% from needed EXP to level up (you won't be level 45 when you finish)
        set Zeny, Zeny + 5000;
        setq MineQuest_Tycoon, 15, 0;
    }
    close;

L_NoLevel:
    mesn;
    mesq l("Ah, yes... You see, there is just no task I can give to you right now. You are too weak to fight these monsters.");
    next;
    mesn;
    mesq l("come back stronger, and I will give you a task.");
    next;
    goto L_Close;



L_Miner:
    .@price=800;
    if (getq(MineQuest_Tycoon) >= 9)
        .@price-=200;
    if (getq(MineQuest_Tycoon) >= 12)
        .@price-=100;
    if (getq(MineQuest_Tycoon) == 15)
        .@price-=300;
    if (getq(MineQuest_Caelum) >= 2)
        .@price-=150;

    mes "";
    mesn;
    mesq l("Well, I'm the miner leader, indeed. A miner need to have the full set.");
    next;
    mesn;
    mesq l("The uniform would be: @@, @@, @@ and @@", getitemlink(LeatherShirt), getitemlink(MinerHat), getitemlink(MinerGloves), getitemlink(Pickaxe));
    next;
    mesn;
    mesq l("I can sell you the Pickaxe for @@ GP if you have and @@ the remaining equipment.", .@price, b(l("equip")));
    if (Zeny < .@price)
        close;

    // Allow you to change
    enable_items();
    next;
    disable_items();
    // If you're in proper uniform, DO NOT ASK FOR CONFIRMATION
    if (
        getequipid(EQI_HEAD_TOP) == MinerHat &&
        getequipid(EQI_HEAD_MID) == LeatherShirt &&
        getequipid(EQI_GARMENT)  == MinerGloves &&
        Zeny >= .@price) {

        inventoryplace Pickaxe, 1;
        Zeny-=.@price;
        getitem Pickaxe, 1;
        setq MineQuest_Pickaxe, 2;
        mesn;
        mesq l("Good. You look like a real miner! Take this @@ and go mine bifs!");
        if (TUTORIAL) {
            next;
            mesc l("@@ is a two-handed weapon with very low damage and attack speed.", getitemlink(Pickaxe));
            mesc l("While it may not be suitable for fighting, by using it against Bifs and other mineral formations, you'll be able to get more ore.");
            next;
            mesc l("You may even obtain ores which you wouldn't otherwise obtain normally, and the drop rate is fixed.");
            mesc l("A Miner is a must-have for a craftsman/craftswoman, as they are in constant need of ores and coal.");
        }
        next;
    }
    goto L_Menu;


L_CaveMiner:
    mes "";
    mesn;
    mesq l("Tulimshar miners mainly try to obtain gems.");
    next;
    mesq l("In this cave we can easily find Coal, Iron ore, and sometimes @@.", getitemlink(Ruby)); // maybe add link for coal and iron ?
    next;
    goto L_Menu;

L_Tip:
    mes "";
    mesn;
    mesq l("If you fight Black scorpion or others dangerous monster you should use the 'hit and run' tactic.");
    next;
    mesn;
    mesq l("To do this you need to disable auto move to target in 'yellow bar'.");
    next;
    mesn;
    mesq l("To open yellow Bar you need to right click on your health bar and active status bar, also called yellow bar. Or you can press '8' on numeric keypad.");
    next;
    mesn;
    mesq l("Now you need to fight monster with the maximum range, you can to do this do these steps in order: lock enemy, Attack, hit, move backwards of one tile, hit, ETC.");
    next;
    mesn;
    mesq l("With this tip you will be able to fight strong monsters. It's on Saul's Strategy Guide Against Archwizards.");
    next;
    goto L_Menu;

L_Terranite:
    mes "";
    mesn;
    mesq l("Haha, no, I'm not! But we found traces of them.");
    next;
    mesn;
    mesq l("Terranite probably mined part of this mine long time ago.");
    next;
    mesn;
    mesq l("That can explain why we have difficulties to find rare gems here. They probably were already extracted.");
    next;
    goto L_Menu;


L_Close:
    close;


    function tycoon_add_kills
    {
        .@qp=getq(MineQuest_Tycoon);
        .@kp=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
        setq MineQuest_Tycoon, .@qp, .@kp+1;
        //message strcharinfo(0), l("Set status @@ with @@ kills", .@qp, .@kp);
    }

    function tycoon_max_kills
    {
        .@qp=getq(MineQuest_Tycoon);
        setq MineQuest_Tycoon, .@qp+1, 0;
        //message strcharinfo(0), l("End status @@", .@qp);
    }

OnKillRatto:
    .@q=getq(MineQuest_Tycoon);
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
    if (.@q == 1) {
        if (.@k+1 >= 10) {
            tycoon_max_kills();
            message strcharinfo(0), l("All rattos are dead! Go back to Tycoon.");
        } else {
            tycoon_add_kills();
            message strcharinfo(0), l("@@/10 Rattos", .@k+1);
        }
    }
    fix_mobkill(Ratto);
    end;

OnKillCaveMaggot:
    .@q=getq(MineQuest_Tycoon);
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
    if (.@q == 4) {
        if (.@k+1 >= 25) {
            tycoon_max_kills();
            message strcharinfo(0), l("All cave maggots are dead! Go back to Tycoon.");
        } else {
            tycoon_add_kills();
            message strcharinfo(0), l("@@/25 Cave Maggots", .@k+1);
        }
    }
    fix_mobkill(CaveMaggot);
    end;
OnKillCaveSnake:
    .@q=getq(MineQuest_Tycoon);
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
    if (.@q == 7) {
        if (.@k+1 >= 50) {
            tycoon_max_kills();
            message strcharinfo(0), l("All cave snakes are dead! Go back to Tycoon.");
        } else {
            tycoon_add_kills();
            message strcharinfo(0), l("@@/50 Cave Snakes", .@k+1);
        }
    }
    fix_mobkill(CaveSnake);
    end;
OnKillRedScorpion:
    .@q=getq(MineQuest_Tycoon);
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
    if (.@q == 10) {
        if (.@k+1 >= 100) {
            tycoon_max_kills();
            message strcharinfo(0), l("All red scorpions are dead! Go back to Tycoon.");
        } else {
            tycoon_add_kills();
            message strcharinfo(0), l("@@/100 Red Scorpions", .@k+1);
        }
    }
    fix_mobkill(RedScorpion);
    end;
OnKillBlackScorpion:
    .@q=getq(MineQuest_Tycoon);
    .@k=getq2(MineQuest_Tycoon); // Get number of kills (via getq2)
    if (.@q == 13) {
        if (.@k+1 >= 200) {
            tycoon_max_kills();
            message strcharinfo(0), l("All black scorpions are dead! Go back to Tycoon.");
        } else {
            tycoon_add_kills();
            message strcharinfo(0), l("@@/200 Black Scorpions", .@k+1);
        }
    }
    fix_mobkill(BlackScorpion);
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, MinerHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ArtisTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 19);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 8);

    .sex = G_MALE;
    .distance = 4;

    end;
}
