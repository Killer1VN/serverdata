// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Responsible for recovering the lost pages (SpellBookPage)
//    helperBookpages*
//
// NivalisQuest_BlueSagePagefinder STRUCTURE
//  FIELD 1:
//      MAIN STATE
//  FIELD 2:
//      nº of pages found (0~31)
//  FIELD 3:
//      nº of duplicates found (for randomness control - capped at 60)

020-7-1,45,39,0	script	Teuvo	NPC_BLUESAGEWORKER_MA,{
    function askQuestion;
    mesn;
    mesc l("@@ seems to be upset.", .name$);
    mesq l("I always thought it was a bad idea to play around with the slimes. And as if holding them in here isn't bad enough, no, they also had to mess around with magic.");
    askQuestion();
    close;

function askQuestion {
    next;
    if (.@qt >= 255) {
        mesn;
        mesq l("Thanks for helping out here. Do you need anything else?");
    } else if (.@qt) {
        mesn;
        mesq l("Did you find some bookpages? Ensio will take them.");
    }

    // Mainframe Loop
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
        .@qt=getq2(NivalisQuest_BlueSagePagefinder);
        next;
        mes "";
        select
            rif(.@qt == 255, ""),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 2:
                mesn;
                mesq l("With a mask? Hm. I don't remember.");
                break;
            case 3:
                mesn;
                mesq l("Peetu? He's one of those magic wielders who think they can do anything. Heh, you see the result here.");
                next;
                mesn;
                mesq l("But to be fair, from what I've seen Peetu was always very attentive and dutiful.");
                if (!(.@q3 & .bsId))
                    setq3 NivalisQuest_BlueSage, .@q3 | .bsId;
                break;
        }
    } while (@menu != 4);
    close;
}

OnInit:
    .bsId=BS_NPC07;
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;
}
