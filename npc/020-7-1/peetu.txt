// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Peetu, gifted since birth, only 1 NPC each 100 are born mages
//
// NivalisQuest_BlueSage STRUCTURE
//  FIELD 1:
//      INVESTIGATION
//      1 - STBY OUTSIDE
//      2 - ACCESS GRANTED - But Peetu is crying too much
//      3 - Peetu was calmed down, go talk to Oskari about him
//      4 - Oskari is OK with peetu, but wanna hear from others. He also sends you
//          to ask what Peetu happened
//      5 - Adultered ingredients seems the cause, report to Elias
//      6 - Elias is now worried about a visitor. Ask people about and report.
//      7 - If everyone found the visitor, confirm Elias the worries
//      8 - Elias sent you to Oskari to inform the issue. Blue Sage probably knew all along.
//      will not advance unless everyone thinks Peetu is good.
//      9 - Oskari accepts the cause. Tells to report Peetu that it probably was
//          a saboutage, to check if the Silk Cocoon really was there.
//      10 - Peetu confirmed the saboutage. Report to Blue Sage.
//      11 - Blue Sage accepted the evidence, and explains about other sages issues.
//          It's not known who or what is behind this. He excuses for making you waste
//          your time. He asks you to return to him later, as he needs to write letters.
//      12 - QUEST COMPLETE - You collected your reward
//          Also picked up a letter for Frostia Mayor, about the incident (Main Story).
//  FIELD 2:
//      Bitwise (BS_QVISITOR)
//  FIELD 3:
//      Bitwise (BS_QHELPER)

020-7-1,122,27,2	script	Peetu	NPC_BLUESAGEWORKER_MC,{
    function pWaiting;

    function pIntro;
    function pContinue;
    function pReflection;
    function pInvestigation;
    function pComplete;

    .@q=getq(NivalisQuest_BlueSage);
    .@q2=getq2(NivalisQuest_BlueSage);
    .@q3=getq3(NivalisQuest_BlueSage);

    if (.@q <= 4)
        npctalk3 any(l("*sob sob*"), l("*crying*"));

    switch (.@q) {
        case 2:
            pIntro();
            break;
        case 3:
            pWaiting("Oskari");
            break;
        case 4:
            pReflection();
            break;
        case 5:
            pWaiting("Elias"); // In case they skipped all dialogs and are now lost.
            break;
        case 6:
        case 7:
        case 8:
            pWaiting("Oskari");
            break;
        case 9:
            pInvestigation();
            break;
        case 10:
        case 11:
            pWaiting("Blue Sage");
            break;
        case 12:
            pComplete();
            break;
        default:
            warp "Save", 0, 0;
            percentheal -100, -100;
            end;
            break;
    }
    close;

// Here we begin
function pWaiting {
    .@name$=getarg(0, "##1##BBUG, REPORT ME: THE FLYING COW##0##b");
    mesn;
    mesc l("*sniff sniff*");
    mesq l("I'm waiting for @@ feedback... Please go talk to them! %%S", .@name$);
    close;
}

// Peetu is too upset with failing (yeah, he is that kind of perfectionist here).
// We should find a way to calm him down.
function pIntro {
    .@q=getq(NivalisQuest_BlueSage);
    .@q2=getq2(NivalisQuest_BlueSage);
    .@q3=getq3(NivalisQuest_BlueSage);
    mesn;
    mesc l("*sniff sniff*");
    next;
    select
        l("Hello Peetu."),
        rif(.@q2 >= 1, l("[Give him some salty Sea Drops?]")),
        rif(.@q2 >= 2, l("[Give him some tasty Chocolate Bar?]")),
        rif(.@q2 >= 3, l("[Give him a Mouboo Figurine to play with?]")),
        rif(.@q2 >= 4, l("[Slap his hands to surprise him and get his attention?]")),
        rif(.@q2 >= 5, l("SHUT UP PEETU, I'M ALREADY TIRED OF LISTENING YOUR CRIES!")),
        rif(.@q2 >= 5, l("I give up. You're hopeless.")),
        rif(.@q2 >= 5, l("Have you cried enough?")),
        rif(.@q2 >= 5, l("[Pat his shoulder and say everything will be fine.]")),
        rif(.@q2, l("Hi Peetu, are you calmer now?"));
    mes "";
    switch (@menu) {
        case 1:
            if (.@q2 < 1)
                setq2 NivalisQuest_BlueSage, 1;
            break;
        case 2:
            if (!countitem(SeaDrops)) {
                mesc l("You don't have @@.", getitemlink(SeaDrops));
                close;
            }
            if (.@q2 < 2)
                setq2 NivalisQuest_BlueSage, 2;
            break;
        case 3:
            if (!countitem(ChocolateBar)) {
                mesc l("You don't have @@.", getitemlink(ChocolateBar));
                close;
            }
            if (.@q2 < 3)
                setq2 NivalisQuest_BlueSage, 3;
            break;
        case 4:
            if (!countitem(MoubooFigurine)) {
                mesc l("You don't have @@.", getitemlink(MoubooFigurine));
                close;
            }
            if (.@q2 < 4)
                setq2 NivalisQuest_BlueSage, 4;
            break;
        case 5:
            mesn;
            mesq l("AH!");
            next;
            mesc l("You seem to have gotten Peetu's attention for a while.");
            next;
            if (.@q2 < 5)
                setq2 NivalisQuest_BlueSage, 5;
            break;
        case 9:
            pContinue();
            break;
    }
    mesn;
    mes l("WAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHH");
    mesc l("@@ burst in tears.", .name$);
    next;
    mesn strcharinfo(0);
    mesc l("I probably should look in a way of calming him down.");
    close;
}

// He is calmer now
function pContinue {
    mesn;
    mesc l("*sniff sniff*");
    next;
    select
        l("What have happened? Why are you crying?"),
        l("Maybe I can help to ease your pain?"),
        l("Don't cry any further. I am here to help."),
        l("Please tell me calmly what happened so I can help.");
    mes "";
    mesn;
    mesq l("It's *sniff* It's all my fault... *sniff*");
    next;
    mesn;
    mesc l("*sniff* *sniff*");
    mesq l("It was my job to seal the slimes away for the night, but I somehow messed it up!");
    next;
    mesn;
    mesc l("*tears weeling up*");
    mesq l("And now I'm going to lose my job and I'll have to leave here and no other sage would give me a new appointment and I don't know what else to do!");
    next;
    mesc l("@@ latches onto you and starts sobbing on your shoulder.", .name$);
    select
        l("[Try to console him]"),
        l("[Shake him and tell him to pull himself together]"),
        l("[Push him away from you and leave]");
    mes "";
    if (@menu == 3) {
        setq2 NivalisQuest_BlueSage, 4;
        close;
    }
    mesc l("@@ calms a bit.", .name$);
    next;
    mesn;
    mesq l("Uh. You're probably right. I'm very sorry. I'm just... You know, I wanted to become a scholar of the sages, studying and... Oh, how could I mess that up? Did you see the library? It caused so much damage! What else should they do other than kick me out?");
    next;
    mesn;
    mesq l("I would kick me out myself! I'm such a failure! A complete disaster! %%i");
    next;
    select
        l("Maybe you should talk to your chief about that?"),
        l("It doesn't make much sense to draw overhasty conclusions."),
        l("Do you have a clue about what went wrong?");
    mes "";
    mesn;
    mesq l("Well ... but ... I mean ... I don't know ... ");
    next;
    mesn;
    mesq l("Would you ... uhm ... would you talk to Chief Oskari for me? And ask her what she plans to do about me? I... I just don't feel capable of doing that myself right now. I'll try to pull myself together in the meanwhile.");
    setq NivalisQuest_BlueSage, 3, 0;
    close;
}

// Report that Oskari is not planning to fire him (yet), and is trying to understand
// what went wrong so it do not repeat.
function pReflection {
    .@q=getq(NivalisQuest_BlueSage);
    .@q2=getq2(NivalisQuest_BlueSage);
    .@q3=getq3(NivalisQuest_BlueSage);
    select
        l("Good news - Oskari doesn't plans in firing you (yet)!"),
        l("I'm here to investigate what exactly happened so this doesn't happens again."),
        l("Everyone commit mistakes and Oskari was very understanding. I need to do some questions about the incident though."),
        l("Oskari have a good opinion of you. Can you tell what exactly happened?");
    mes "";
    mesn;
    mesc l("*sniff sniff*");
    mesq l("So... You see... I usually pay a lot of attention to my work. Especially when doing something as delicate as sealing away the slimes for the night.");
    next;
    mesn;
    mesq l("We're doing researches on slimes, the explosive ones. You probably saw when they explode, they damage anything close to them, friendly or not, right?");
    next;
    mesn;
    mesq l("Nikolai said it was important, so we keep a few of them locked in the basement. I apply a spell to seal them so they don't wander around, explode around, or... *sniff*...");
    next;
    mesn;
    mesq l("It's a very delicate spell. I usually start to prepare it while the helpers are still working on their research and experiments. That way, when they're done, they can just come and place the slimes under the seal.");
    next;
    mesn;
    mesq l("So... @@ That night, I cast it as usual. Everything seemed alright so I went to bed.", "##9*sniff*##0");
    next;
    mesn;
    mesc l("Eyes grows!");
    mesq l("I woke up in a shake! I felt... Oh, it's hard to describe... As if the spell suddenly started inflating, getting bigger... bigger... weaker... weaker...");
    next;
    mesn;
    mesc l("*snap fingers*");
    mesq l("And then, it was no more! It vanished! In a matter of minutes the slimes were all over the library. A good thing they are slow, none escaped to the town.");
    next;
    mesn;
    mesq l("Of course I hurried there as fast as I could, yelling to wake up everyone, but that caused so much confusion that we fell over each other in the corridor and when we finally reached the library, the slimes were already spread all over the room, eating or exploding the books.");
    next;
    select
        l("*snooze*"),
        l("And have you thought in the cause?");
    mes "";
    if (@menu == 1)
        clear;
    mesn;
    mesq l("Now that I've described the spells to you, I'm sure I cast them correctly. But... I think there is a way to have these effects. It's silly though.");
    next;
    mesn;
    mesq l("I never tried it, because it makes no sense to do that, but theoretically adding some Silk Cocoons could have such an effect. Uh... but that should not happen.");
    next;
    mesn;
    mesq l("Could you please ask @@ about it? The ingredients... It would still be my fault...", b(l("Elias")));
    next;
    mesc l("@@ is on the verge of crying again. Better leave out and look for Elias.", .name$);
    setq1 NivalisQuest_BlueSage, 5;
    close;
}

// Peetu hurries back to check what happened. Wait 3 minutes in the library.
// He'll then say that in fact there was silk cocoon
function pInvestigation {
    .@q=getq(NivalisQuest_BlueSage);
    .@q2=getq2(NivalisQuest_BlueSage);
    .@q3=getq3(NivalisQuest_BlueSage);
    mesn;
    mesc l("*sniff sniff*");
    mesq l("So... Anything new on my situation?");
    next;
    select
        l("Not yet."),
        l("Yes, you'll be fired."),
        l("Yes, Chief Oskari suspects a sabotage.");
    mes "";
    switch (@menu){
    case 1:
        close;
    case 2:
        mesn;
        mes l("WAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHH");
        mesc l("@@ burst in tears.", .name$);
        next;
        percentheal 0, -100;
        mesn strcharinfo(0);
        mesc l("Meh, telling him it was a lie is not going to help.");
        close;
    }
    mesn;
    mesc l("*eyes widen up*");
    mesq l("A... A sabotage?! O.o");
    next;
    mesn strcharinfo(0);
    mesq l("Keep your voice down! And yes. Can you confirm @@ was the cause?", getitemlink(SilkCocoon));
    next;
    mesn;
    mesq l("...Yes. Hold tight.");
    next;
    mes "... ... ...";
    next;
    mesn;
    mesq l("...Yes. It was there. Please report to Blue Sage at once.");
    setq1 NivalisQuest_BlueSage, 10;
    close;
}

// The crime was "solved"
function pComplete {
    mesn;
    mesq l("Oh, hey, welcome back, @@! Thanks for all your help!", strcharinfo(0));
    next;
    mesn;
    .@subject$=any(l("town finances"), l("house finances"), l("town damage by monsters"), l("library damage"), l("supply report"), l("magic book"), l("town overview"));
    mesq l("I'm currently going over some of the household paperwork. Right now I'm inspecting the @@. The work never stops!", .@subject$);
    close;
}

OnInit:
    .sex=G_MALE;
    .distance=5;
    npcsit;
    end;
}

