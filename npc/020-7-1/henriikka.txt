// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    Responsible for recovering the lost pages (SpellBookPage)
//    helperBookpages*
//
// NivalisQuest_BlueSagePagefinder STRUCTURE
//  FIELD 1:
//      MAIN STATE
//  FIELD 2:
//      nº of pages found (0~31)
//  FIELD 3:
//      nº of duplicates found (for randomness control - capped at 60)

020-7-1,33,39,4	script	Henriikka	NPC_BLUESAGEWORKER_FA,{
    function askQuestion;
    mesc l("You see a helper looking through some papers that have a strange smell.");
    askQuestion();
    close;

function askQuestion {
    next;
    if (.@qt >= 255) {
        mesn;
        mesq l("I'm so glad we didn't have to go out to hunt the slimes! Thank you!");
    } else if (.@qt) {
        mesn;
        mesq l("If you get any bookpages from the slimes, bring them to Ensio. Thanks for your help!");
    } else {
        mesn;
        mesc l("*sighs*");
        mesq l("Oh, hey. Welcome to the library, or what's left of it.");
    }

    // Mainframe Loop
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
        .@qt=getq2(NivalisQuest_BlueSagePagefinder);
        next;
        mes "";
        select
            rif(.@qt < 255, "What are you doing?"),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 1:
                mesn;
                mesq l("We're trying to repair the books by collecting the ripped out bookpages and sorting them and copying them for new books. It's a lot to do, and after being eaten by a slime they stink!");
                next;
                mesn;
                mesq l("And a lot of pages are missing, since most of the slimes escaped. I suppose we'll have to go out and hunt them once we're done here. Unless someone else hunt them for us.");
                if (!.@qt) {
                    next;
                    mesn;
                    mesq l("Well, these slimes are dangerous, but if you find some pages, be sure to bring them to Ensio. This will help us a lot. Eh, if you're interested in helping, that is. %%1");
                }
                break;
            case 2:
                mesn;
                mesq l("With a mask? I don't really remember... We have so many visitors. Though I suppose someone wearing a mask would be noticeable... But I'm so worn out from the past few days that I'm just glad I can even recall my own name! Sorry.");
                break;
            case 3:
                mesn;
                mesq l("Oh, I never really thought about that. He was the one who failed the sealing, right? But I heard it's a quite difficult spell, so I suppose this could've happened to any mage. I don't know. Why are you asking such difficult questions?");
                next;
                mesn;
                mesq l("I need to go on with sorting the bookpages now.");
                break;
        }
    } while (@menu != 4);
    close;
}

OnInit:
    .sex=G_FEMALE;
    .distance=5;
    npcsit;
    end;
}


