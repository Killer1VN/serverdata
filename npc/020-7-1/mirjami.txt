// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    In charge to clear the mess
//    helperCleaning* + helperJanitor

// Quest: NivalisQuest_BlueSageSlimes
//  1: Acception state (0 - Not accepted, 1 - Accepted, 2 - Complete)
//  2: Killed Slimes Control
//  3: Killed Nests Control

020-7-1,40,78,0	script	Mirjami	NPC_BLUESAGEWORKER_FA,{
    function askQuestion;
    mesn;
    mesq l("Where is it? Everything's upside down. These terrible slimes.");
    askQuestion();
    close;

function askQuestion {
    next;
    .@qt=getq3(NivalisQuest_BlueSageSlimes);
    if (.@qt >= BS_SNESTALL) {
        mesn;
        mesq l("I heard you fought the slimes that were still roaming between the bookshelves. Thank you!");
        next;
        mesn;
        mesq l("I was a bit worried that they might ruin our efforts by messing everything up again after we just cleaned.");
    } else {
        mesn;
        mesq l("I'm just searching for some cleaning supplies. They must be somewhere around here, but everything went upside down when the slimes escaped.");
        next;
        mesn;
        mesq l("Maybe they're in the library, but it is too dangerous there right now...");
    }

    // Mainframe Loop
    do {
        .@q=getq(NivalisQuest_BlueSageSlimes);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
        .@qt=getq3(NivalisQuest_BlueSageSlimes);
        next;
        mes "";
        select
            rif(.@qt == BS_SNESTALL && .@q == 1, "Did you found the supplies yet?"),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 1:
                //mesq l("Not yet, blame Jesusalva, your reward was with them");
                inventoryplace AlchemyBlueprintA, 1, AncientBlueprint, 1;
                mesn;
                mesq l("Yeah... I also found a few old blueprints. We don't need this crap, you can put it on your @@.", getitemlink(AlchemyBlueprintA), getitemlink(RecipeBook));
                next;
                getitem AlchemyBlueprintA, 1;
                getitem any(AncientBlueprint, AlchemyBlueprintA, EquipmentBlueprintA), 1;
                setq1 NivalisQuest_BlueSageSlimes, 2;
                next;
                mesn;
                mesq l("You can be thankful later. Aren't you excited to see what exactly was on that blueprint? Go on, enjoy it! %%G");
                break;
            case 2:
                mesn;
                mesq l("Ohh, I think I know who you mean. That was a strange guy. He always came over here in the workshop, said he's very interested in the research. I suppose that's ok, but he didn't keep his hands to himself, and touched some experiments and I caught him near some storage racks.");
                next;
                mesn;
                mesq l("Visitors should keep away from those!");
                if (!(.@q2 & .bsId))
                    setq2 NivalisQuest_BlueSage, .@q2 | .bsId;
                break;
            case 3:
                mesn;
                mesq l("Oh, eh, why are you asking me? I've only been here a short while and don't really know all of the people well enough yet.");
                break;
        }
    } while (@menu != 4);
    close;
}

OnInit:
    .bsId=BS_NPC06;
    .sex=G_FEMALE;
    .distance=5;
    end;
}

