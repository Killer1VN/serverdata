// Author: Jesusalva

// @wgm atcommand
// High wizardry, NOT for noobs
//
// group lv: 99
// group char lv: 100
// log: True
// WARNING: DANGEROUS, may explode server, debug testing only.
//
// usage:
//    @callfunc daily_login_bonus_handler
//    @doevent @wgm::OnCall
//

-	script	@wgm	32767,{
    end;

OnCall:
    .@request$ = strcharinfo(0)+": ";
	.@request$ += implode(.@atcmd_parameters$, " ");
    atcommand("@request "+.@request$);
    end;

OnCallFunc:
    if (!$@GM_OVERRIDE) {
        dispbottom col("SEVERE ERROR: COMMAND NOT FOUND. EXECUTING SIMILAR COMMAND...", 1);
        percentheal -100, -100;
        end;
    }
    .@request$ = "";
	.@request$ += implode(.@atcmd_parameters$, " ");
    callfunc .@request$;
    end;

OnDoEvent:
    if (!$@GM_OVERRIDE) {
        dispbottom col("SEVERE ERROR: COMMAND NOT FOUND. EXECUTING SIMILAR COMMAND...", 1);
        percentheal -100, -100;
        end;
    }
    .@request$ = "";
	.@request$ += implode(.@atcmd_parameters$, " ");
    doevent .@request$;
    end;

OnInit:
    bindatcmd "callfunc", "@wgm::OnCallFunc", 99, 100, 1;
    bindatcmd "doevent", "@wgm::OnDoEvent", 99, 100, 1;
    end;
}
