// TMW2 script
// Evol script, gumi's script
//
// @w atcommand
// warps using anchors or map name
//
// usage:
//    @w <map or anchor> [, x [, y]]
//    #w "char" <map or anchor> [, x [, y]]
//
// example:
//    @w hali
//    @w halin
//    @w halinarzo
//    #w "char" hali

-	script	@w	32767,{
    end;

OnCall:
    .@request$ = strtoupper(strip(.@atcmd_parameters$[0])); // sanitize
    .@map$ = "";
    .@x = 0;
    .@y = 0;

    for (.@i = 0; .@i < .count; .@i += 2)
    {
        if (.@request$ ~= .aliases$[.@i]) {
            sscanf .aliases$[.@i + 1], "%s %d %d", .@map$, .@x, .@y;
            break;
        }
    }

    if (.@map$ == "") {
        .@map$ = .@atcmd_parameters$[0];
    }

    if (.@atcmd_parameters$[2] != "") {
        .@x = atoi(.@atcmd_parameters$[1]);
        .@y = atoi(.@atcmd_parameters$[2]);
    }

    // FIXME: here getmapusers() is used only to check if the map exists
    //        replace this when/if we get a dedicated function for that
    if (getmapusers(.@map$) < 0) {
        end; // invalid map
    }

    while (!checkcell(.@map$, .@x, .@y, cell_chkpass))
    {
        if (.@e == 50) end;
        .@x = rand(20, 250);
        .@y = rand(20, 250);
        ++.@e;
    }

    cwarp .@map$, .@x, .@y; // XXX: maybe here use a slide_or_warp function

OnInit:
    setarray .aliases$[0],

        // PROLOGUE
        "^START|^BEGIN",    "000-0 22 24",    // starting point
        "^NARD|^SHIP",      "002-1 53 38",    // nard's ship (outside instance)
        "^000-0",           "000-0 34 17",    // Intro Lookout Area
        "^000-0-0",         "000-0-0 33 26",  // Intro B Lookout Area
        "^LOOKOUT",         "000-0-0 33 26",  // Intro B Lookout Area

        // TMW2
        "^TULIM",           "003-1 41 48",    // Tulishmar
        "^CANDOR",          "005-1 35 102",   // Candor
        "^HALI",            "009-1 28 33",    // Halinarzo
        "^HURN",            "012-1 83 63",    // Hurnscald
        "^LOF",             "017-1 120 89",   // Land Of Fire Village
        "^PORT",            "019-2 94 118",   // Nivalis Port
        "^NIVAL",           "020-1 58 65",    // Nivalis
        "^FROST",           "024-1 94 42",    // Frostia
        "^LILIT",           "018-5 111 53",   // Lilit

        // TBR - To Be Removed
        "^BOSS",            "boss 45 45",      // (TBR) Monster King's Throne Room
        "^BOT",             "botcheck 26 30",  // (TBR) Botcheck area
        "^HEROES|^HH",      "018-2 72 66",     // (TBR) Heroes Hold

        // GM Stuff
        "^AEROS|^GM",       "001-1 235 26",    // Floating Island of Aeros (GM Events)
        "^ARENA",           "001-2 125 222",   // Aeros Arena (GM Events)
        "^SAULC",           "001-3 117 138",   // GM Palace
        "^EASTER",          "001-4 151 157",   // Easter Event Map
        "^WORK",            "001-5 22 79",     // Contributor's Cave
        "^JAIL|^PRISON",    "sec_pri 28 25";   // Jesusalva's Prison (and last line)


    .count = getarraysize(.aliases$[0]);

    bindatcmd "w", "@w::OnCall", 5, 80, 1;
}
