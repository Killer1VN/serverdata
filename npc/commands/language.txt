// TMW2 Script
// Author: Jesusalva
// With code parts from Julia (Evol)

// @lang atcommand
// Changes Language
//
// group lv: 0
// group char lv: 0
// log: False
//
// usage:
//    @lang
//

-	script	@lang	32767,{
    end;

OnCall:
    checkclientversion;
    mesq l("Which language do you speak?");
    next;
    asklanguage(LANG_IN_SHIP);
    mes "";
    mesn;
    mesq l("Ok, done.");
    close;

OnInit:
    bindatcmd "lang", "@lang::OnCall", 0, 60, 0;
    end;
}
