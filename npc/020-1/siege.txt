// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Nivalis Siege for Liberation Day
//    Each player process its own share of monsters. There are 10 waves.
//
//    Q_NivalisLibday
//    Day, Score, Temporary Time;

020-1,0,0,0	script	#NLib_Siege	NPC_HIDDEN,{

OnLoop:
    @nlib_time+=5; // This is looped every 5 s

    // Victory conditions: All monsters dead & number of waves filled. (Or if you reach level 40)
    if (BaseLevel >= 30 || (@nlib_wave >= 10 && mobcount("020-1", "#NLib_Siege::OnPetDeath") <= 0))
        goto L_CleanUp;

    // New wave condition: Waves pending and A- All Mobs Dead B- 4 minutes spent
    if (@nlib_wave < 10 && (mobcount("020-1", "#NLib_Siege::OnPetDeath") <= 0 || @nlib_time >= 240))
        goto L_NextRound;

    // reset timer
    addtimer(5000, "#NLib_Siege::OnLoop");
    end;

L_NextRound:
    @nlib_time=0;
    @nlib_wave = @nlib_wave + 1;
    // Prepare next round
    dispbottom l("Wave @@/10", @nlib_wave);
    .@amount=@nlib_wave+rand(1,2);
    freeloop(true);
	for (.@i = 0; .@i < .@amount; ++.@i) {
        .@monsterId=any(CaveMaggot, WhiteSlime, MagicGoblin, Bandit, GreenSlime,
                        CaveSnake, LavaSlime, DesertBandit, AngryRedScorpion,
                        Scorpion, RedScorpion, BlackSlime, Piousse, CandiedSlime,
                        AzulSlime, BlueSlime, SlimeBlast, RedSlime, AngryScorpion);
        areamonster "020-1", 20, 20, 100, 100, strmobinfo(1, .@monsterId), .@monsterId, 1, "#NLib_Siege::OnPetDeath";
    }
    freeloop(false);

    // reset timer
    addtimer(5000, "#NLib_Siege::OnLoop");
    end;

// Warp you back, and give you a random small score.
L_CleanUp:
    .@q2=getq2(Q_NivalisLibday);
    setq2 Q_NivalisLibday, .@q2+rand(1,5);
    warp "019-2", 43, 55;
    end;

OnPetDeath:
    .@lf=mobcount("020-1", "#NLib_Siege::OnPetDeath");
    dispbottom l("Mobs remaining: @@", .@lf);
    end;

}
