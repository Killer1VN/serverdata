// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Protect Nivalis

020-1,49,48,0	script	Lieutenant Joshua	NPC_PLAYER,{
    // The Monster King guild have a special menu
    if (strcharinfo(2) == "Monster King") goto L_MKControl;

    if ($NIVALIS_LIBDATE) {
        mesn;
        mesq l("Nivalis was liberated @@ ago.", FuzzyTime($NIVALIS_LIBDATE));
        close;
    } else {
        legiontalk; end;
    }

L_MKControl:
    mesn;
    mes "Oh noes! You've found the Nivalis control panel!";
    next;
    select
        l("Abort"),
        l("Initiate a siege");
    mes "";
    if (@menu == 2) {
        doevent "Lieutenant Joshua::OnStartSiege";
        closedialog;
    }
    close;


OnMKSiege:
OnStartSiege:
    kamibroadcast(col("WARNING! WARNING! Monster Army is moving towards Nivalis!!",1));
    do_siege("020-1", "019-2", "NIVAL", TP_NIVAL, .name$, .siegetime);
    initnpctimer;
    end;

// Timers
OnTimer5000:
    .siegetime+=5;
    do_siege("020-1", "019-2", "NIVAL", TP_NIVAL, .name$, .siegetime);
    switch (.siegetime) {
    // Monster Army arrives in town
    case 60:
        disablenpc "Serge";
        disablenpc "Knox The Traveler";
        disablenpc "Camilot";
        disablenpc "Mede";
        disablenpc "Gambler#020-4";
        disablenpc "Gambling Xan";
        disablenpc "Baktar";
        disablenpc "Bracco";
        disablenpc "Agostine";
        break;
    // Monster Army deployed in town
    case 90:
        disablenpc "Ben#NivBanker";
        break;
    // Monster army have withdrawn completly
    case MK_SIEGE_DURATION:
        .siegetime=0;
        announce(("Nivalis siege is over!"), bc_all);
        enablenpc "Serge";
        enablenpc "Knox The Traveler";
        enablenpc "Camilot";
        enablenpc "Mede";
        enablenpc "Gambler#020-4";
        enablenpc "Gambling Xan";
        enablenpc "Baktar";
        enablenpc "Bracco";
        enablenpc "Agostine";
        enablenpc "Ben#NivBanker";
        stopnpctimer;
        end;
        break;
    }

    // Loop again
    initnpctimer;
    end;

OnInit:
    .siegetime=0;
    .sex = G_MALE;
    .distance = 4;

    // Check items.xml for info about this.
    .@npcId = getnpcid();
    setunitdata(.@npcId, UDT_HEADTOP, Bull);
    setunitdata(.@npcId, UDT_HEADMIDDLE, LieutenantArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_SHIELD, LousyMoccasins);    // TODO FIXME: Display Boots
    setunitdata(.@npcId, UDT_WEAPON, Backsword);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 12);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 15);
    end;


}

