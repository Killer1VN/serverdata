// Map 018-2-1: Heroes' Hold - Castle manual warps
// Author: Jesusalva

// LoFQuest_HH
// Field 1
//   1- Accepted
//   2- Master Finished
//
// Field 2
//  BITWISE:
//      1 - Novice
//      2 - Intermediary
//      4 - Advanced
//      8 - Expert
//     16 - Master
//     32 - Ultimate
//     64 - Supreme
//    128 - Saulc's Madness
//    256 - The Mouboo Realm
//    512 - The Moubootaur Dungeon
//
// Field 3
//   0- The Loyalists
//   1- The Wildlife

018-2-1,26,26,0	script	#018-2-1_26_26	NPC_HIDDEN,1,1,{
    end;

OnTouch:
    .@hh=getq(LoFQuest_HH);
    if (.@hh <= 0) {
        npctalk3 l("The stairs lead to nowhere. However, there is a magic sigil on the bottom.");
        end;
    }
    if (HH_COOLDOWN > gettimetick(2)) {
        npctalk3 l("You still need to wait @@ before going to HH again.", FuzzyTime(HH_COOLDOWN));
        end;
    }

        .@q=getq2(LoFQuest_HH);
        mesn l("Heroes Hold");
        mes l("This is only for the skilled players. Newbies, KEEP OUT!");
        mes l("Time Limit: 25 minutes on any dungeon.");
        mes l("Please select target dungeon:");
        mes "";
        select
            l("Sorry, I am a newbie."),
            rif(BaseLevel >= 40, l("Novice Dungeon (Lv 40+)")), // Level 0-40
            rif(.@q & HH_NOVICE, l("Intermediary Dungeon (Lv 60+)")), // Level 21-60
            rif(.@q & HH_INTERMEDIARY, l("Advanced Dungeon (Lv 80+)")), // Level 41-80
            rif(.@q & HH_ADVANCED, l("Expert Dungeon (Lv 100+)")), // Level 61-100
            rif(.@q & HH_EXPERT, l("Master Dungeon (BOSS)")), // Boss Only
            l("Heroes Hold - Exchange Hall"),
            l("Information");
        mes "";
        if (@menu > 1 && @menu < 7)
            HH_COOLDOWN=gettimetick(2)+rand(90,150);
        switch (@menu) {
            case 2:
                @HH_LEVEL=HH_NOVICE;
                @HH_TIMER=0;
                warp "018-2-2@No", 37, 196;
                addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
                closedialog;
                break;
            case 3:
                @HH_LEVEL=HH_INTERMEDIARY;
                @HH_TIMER=0;
                warp "018-2-3@In", 204, 40;
                addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
                closedialog;
                break;
            case 4:
                @HH_LEVEL=HH_ADVANCED;
                @HH_TIMER=0;
                warp "018-2-2@Ad", 209, 178;
                addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
                closedialog;
                break;
            case 5:
                @HH_LEVEL=HH_EXPERT;
                @HH_TIMER=0;
                warp "018-2-3@Ex", 51, 40;
                addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
                closedialog;
                break;
            case 6:
                @HH_LEVEL=HH_MASTER;
                @HH_TIMER=0;
                warp "018-2-5@Ma", 132, 92;
                addtimer(500, "#HH_CONTROLLER01::OnPlayerCycle");
                closedialog;
                break;
            case 7:
                .@g=getq3(LoFQuest_HH);
                // Wildlife
                if (.@g)
                    warp "018-2-4", 24, 54;
                else
                    warp "018-2-4", 24, 33;
                // Loyalists

                closedialog;
                break;
            case 8:
                mes "";
                mesn l("Heroes Hold");
                mes l("The Heroes Hold is divided in seven dungeons level: Novice, Intermediary, Advanced, Expert, Master, Ultimate and Supreme.");
                next;
                mesn l("Heroes Hold");
                mes l("It is NOT designed for noobs. It is for the pain-seeking pro adventurers who laugh at death, and see danger as fun.");
                next;
                mesn l("Heroes Hold");
                mes l("The monsters on each Heroes Hold Dungeon will drop @@, a coin which can only be found here.", getitemlink(HeroCoin));
                next;
                mesn l("Heroes Hold");
                mes l("Use these coins to exchange for stuff. But beware: Each dungeon difficulty will increase the coin drop in the square value of previous.");
                next;
                mes l("This means that if you drop a coin on Advanced Dungeon, 4 coins will be dropped instead. On Expert, that would be 8.");
                mes l("Needless to say, monsters from Expert Dungeon usually drops more often than the ones from Advanced Dungeon.");
                next;
                mesn l("Heroes Hold");
                mes l(".:: Victory Conditions ::.");
                mes l("- Defeat the BOSS on each dungeon!");
                mes "";
                mes l(".:: Withdraw Conditions ::.");
                mes l("- Time runs out (25m).");
                mes l("- Get killed yourself.");
                mes l("- Teleport yourself away.");
                next;
                mes l("Dying inside the Heroes Hold Main Dungeon does not have a penalty. However, dying outside the castle have.");
                break;

        }
    close;
}


