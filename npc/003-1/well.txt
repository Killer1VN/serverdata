// TMW2 Script
// Author:
//    Saulc
//    Jesusalva

003-1,45,80,0	script	Well#003-1	NPC_NO_SPRITE,{
    if (countitem(Bucket) <= 0) {
        mesc l("This well is too deep and you don't have a bucket.");
    } else {
        mesc l("Hey you have a bucket! Too bad there are, you know, HOLES on it, so you can see.");
        mesc l("That's exactly why you can't use it to get water. Silly.");
    }
    if (!Zeny)
        close;
    next;
    mesc l("Throw a coin?");
   if (askyesno() != ASK_YES)
        close;

    mes "";
    Zeny=Zeny-1;
    mesc l("You throw a coin on the well.");
    next;

    .@n=rand(0, 10000);
    if (.@n <= 10) {
        getitem StrangeCoin, 1;
        mes l("##9Hey wait... Your coin turned on a @@!", getitemlink(StrangeCoin));
    } else if (.@n <= 50) {
        getitem CasinoCoins, 1;
        mes l("##9Hey wait... Your coin turned on a @@!", getitemlink(CasinoCoins));
    } else if (.@n <= 100) {
        Zeny=Zeny+2;
        mesc l("Hey wait... You found 2 GP!");
    } else if (.@n <= 250) {
        percentheal 100, 100;
        mesc l("Hey wait... You're enveloped by a bright light and fully healed!");
    } else if (.@n <= 300) {
        getexp rand(1,BaseLevel), rand(1,BaseLevel);
        mesc l("Hey wait... You're enveloped by a bright light and gain experience!");
    } else if (.@n > 9900) {
        mesc l("Hey wait... A monster!! Run for your life!!");
        getmapxy(.@m$, .@x, .@y, 0);
	    .@mobGID = monster(.@m$, .@x, .@y, "Croc", Croc, 1);
	    unitattack(.@mobGID, getcharid(CHAR_ID_ACCOUNT)); // Order the summoned monster to engage!!
    } else if (.@n > 9850) {
        mesc l("Hey wait... A monster!! Run for your life!!");
        getmapxy(.@m$, .@x, .@y, 0);
	    .@mobGID = monster(.@m$, .@x, .@y, "Blub", Blub, 1);
	    unitattack(.@mobGID, getcharid(CHAR_ID_ACCOUNT)); // Order the summoned monster to engage!!
    } else if (.@n > 9800) {
        mesc l("Hey wait... A monster!! Run for your life!!");
        getmapxy(.@m$, .@x, .@y, 0);
	    .@mobGID = monster(.@m$, .@x, .@y, "Red Scorpion", RedScorpion, 1);
	    unitattack(.@mobGID, getcharid(CHAR_ID_ACCOUNT)); // Order the summoned monster to engage!!
    } else if (.@n > 9700) {
        mesc l("Hey wait... A monster!! Run for your life!!");
        getmapxy(.@m$, .@x, .@y, 0);
	    .@mobGID = monster(.@m$, .@x, .@y, "Bat", Bat, 1);
	    unitattack(.@mobGID, getcharid(CHAR_ID_ACCOUNT)); // Order the summoned monster to engage!!
    } else {
        mesc l("Nothing happens.");
        next;
        mesc l("What did you expected, anyway?");
    }
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;
    end;
}

