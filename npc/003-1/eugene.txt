// TMW2 scripts.
// Authors:
//    Reid
//    Travolta
//    Saulc
// Description:
//    Fishman NPC
// Quest variable:
//    TulimsharQuests_Fishman
// Quest stages:
//    0 - not started
//    1 - Eugene asked for items
//    2 - completed

003-1,80,127,0	script	Eugene	NPC_EUGENE,{

    narrator S_LAST_NEXT,
        l("You see a raijin boy, sitting on the edge of the dock."),
        l("He's holding a fishing rod, while gazing out at the sea.");

    .@q = getq(TulimsharQuests_Fishman);
    if (.@q == 1) goto L_CheckItems;
    if (.@q == 2) goto L_QuestDone;

    speech S_LAST_BLANK_LINE,
        l("Ahoi."),
        l("Hey, check out my brand new fishing rod. I bought it just today."),
        l("I was so excited, I wanted to try it as soon as possible."),
        l("So in a hurry, I forgot to take enough bait for fishing."),
        lg("Be a friend and bring me @@ @@.", "Be a friend and bring me @@ @@.", .BaitCount, getitemlink(.BaitID));

    switch (select(l("I'll be back in no time."),
                   l("Sorry, I'm doing other things at the moment.")))
    {
        case 1:
            setq TulimsharQuests_Fishman, 1;
            speech S_FIRST_BLANK_LINE,
                l("Thank you. I'll wait here.");
            next;
            mesc l("Protip: @@ are dropped by @@. That monster helps each other, so don't attack when they are in packs.", getitemlink(.BaitID), getmonsterlink(LittleBlub));
            close;
        case 2:
            speech S_FIRST_BLANK_LINE,
                l("But I'm almost out of @@...", getitemlink(.BaitID));
            close;
    }

L_CheckItems:
    if (countitem(.BaitID) < .BaitCount)
    {
        speech
            l("Sorry, but you don't have what I need."),
            l("I need @@ @@.", .BaitCount, getitemlink(.BaitID));
        close;
    }

    speech
        l("That's exactly what I needed!"),
        l("To thank you, accept my old fishing rod."),
        l("It's not as good as my new one, but still very useful."),
        l("Just look at that water! There's a whole bunch of fish down there."),
        l("Oh, and you will need this book too, it will help you learn the basics of fishing."),
        lg("You might even get lucky, and get a @@.",
           "You might even get lucky, and get a @@.", getitemlink(GrassCarp)),
        l("Have a good time fishing!");

    delitem .BaitID, .BaitCount;
    getitem FishingRod, 1;
    getitem FishingGuideVolI, 1;
    getexp 62, 5;
    setq TulimsharQuests_Fishman, 2;
    close;

L_QuestDone:
    // Idea for future: Eugene telling fishman jokes.
    speech
        l("Ahoy, @@!", strcharinfo(0)),
        l("Are the fish biting today?");

    switch (select(l("Yes, everything is going great, thank you!"),
                   l("Actually, I have bad luck. Could you sell me a box full of fresh fish?")))
    {
        case 1:
            speech S_FIRST_BLANK_LINE,
                l("Glad to hear. I swear, the fish I picked before you arrive was THAT big!");
            close;
        case 2:
            speech S_FIRST_BLANK_LINE,
                l("Earlier I hadn't any, but now that I have the baits, I will be glad to sell some to you!");
            npcshopattach(.name$);
            openshop;
            closedialog;
    }

    close;

OnInit:
    .BaitID = SmallTentacles;
    .BaitCount = 10;

    tradertype(NST_MARKET);
    sellitem SmallFishingNet, -1, 1;
    sellitem FishBox, -1, 5;
    sellitem CommonCarp, -1, 3;
    sellitem GrassCarp, -1, 1;

    .sex = G_MALE;
    .distance = 6;
    end;

OnClock0611:
OnClock1200:
OnClock1801:
OnClock0003:
    restoreshopitem SmallFishingNet, 1;
    restoreshopitem FishBox, 5;
    restoreshopitem CommonCarp, 3;
    restoreshopitem GrassCarp, 1;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}

