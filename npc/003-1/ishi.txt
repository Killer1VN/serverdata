// TMW-2 Script
// Author:
// Crazyfefe
// Jesusalva
//
// Do not add void items to array.
// note : the rare item system suck.

003-1,97,97,0	script	Ishi	NPC_PLAYER,{

    if (MPQUEST == 0) {
        mesn l("Ishi, the Rewards Master");
        mesq l("Hey, it seems like you didn't register as a Monster Hunting Quest participant yet! You can sign up with Aidan.");
        close;
    }
    if (BaseLevel < 10) {
        dispbottom l("##1Bug abuser detected! Automatically banning!!");
        atcommand "@ban 5mn "+strcharinfo(0);
        end;
    }

    if (BaseLevel < 37) {
        @mpq_cost=((BaseLevel*2/3) ** 2);
    } else if (BaseLevel <= 50) {
        // From level 37 onwards, we will notice a small drop on price increase factor (until level 50)
        @mpq_cost=((BaseLevel*2/3) ** 2)-(BaseLevel*2);
    } else {
        // After level 50, the formula changes. We don't have exponential anymore.
        // Previous increase reached 67 and will stop at this value.
        @mpq_cost=((50*2/3) ** 2)-(50*2);
        @mpq_cost+=(BaseLevel-50)*67;
        // Of course... We still need to act as if exponent was still there...
        // So we raise it a bit each 3 levels
        @mpq_cost+=(BaseLevel/3)*3;
    }

    if (Mobpt < @mpq_cost)
    {
        mesn l("Ishi, the Rewards Master");
        mesq l("Welcome! I see you have @@ Monster Points. But that isn't enough to get items at your current level, sorry!", Mobpt);
        mesc l("I need at minimum @@ Monster Points to get items at current level.",@mpq_cost);
        close;
    }

    if (BaseLevel < 25) {
        setarray @Items,   Bread,
                            Candy,Orange,BugLeg,
                            CobaltHerb,GambogeHerb,
                            MauveHerb,MaggotSlime,ScorpionStinger,SilkCocoon,
                            RustyKnife,Coral,PiouLegs,Cheese,RoastedMaggot,RawLog;
    } else {
        setarray @Items,   Bread, Croconut,Plushroom,
                            RedApple,Beer,Candy,Orange,ChocolateBar,BugLeg,CoinBag,
                            Coal,SnakeSkin,CottonCloth,GrassSeeds,HardSpike,CobaltHerb,GambogeHerb,
                            MauveHerb,CopperOre,MaggotSlime,RawLog,ScorpionStinger,SilkCocoon,TreasureKey,
                            WhiteFur,EmptyBottle,RustyKnife,Coral,PiouLegs,Cheese,SnakeEgg,RoastedMaggot,BlueDye;
    }
    setarray @Rares,   BrimmedHat, 5, IronIngot, 10, BronzeGift, 10, SmokeGrenade, 15, Grenade, 15;

    // Push blueprints based on your Job Level
    array_push(@Rares, AlchemyBlueprintA);
    array_push(@Rares, (JobLevel/3));
    array_push(@Rares, EquipmentBlueprintA);
    array_push(@Rares, (JobLevel/3));

    // Additional Loot for level 45+
    if (BaseLevel >= 45) {
        array_push(@Items, IronOre);
        array_push(@Items, SilverOre);
        array_push(@Items, TinOre);
        array_push(@Items, ChocolateMouboo);
        array_push(@Items, MoubooSteak);
        array_push(@Items, Milk);
        array_push(@Items, TolchiAmmoBox);
        array_push(@Items, ShadowHerb);

        array_push(@Rares, AlchemyBlueprintB);
        array_push(@Rares, (JobLevel/6));
        array_push(@Rares, EquipmentBlueprintB);
        array_push(@Rares, (JobLevel/6));
    }

    // Additional Loot for level 75+
    if (BaseLevel >= 75) {
        array_push(@Items, LeadOre);
        array_push(@Items, TitaniumOre);
        array_push(@Items, IridiumOre);
        array_push(@Items, GoldOre);
        array_push(@Items, IronAmmoBox);

        array_push(@Rares, TulimWarpCrystal);
        array_push(@Rares, 40);
        array_push(@Rares, MercBoxA);
        array_push(@Rares, 40);
        array_push(@Rares, AlchemyBlueprintC);
        array_push(@Rares, (JobLevel/9));
        array_push(@Rares, EquipmentBlueprintC);
        array_push(@Rares, (JobLevel/9));
    }

    mesn l("Ishi, the Rewards Master");
    mesq l("Welcome! I see you have @@ Monster Points. Would you like to exchange some of those for items?", Mobpt);
    next;
    if (@mpq_cost > 0)
        mesc l("You can get up to @@ items.", (Mobpt/@mpq_cost));

    menuint
        rif(Mobpt >= @mpq_cost,     "1"), 1,
        rif(Mobpt >= (@mpq_cost)*2, "2"), 2,
        rif(Mobpt >= (@mpq_cost)*3, "3"), 3,
        rif(Mobpt >= (@mpq_cost)*4, "4"), 4,
        rif(Mobpt >= (@mpq_cost)*5, "5"), 5,
        rif(Mobpt >= (@mpq_cost)*6, "6"), 6,
        rif(Mobpt >= (@mpq_cost)*7, "7"), 7,
        rif(Mobpt >= (@mpq_cost)*8, "8"), 8,
        rif(Mobpt >= (@mpq_cost)*9, "9"), 9,
        rif(Mobpt >= (@mpq_cost)*10,"10"), 10,
        rif(Mobpt >= (@mpq_cost)*11,l("Gimme as many as I deserve!")), -1,
        l("Sorry, I have to go now."), 0;

    .@var=@menuret;

    // Special cases
    if (!.@var)
        goto L_Close;
    if (.@var < 0)
        goto L_Give_all;

    goto L_Items;

L_Close:
    .@var=0;
    closedialog;
    goodbye;
    close;

L_Items:
    //debugmes "Reaching item loop";
    for (.@i = 0; .@i < .@var; .@i ++)
    {
        //debugmes "Items: "+str(.@var);
        .@lucked=0;
        .@reward=0;

        if (BaseLevel > 25) {
            //debugmes "Testing rares";
            @lucky = rand(10000) + 1;
            for (.@b = 0; .@b < getarraysize(@Rares); .@b=.@b+2) {
                //debugmes "Checking "+@Rares[.@b]+" - b is now "+.@b;
                //debugmes l("Check @@ <= @@", @lucky, @Rares[.@b+1]);
                if (.@b == 0)
                    @control = 0;
                if(@lucky >= (@control + 1) && @lucky <= @control + (@Rares[.@b+1]))
                {
                    .@lucked=1;
                    .@reward = @Rares[.@b];
                }
                @control = @control + (@Rares[.@b+1]);
            }
        }

        //debugmes "Setting reward"; // could be if (!.@lucked) but for sanity...
        if (!.@reward)
            .@reward=any_of(@Items);

        //debugmes "Check weight";
        inventoryplace .@reward, 1;

        //debugmes "Processing...";
        Mobpt = Mobpt - @mpq_cost;
        getitem .@reward,1;

        //debugmes "Printing...";
        if (.@lucked) {
            mes "";
            mes l("Wow!");
            mes l("I can't believe.");
            mes l("you got lucky and got a(n) @@!", getitemlink(.@reward));
            mes "";
        } else {
            mesq l("You received one @@!", getitemlink(.@reward));
        }

    }
    close;

L_Continue:
    mesq l("You still have @@ Monster Points! Do you want more items?", Mobpt);
    select
        l("Yes"),
        l("No");

    switch (@menu)
    {
        case 1:
            goto L_Give_all;
            break;
        case 2:
            goto L_Close;
            break;
    }
    close;

L_Give_all:
    .@var = Mobpt / @mpq_cost;
    if (.@var > 50) { // limit to avoid lag server. Probably a bad idea.
        .@var = 50;
        mes l("You have too much points. I can't allow you to take all at once right now. I'll try to give you 50, and you come back later!");
        next;
    }

    goto L_Items;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    //setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 4);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 13);

    .sex = G_MALE;
    .distance = 5;
    end;
}

