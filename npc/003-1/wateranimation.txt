// TMW2 scripts.
// Author:
//    gumi
//    Reid
//    Saulc
// Description:
//    Water animations, splash, fishes, etc...

003-1,83,128,0	script	#water_animation0	NPC_WATER_SPLASH,{

    fishing; // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

003-1,87,125,0	duplicate(#water_animation0)	#water_animation1	NPC_WATER_SPLASH
003-1,88,120,0	duplicate(#water_animation0)	#water_animation2	NPC_WATER_SPLASH
003-1,76,120,0	duplicate(#water_animation0)	#water_animation3	NPC_WATER_SPLASH
003-1,75,127,0	duplicate(#water_animation0)	#water_animation4	NPC_WATER_SPLASH
003-1,79,111,0	duplicate(#water_animation0)	#water_animation5	NPC_WATER_SPLASH
003-1,82,105,0	duplicate(#water_animation0)	#water_animation6	NPC_WATER_SPLASH
003-1,85,110,0	duplicate(#water_animation0)	#water_animation7	NPC_WATER_SPLASH
003-1,88,113,0	duplicate(#water_animation0)	#water_animation8	NPC_WATER_SPLASH
003-1,86,126,0	duplicate(#water_animation0)	#water_animation9	NPC_WATER_SPLASH
003-1,87,132,0	duplicate(#water_animation0)	#water_animation10	NPC_WATER_SPLASH
003-1,83,111,0	duplicate(#water_animation0)	#water_animation11	NPC_WATER_SPLASH
003-1,78,144,0	duplicate(#water_animation0)	#water_animation12	NPC_WATER_SPLASH
003-1,83,140,0	duplicate(#water_animation0)	#water_animation13	NPC_WATER_SPLASH
003-1,72,147,0	duplicate(#water_animation0)	#water_animation14	NPC_WATER_SPLASH
003-1,72,122,0	duplicate(#water_animation0)	#water_animation15	NPC_WATER_SPLASH


003-1,71,58,0	script	#lowsea_tulim0	NPC_WATER_SPLASH,{

    fishing; // begin or continue fishing
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    .fishing_rod=SmallFishingNet;
    .net_ratio=3;
    .catch_time=8000; // You have 3 more seconds to pull here
    .wait_time_min=8000;
    .wait_time_max=21000;
    .pull_rand_max=1600;
    .regen_time=30;
    end;
}

003-1,95,47,0	duplicate(#lowsea_tulim0)	#lowsea_tulim1	NPC_WATER_SPLASH
003-1,121,48,0	duplicate(#lowsea_tulim0)	#lowsea_tulim2	NPC_WATER_SPLASH
003-1,121,36,0	duplicate(#lowsea_tulim0)	#lowsea_tulim3	NPC_WATER_SPLASH
003-1,32,107,0	duplicate(#lowsea_tulim0)	#lowsea_tulim4	NPC_WATER_SPLASH
003-1,25,78,0	duplicate(#lowsea_tulim0)	#lowsea_tulim5	NPC_WATER_SPLASH
003-1,16,50,0	duplicate(#lowsea_tulim0)	#lowsea_tulim6	NPC_WATER_SPLASH

