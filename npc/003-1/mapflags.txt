003-1	mapflag	mask	1
003-1	mapflag	town
003-1	mapflag	nopenalty
003-2	mapflag	zone	indoors
003-3	mapflag	zone	indoors
003-4	mapflag	zone	indoors
003-5	mapflag	zone	indoors
003-6	mapflag	zone	indoors
003-7	mapflag	zone	indoors
003-8	mapflag	zone	indoors
//003-9	mapflag	zone	indoors
003-10	mapflag	zone	indoors
//003-11	mapflag	zone	indoors
//003-12	mapflag	zone	indoors
003-13	mapflag	zone	indoors

// Magic Council
003-0	mapflag	zone	indoors
003-0-1	mapflag	zone	indoors
003-0-2	mapflag	zone	indoors

// Town Walls are a no penalty zone
003-1-2	mapflag	zone	indoors
