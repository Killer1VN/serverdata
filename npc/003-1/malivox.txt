// Author:
//    Saulc

003-1,114,106,0	script	Malivox	NPC_PLAYER,{

    speech S_LAST_NEXT,
        l("I am Malivox, an alchemist specialized in reset potions.");

L_Menu:
    if (BaseLevel < 10)
        .@plush_count = (BaseLevel*5)+5;
    else
        .@plush_count = BaseLevel*190-(9*190);
    // Lv 9: 50 GP | Lv 10: 190 GP
    // Lv 90: 1.710 GP

    if (BaseLevel > 10)
        .@plush_count = .@plush_count/(BaseLevel/10);

    select
        l("Can you reset my stats please?"),
        lg("You are weird, I have to go sorry.");

    switch (@menu)
    {
        case 1:
            goto L_ResetStats;
        case 2:
            goto L_Quit;
    }

L_ResetStats:
    mesn;
    mesq l("Status point reset can't be undone. Do you really want this?");

L_ConfirmReset:
    ConfirmStatusReset();
    goto L_Quit;


L_Quit:
    goodbye;
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, FancyHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SailorShirt);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BromenalPants);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 3;
    end;
}
